import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { SubcuentaZoomEntity } from './subcuenta-zoom.entity';

@Entity('cuenta_zoom')
export class CuentaZoomEntity {

  @PrimaryGeneratedColumn({ type: 'integer' })
  id_cuenta_zoom?: number;

  @Column()
  nombre_cuenta: string;

  @Column()
  password_cuenta: string;

  @Column()
  api_secret: string;

  @Column()
  token: string;

  @Column()
  numero_cuentas: number;

  @OneToMany(
    () => SubcuentaZoomEntity,
    subcuenta => subcuenta.cuenta,
  )
  subcuenta?: SubcuentaZoomEntity;

}

