import { Column, Entity, JoinTable, ManyToMany, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { PermisoEntity } from './permiso.entity';
import { UsuarioRolEntity } from './usuario-rol.entity';

@Entity('rol')
export class RolEntity {

  @PrimaryGeneratedColumn()
  id_rol?: number;

  @Column({ name: 'nombre_rol', length: 50 })
  nombre_rol: string;

  @Column({ name: 'descripcion', length: 100, nullable: true })
  descripcion: string;

  @ManyToMany(() => PermisoEntity, { eager: true, cascade: true })
  @JoinTable({
    name: 'rol_permiso',
    joinColumn: {
      name: 'rol_id',
      referencedColumnName: 'id_rol',
    },
    inverseJoinColumn: {
      name: 'permiso_id',
      referencedColumnName: 'id_permiso',
    },
  })
  permisos?: PermisoEntity[];

  @OneToMany(
    () => UsuarioRolEntity,
    usuario => usuario.rol,
  )
  usuario?: UsuarioRolEntity;

}
