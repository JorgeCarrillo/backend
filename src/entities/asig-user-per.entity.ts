import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { ComentariosEntity } from './comentarios.entity';
import { UsuarioRolEntity } from './usuario-rol.entity';
import { AsigPerServEntity } from './asig-per-serv.entity';
import { PedidoEntity } from './pedido.entity';

@Entity('asig_user_per')
export class AsigUserPerEntity {

  @PrimaryGeneratedColumn({type: 'integer'})
  id_asig_user_per?: number;

  @Column()
  id_usuario_rol: number;

  @Column()
  id_asig_per_serv: number;

  @ManyToOne(
    () => UsuarioRolEntity,
    usuario_rol => usuario_rol.asig_user_per,
    { eager: false, onDelete: 'CASCADE' },
  )
  @JoinColumn({ name: 'id_usuario_rol' })
  usuario_rol?: UsuarioRolEntity;

  // @OneToMany(
  //   () => ComentariosEntity,
  //   comentario => comentario.user_per
  // )
  // comentario?: ComentariosEntity;

  @ManyToOne(
    () => AsigPerServEntity,
    asig_per_serv => asig_per_serv.asig_user_per,
    { eager: false, onDelete: 'CASCADE' },
  )
  @JoinColumn({ name: 'id_asig_per_serv' })
  asig_per_serv?: AsigPerServEntity;

  @OneToMany(
    () => PedidoEntity,
    pedido => pedido.asig_user_per
  )
  pedido?: PedidoEntity

}