import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { PedidoEntity } from './pedido.entity';
import { PagoEntity } from './pago.entity';

@Entity('detalle_pedido')
export class DetallePedidoEntity {

  @PrimaryGeneratedColumn({ type: 'integer' })
  id_detalle_pedido?: number;

  @Column({ type: 'varchar', length: 10 })
  codigo: string;

  @Column({ type: 'varchar', length: 500 })
  descripcion: string;

  @Column({ type: 'integer' })
  cantidad: number;

  @Column({ type: 'decimal', precision: 5, scale: 2 })
  valor_unitario: number;

  @Column({ type: 'decimal', precision: 10, scale: 2 })
  valor_total: number;

  @Column({ type: 'varchar', length: 100 })
  fecha_creacion: string;

  @Column()
  id_pedido: number;

  @ManyToOne(
    () => PedidoEntity,
    pedido => pedido.detalle,
    { eager: true, onDelete: 'CASCADE' },
  )
  @JoinColumn({ name: 'id_pedido' })
  pedido?: PedidoEntity;

  @OneToMany(
    () => PagoEntity,
    pago => pago.detalle,
  )
  pago?: PagoEntity;

}