import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { UsuarioRolEntity } from './usuario-rol.entity';
import { AsigUserPerEntity } from './asig-user-per.entity';

@Entity('comentarios')
export class ComentariosEntity {

  @PrimaryGeneratedColumn({ type: 'integer' })
  id_comentario?: number;

  @Column({ type: 'timestamp without time zone', nullable: true })
  fecha_valoracion: Date;

  @Column({ type: 'varchar', length: 500 })
  comentario: string;

  @Column()
  valoracion: number;

  @Column()
  id_usuario_rol: number;

  @Column()
  id_usuario_rol_profesor: number;

  @ManyToOne(
    () => UsuarioRolEntity,
    usuario_rol => usuario_rol.comentario,
    { eager: false, onDelete: 'CASCADE' },
  )
  @JoinColumn({ name: 'id_usuario_rol' })
  usuario_rol?: UsuarioRolEntity;

  // @ManyToOne(
  //   () => AsigUserPerEntity,
  //   user_per => user_per.comentario,
  //   { eager: false, onDelete: 'CASCADE' },
  // )
  // @JoinColumn({ name: 'id_asig_user_per' })
  // user_per?: AsigUserPerEntity;

}