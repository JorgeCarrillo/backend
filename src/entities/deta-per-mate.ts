import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { PersonalizadoEntity } from './personalizado.entity';
import { MateriaEntity } from './materia.entity';

@Entity('deta_per_mate')
export class DetaPerMateEntity {

  @PrimaryGeneratedColumn({ type: 'integer' })
  id_deta_per_mate?: number;

  @Column({ type: 'integer' })
  id_tutoria: number;

  @Column({ type: 'integer' })
  id_materia: number;



  @ManyToOne(
    () => PersonalizadoEntity,
    personalizado => personalizado.detalle,
    { eager: true, onDelete: 'CASCADE' }
  )
  @JoinColumn({ name: 'id_tutoria' })
  personalizado?: PersonalizadoEntity;


  @ManyToOne(
    () => MateriaEntity,
    materia => materia.detalle,
    { eager: true, onDelete: 'CASCADE' }
  )
  @JoinColumn({ name: 'id_materia' })
  materia?: MateriaEntity;


  /* @OneToMany(
     () => DisponibilidadEntity,
     disponibilidad => disponibilidad.detalle,
   )
   disponibilidad?: DisponibilidadEntity;*/
}