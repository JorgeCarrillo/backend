import { Column, Entity, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { TitulosProfesorEntity } from './titulos-profesor.entity';
import { DetallesProfesorEntity } from './detalles-profesor.entity';
import { RolEntity } from './rol.entity';
import { UsuarioEntity } from './usuario.entity';
import { EmisorReceptorEntity } from './emisor-receptor.entity';
import { ComentariosEntity } from './comentarios.entity';
import { AsigUserPerEntity } from './asig-user-per.entity';
import { PedidoEntity } from './pedido.entity';
import { DetaDiaHoraEntity } from './deta-dia-hora.entity';
import { TajetaPagoEntity } from './tarjeta-pago.entity';

@Entity('usuario_rol')
export class UsuarioRolEntity {

  @PrimaryGeneratedColumn({ type: 'integer' })
  id_usuario_rol?: number;

  @OneToMany(
    () => TitulosProfesorEntity,
    titulos => titulos.usuario_rol,
  )
  titulos?: TitulosProfesorEntity;

  @OneToOne(
    () => DetallesProfesorEntity,
    detalle => detalle.usuario_rol,
  )
  detalle?: DetallesProfesorEntity;

  @ManyToOne(
    () => UsuarioEntity,
    usuario => usuario.usuario_rol,
    { eager: true, onDelete: 'CASCADE' },
  )
  @JoinColumn({ name: 'id_usuario' })
  usuario?: UsuarioEntity | number;

  @Column({ name: 'id_rol' })
  idRol?: number;

  @ManyToOne(
    () => RolEntity,
    rol => rol.usuario,
    { eager: true, onDelete: 'CASCADE' },
  )
  @JoinColumn({ name: 'id_rol' })
  rol?: RolEntity | number;

  @OneToMany(
    () => EmisorReceptorEntity,
    emisor_receptor => emisor_receptor.usuario_rol,
  )
  emisor_receptor?: EmisorReceptorEntity;

  @OneToMany(
    () => ComentariosEntity,
    comentario => comentario.usuario_rol,
  )
  comentario?: EmisorReceptorEntity;

  @OneToMany(
    () => AsigUserPerEntity,
    asig_user_per => asig_user_per.usuario_rol,
  )
  asig_user_per?: AsigUserPerEntity;

  @OneToMany(
    () => PedidoEntity,
    pedido => pedido.usuario_rol
  )
  pedido?: PedidoEntity

  @OneToMany(
    () => DetaDiaHoraEntity,
    detallehora => detallehora.usuario,
  )
  detallehora?: DetaDiaHoraEntity;

  @OneToMany(
    () => TajetaPagoEntity,
    tarjetapago => tarjetapago.usuario,
  )
  tarjetapago?: TajetaPagoEntity;

}