import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { EmisorReceptorEntity } from './emisor-receptor.entity';

@Entity('mensaje')
export class MensajeEntity {

  @PrimaryGeneratedColumn({ type: 'integer' })
  id_mensaje?: number;

  @Column({ type: 'varchar', length: 500 })
  mensaje: string;

  @Column({ default: null })
  fecha_mensaje: string;

  @Column({ nullable: true })
  hora: string;

  @OneToMany(
    () => EmisorReceptorEntity,
    emisor_receptor => emisor_receptor.mensaje,
  )
  emisor_receptor?: EmisorReceptorEntity;

}