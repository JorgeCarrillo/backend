import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { CuentaZoomEntity } from './cuenta-zoom.entity';
import { ReunionEntity } from './reunion.entity';

@Entity('subcuenta_zoom')
export class SubcuentaZoomEntity {

  @PrimaryGeneratedColumn({type: 'integer'})
  id_subcuenta?: number;

  @Column()
  nombre_usuario: string;

  @Column()
  contraseña: string;

  @Column()
  id_cuenta: number;
  @Column()
  id_usuario_zoom: string;
  @Column()
  disponibilidad_hora: string;

  @ManyToOne(
    () => CuentaZoomEntity,
    cuenta => cuenta.subcuenta,
    {eager: true, onDelete: 'CASCADE'}
  )
  @JoinColumn({name: 'id_cuenta'})
  cuenta?: CuentaZoomEntity | number;

  @OneToMany(
    () => ReunionEntity,
    reunion => reunion.subcuenta_zoom
  )
  reunion?: ReunionEntity

}