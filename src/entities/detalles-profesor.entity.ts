import { IsInt } from 'class-validator';
import { Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { ManyToOne } from 'typeorm/decorator/relations/ManyToOne';
import { DetaDiaHoraEntity } from './deta-dia-hora.entity';
import { UsuarioRolEntity } from './usuario-rol.entity';

@Entity('detalles_profesor')
export class DetallesProfesorEntity {

  @PrimaryGeneratedColumn({ type: 'integer' })
  id_deta_profe?: number;

  @Column({ length: 500, type: 'varchar', nullable: true })
  cv_deta_profe: string;

  @Column({ length: 500, type: 'varchar', nullable: true })
  penales_deta_profe: string;

  @Column({ length: 300, type: 'varchar', nullable: true })
  descri_deta_profe: string;

  @Column({ length: 500, type: 'varchar', nullable: true })
  ruta_deta_profe: string;

  /*@Column({ length: 500, type: 'varchar', nullable: true })
  ruta_video_clip?: string;*/

  @Column({ type: 'varchar', length: 300, nullable: true })
  facebook?: string;

  @Column({ type: 'varchar', length: 300, nullable: true })
  twitter?: string;

  @Column({ type: 'varchar', length: 300, nullable: true })
  youtube?: string;

  @Column({ type: 'varchar', length: 300, nullable: true })
  instagram?: string;

  @Column({ type: 'varchar', nullable: true })
  experiencia?: string;

  @Column({ type: 'varchar', nullable: true })
  preferencia_ense?: string;

  @Column({ type: 'varchar', length: 10, nullable: true })
  preciofijo?: string;

  @Column({ nullable: true })
  video_presentacion?: string;

  @Column({ type: 'varchar', length: 50, nullable: true })
  rating?: string;

  @Column()
  id_usuario_rol: number;



  @OneToOne(
    () => UsuarioRolEntity,
    usuario_rol => usuario_rol.detalle,
    { eager: false, onDelete: 'CASCADE' },
  )
  @JoinColumn({ name: 'id_usuario_rol' })
  usuario_rol?: number;

  /* @ManyToOne(
     () => DetaDiaHoraEntity,
     detalle => detalle.detallepro,
     { eager: true, onDelete: 'CASCADE' }
   )
   @JoinColumn({ name: 'id_deta_dia_hora' })
   detalle?: DetaDiaHoraEntity*/
}