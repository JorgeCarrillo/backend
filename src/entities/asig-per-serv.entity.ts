import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { ServiciosEntity } from './servicios.entity';
import { PersonalizadoEntity } from './personalizado.entity';
import { AsigUserPerEntity } from './asig-user-per.entity';

@Entity('asig_per_serv')
export class AsigPerServEntity {

  @PrimaryGeneratedColumn({ type: 'integer' })
  id_asig_per_serv?: number;

  @Column()
  id_servicio: number;

  @Column()
  id_tutoria: number;

  @ManyToOne(
    () => ServiciosEntity,
    servicio => servicio.asig_per_serv,
    { eager: false, onDelete: 'CASCADE' },
  )
  @JoinColumn({ name: 'id_servicio' })
  servicio?: ServiciosEntity;

  @ManyToOne(
    () => PersonalizadoEntity,
    personalizado => personalizado.asig_per_serv,
    { eager: false, onDelete: 'CASCADE' },
  )
  @JoinColumn({ name: 'id_tutoria' })
  personalizado?: PersonalizadoEntity;

  @OneToMany(
    () => AsigUserPerEntity,
    asig_user_per => asig_user_per.asig_per_serv,
  )
  asig_user_per?: AsigUserPerEntity;

}