import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { PersonalizadoEntity } from './personalizado.entity';
import { PedidoEntity } from './pedido.entity';

@Entity('cancel_anula')
export class CancelAnulaEntity {

  @PrimaryGeneratedColumn({ type: 'integer' })
  id_cambia_anula?: number;

  @Column({ type: 'varchar', length: 50 })
  condicion_anula: string;

  @Column({ type: 'varchar', length: 50 })
  condicion_cambia: string;

  /* @Column()
   id_tutoria: number;*/

  @Column()
  id_pedido: number;

  /*@ManyToOne(
    () => PersonalizadoEntity,
    personalizado => personalizado.cancel_anula,
    { eager: true, onDelete: 'CASCADE' },
  )
  @JoinColumn({ name: 'id_tutoria' })
  personalizado?: PersonalizadoEntity;*/

  @ManyToOne(
    () => PedidoEntity,
    pedido => pedido.cancel_anula,
    { eager: true, onDelete: 'CASCADE' },
  )
  @JoinColumn({ name: 'id_pedido' })
  pedido?: PedidoEntity;


}