import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { AsigPerServEntity } from './asig-per-serv.entity';

@Entity('servicios')
export class ServiciosEntity {

  @PrimaryGeneratedColumn({ type: 'integer' })
  id_servicio?: number;

  @Column({type: 'varchar', length: 100})
  nombre_servicio: string;

  @Column({type: 'varchar', length: 500})
  descripcion: string;

  @Column({type: 'boolean'})
  estado: boolean;

  @OneToMany(
    () => AsigPerServEntity,
    asig_per_serv => asig_per_serv.servicio
  )
  asig_per_serv?: AsigPerServEntity

}