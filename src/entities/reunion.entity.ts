import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { SubcuentaZoomEntity } from './subcuenta-zoom.entity';
import { PedidoEntity } from './pedido.entity';

@Entity('reunion')
export class ReunionEntity {

  @PrimaryGeneratedColumn({ type: 'integer' })
  id_activado?: number;

  @Column({ type: 'varchar', length: 50 })
  id_reunion: string;

  @Column({ type: 'varchar' })
  enlace: string;
  @Column({ type: 'varchar' })
  enlaceEstudiante: string;

  @Column({ type: 'varchar', length: 20 })
  password: string;

  @Column({ type: 'varchar' })
  fecha_inicio: string;

  @Column({ type: 'varchar' })
  fecha_fin: string;

  @Column()
  hora_inicio: string;

  @Column()
  hora_fin: string;

  @Column({ type: 'boolean' })
  estado: boolean;

  @Column()
  id_subcuenta?: number;

  @Column()
  id_pedido?: number;

  @ManyToOne(
    () => SubcuentaZoomEntity,
    subcuenta_zoom => subcuenta_zoom.reunion,
    {eager: true, onDelete: 'CASCADE'}
  )
  @JoinColumn({name: 'id_subcuenta'})
  subcuenta_zoom?: SubcuentaZoomEntity;

  @ManyToOne(
    () => PedidoEntity,
    pedido => pedido.reunion,
    {eager: true, onDelete: 'CASCADE'}
  )
  @JoinColumn({name: 'id_pedido'})
  pedido?: PedidoEntity;

}