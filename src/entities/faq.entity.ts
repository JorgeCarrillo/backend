import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('faq')
export class FaqEntity {

  @PrimaryGeneratedColumn({ type: 'integer' })
  id_faq?: number;

  @Column({ type: 'varchar', length: 200 })
  pregunta: string;

  @Column({ type: 'varchar', length: 200 })
  respuesta: string;

}