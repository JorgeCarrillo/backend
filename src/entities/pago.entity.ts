import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { DetallePedidoEntity } from './detalle-pedido.entity';

@Entity('pago')
export class PagoEntity {

  @PrimaryGeneratedColumn({ type: 'integer' })
  id_pago?: number;

  @Column({ type: 'varchar', length: 50 })
  metodo_pago: string;

  @Column({ type: 'decimal', precision: 5, scale: 2 })
  sub_total: number;

  @Column({ type: 'decimal', precision: 5, scale: 2 })
  impuesto: number;

  @Column({ type: 'decimal', precision: 5, scale: 2 })
  total: number;

  @Column()
  id_detalle_pedido: number;

  @ManyToOne(
    () => DetallePedidoEntity,
    detalle => detalle.pago,
    { eager: true, onDelete: 'CASCADE' },
  )
  @JoinColumn({ name: 'id_detalle_pedido' })
  detalle?: DetallePedidoEntity;

}