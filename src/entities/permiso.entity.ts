import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity('permiso')
export class PermisoEntity {

  @PrimaryGeneratedColumn()
  id_permiso?: number;

  @Column({ length: 50 })
  nombre_permiso: string;

  @Column({ length: 100, nullable: true })
  descripcion_permiso: string;

  @Column({ length: 500, nullable: true })
  url_permiso: string;

  @Column({length: 200, nullable: true})
  metodo: string;

  @OneToMany(type => PermisoEntity, permiso => permiso.permiso)
  permisos?: PermisoEntity[];

  @ManyToOne(type => PermisoEntity, permiso => permiso.permisos)
  @JoinColumn({ name: 'id_permiso' })
  permiso?: PermisoEntity;

  /*@Column({ name: 'id_tipo_permiso' })
  idTipoPermiso: number;

  @ManyToOne(type => TipoPermisoEntity, tipoPermiso => tipoPermiso.permisos)
  @JoinColumn({ name: 'id_tipo_permiso' })
  tipoPermiso?: TipoPermisoEntity;*/
}
