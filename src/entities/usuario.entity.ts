import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { CiudadEntity } from './ciudad.entity';
import { LoginUsuarioEntity } from './login-usuario.entity';
import { UsuarioRolEntity } from './usuario-rol.entity';
import { IdiomaEntity } from './idioma.entity';

@Entity('usuario')
export class UsuarioEntity {

  @PrimaryGeneratedColumn({ type: 'integer' })
  id_usuario?: number;

  @Column({ length: 15, type: 'varchar', nullable: true })
  identificacion_usuario?: string;

  @Column({ length: 100, type: 'varchar', nullable: true })
  nombres_usuario: string;

  @Column({ length: 100, type: 'varchar', nullable: true })
  apellidos_usuario: string;

  @Column({ type: 'date', default: null })
  fecha_naci_usuario?: Date;

  @Column({ type: 'timestamp with time zone', default: new Date().toLocaleString() })
  fecha_regisro_usuario?: Date;

  @Column({ type: 'timestamp with time zone', default: new Date().toLocaleString() })
  ultima_conexion?: Date;


  @Column({ length: 50, type: 'varchar', nullable: true })
  correo_usuario?: string;

  @Column({ nullable: true })
  foto_usuario?: string;

  @Column({ length: 15, type: 'varchar', nullable: true })
  telefono_usuario?: string;

  @Column({ length: 500, nullable: true, type: 'varchar' })
  direccion_usuario?: string;

  @Column({ length: 20, type: 'varchar', nullable: true })
  sexo_usuario?: string;

  @Column({ type: 'varchar', nullable: true })
  codigo_password?: string;

  @Column({ nullable: true })
  estado_codigo: boolean;

  @Column()
  estado_usuario: boolean;

  @Column({ nullable: true })
  estado_usuario_cuenta: boolean;

  @Column({ type: 'varchar', nullable: true },)
  ciudad_nombre?: string;

  @ManyToOne(
    () => CiudadEntity,
    ciudad => ciudad.usuario,
    { eager: true, onDelete: 'CASCADE' },
  )
  @JoinColumn({ name: 'id_ciudad' })
  ciudad?: CiudadEntity | number;

  @OneToMany(
    () => LoginUsuarioEntity,
    login => login.usuario,
  )
  login?: LoginUsuarioEntity;

  @OneToMany(
    () => UsuarioRolEntity,
    usuario_rol => usuario_rol.usuario,
  ) usuario_rol?: UsuarioRolEntity;


  @OneToMany(
    () => IdiomaEntity,
    idioma => idioma.usuario,
  )
  idioma?: IdiomaEntity;

}
