import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { UsuarioRolEntity } from './usuario-rol.entity';
import { PedidoEntity } from './pedido.entity';

@Entity('deta_dia_hora')
export class DetaDiaHoraEntity {

  @PrimaryGeneratedColumn({ type: 'integer' })
  id_deta_dia_hora?: number;

  @Column({ nullable: true })
  hora_inicio: string;

  @Column({ nullable: true })
  hora_fin: string;



  /* @ManyToOne(
     () => HorarioDiaEntity,
     dia => dia.detalle,
     { eager: true, onDelete: 'CASCADE' }
   )
   @JoinColumn({ name: 'id_horario_dia' })
   dia?: HorarioDiaEntity;*/

  /*@ManyToOne(
    () => HorarioHoraEntity,
    hora => hora.detalle,
    { eager: true, onDelete: 'CASCADE' }
  )
  @JoinColumn({ name: 'id_horario_hora' })
  hora?: HorarioHoraEntity;*/


  @Column()
  id_usuario_rol: number;

  @ManyToOne(
    () => UsuarioRolEntity,
    usuario => usuario.detallehora,
    { eager: true, onDelete: 'CASCADE' }
  )
  @JoinColumn({ name: 'id_usuario_rol' })
  usuario?: UsuarioRolEntity;

  @Column({ nullable: true })
  estado_reserva: number;

  @OneToMany(
    () => PedidoEntity,
    pedido => pedido.deta_dia_hora
  )
  pedido?: PedidoEntity
}