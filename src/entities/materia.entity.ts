import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { PersonalizadoEntity } from './personalizado.entity';
import { CategoriaEntity } from './categoria.entity';
import { DetaPerMateEntity } from './deta-per-mate';

@Entity('materia')
export class MateriaEntity {

  @PrimaryGeneratedColumn({ type: 'integer' })
  id_materia?: number;

  @Column({ type: 'varchar', length: 100 })
  nombre_materia: string;

  @Column()
  id_categoria?: number;

  /* @OneToMany(
     () => PersonalizadoEntity,
     personalizado => personalizado.materia
   )
   personalizado?: PersonalizadoEntity;*/

  @ManyToOne(
    () => CategoriaEntity,
    categoria => categoria.materia,
    { eager: false, onDelete: 'CASCADE' }
  )
  @JoinColumn({ name: 'id_categoria' })
  categoria?: CategoriaEntity

  @OneToMany(
    () => DetaPerMateEntity,
    detalle => detalle.materia
  )
  detalle?: DetaPerMateEntity
}