import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { UsuarioEntity } from './usuario.entity';

@Entity('idioma')
export class IdiomaEntity {

  @PrimaryGeneratedColumn({ type: 'integer' })
  id_idioma?: number;

  @Column({ type: 'varchar', length: 20, nullable: true })
  idioma?: string;

  @Column({ type: 'varchar', length: 20, nullable: true })
  nivel?: string;

  @Column({ default: false })
  preferencia: boolean;

  @Column({ type: 'integer' })
  id_usuario?: number;

  @ManyToOne(
    () => UsuarioEntity,
    usuario => usuario.idioma,
    { eager: true, onDelete: 'CASCADE' },
  )
  @JoinColumn({ name: 'id_usuario' })
  usuario?: UsuarioEntity;

}