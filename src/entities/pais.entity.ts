import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { EstadoEntity } from './estado.entity';

@Entity('pais')
export class PaisEntity {

  @PrimaryGeneratedColumn({ type: 'integer' })
  id_pais?: number;

  @Column({ nullable: true, length: 20, type: 'varchar' })
  nombre_pais: string;

  @OneToMany(
    () => EstadoEntity,
    estado => estado.pais,
  )
  estado?: EstadoEntity;

}