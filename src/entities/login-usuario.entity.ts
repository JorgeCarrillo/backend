import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn, Unique, UpdateDateColumn } from 'typeorm';
import { UsuarioEntity } from './usuario.entity';

@Entity('login_usuario')
@Unique(['nombre_login'])
export class LoginUsuarioEntity {

  @PrimaryGeneratedColumn({ type: 'integer' })
  id_login?: number;

  @Column({ type: 'varchar', length: 50 })
  nombre_login: string;

  @Column()
  password_login: string;

  @UpdateDateColumn({ type: 'timestamp without time zone', default: new Date().toLocaleString() })
  fecha_ingre?: Date;

  @Column({ nullable: true })
  id_usuario?: number;

  @ManyToOne(
    () => UsuarioEntity,
    usuario => usuario.login,
    { eager: true, onDelete: 'CASCADE' },
  )
  @JoinColumn({ name: 'id_usuario' })
  usuario?: UsuarioEntity;

}