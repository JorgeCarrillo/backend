import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { AsigPerServEntity } from './asig-per-serv.entity';
import { MateriaEntity } from './materia.entity';
import { CancelAnulaEntity } from './cancel-anula.entity';
import { DetaPerMateEntity } from './deta-per-mate';

@Entity('personalizado')
export class PersonalizadoEntity {

  @PrimaryGeneratedColumn({ type: 'integer' })
  id_tutoria?: number;

  @Column({ nullable: true })
  fecha_creacion: string;

  @Column({ type: 'boolean' })
  demo: boolean;

  @Column()
  duracion_demo: string;

  /*@Column()
  id_materia?: number;*/

  /*@Column({ nullable: true })
  precio_fijo?: number;*/

  /*@Column()
  id_disponibilidad: number;*/

  @OneToMany(
    () => AsigPerServEntity,
    asig_per_serv => asig_per_serv.personalizado,
  )
  asig_per_serv?: AsigPerServEntity;

  /* @ManyToOne(
     () => MateriaEntity,
     materia => materia.personalizado,
     { eager: false, onDelete: 'CASCADE' },
   )
   @JoinColumn({ name: 'id_materia' })
   materia?: MateriaEntity;*/

  /*@OneToMany(
    () => CancelAnulaEntity,
    cancel_anula => cancel_anula.personalizado,
  )
  cancel_anula?: CancelAnulaEntity;*/


  @OneToMany(
    () => DetaPerMateEntity,
    detalle => detalle.personalizado,
  )
  detalle?: DetaPerMateEntity



}