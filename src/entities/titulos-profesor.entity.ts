import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { UsuarioRolEntity } from './usuario-rol.entity';

@Entity('titulos_profesor')
export class TitulosProfesorEntity {

  @PrimaryGeneratedColumn({ type: 'integer' })
  id_titulos_profe?: number;

  @Column({ type: 'varchar', length: 500 })
  senescyt_titulos_profe: string;

  @Column()
  id_usuario_rol?: number;

  @Column({ type: 'varchar', length: 50 })
  nivel_profesor?: string;

  @ManyToOne(
    () => UsuarioRolEntity,
    usuario_rol => usuario_rol.titulos,
    { eager: false, onDelete: 'CASCADE' },
  )
  @JoinColumn({ name: 'id_usuario_rol' })
  usuario_rol?: number;

}