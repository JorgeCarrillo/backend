import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('configuracion')
export class ConfiguracionEntity {

  @PrimaryGeneratedColumn({ type: 'integer' })
  id_configuracion?: number;

  @Column({ type: 'varchar', length: 20 })
  nombre_config: string;

  @Column({ type: 'varchar', length: 20 })
  valor_config: string;

}