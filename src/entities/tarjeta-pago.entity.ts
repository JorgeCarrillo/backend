import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { UsuarioRolEntity } from './usuario-rol.entity';

@Entity('tarjeta_pago')
export class TajetaPagoEntity {
  @PrimaryGeneratedColumn({ type: 'integer' })
  id_tarjeta_pago?: number;

  @Column({ nullable: true })
  titular_tarjeta: string;

  @Column({ nullable: true })
  numero_tarjeta: number;

  @Column({ nullable: true })
  mes: number;

  @Column({ nullable: true })
  ano: number;

  @Column({ nullable: true })
  cvv: number;

  @Column({ nullable: false })
  id_usuario_rol: number;

  @ManyToOne(
    () => UsuarioRolEntity,
    usuario => usuario.tarjetapago,
    { eager: true, onDelete: 'CASCADE' }
  )
  @JoinColumn({ name: 'id_usuario_rol' })
  usuario?: UsuarioRolEntity;

}