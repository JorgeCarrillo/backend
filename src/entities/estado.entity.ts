import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { PaisEntity } from './pais.entity';
import { CiudadEntity } from './ciudad.entity';

@Entity('estado')
export class EstadoEntity {

  @PrimaryGeneratedColumn({ type: 'integer' })
  id_estado?: number;

  @Column({ type: 'varchar', length: 20 })
  nombre_estado: string;

  @Column({ type: 'integer' })
  id_pais?: number;

  @ManyToOne(
    () => PaisEntity,
    pais => pais.estado,
    { eager: true, onDelete: 'CASCADE' },
  )
  @JoinColumn({ name: 'id_pais' })
  pais?: PaisEntity | number;

  @OneToMany(
    () => CiudadEntity,
    ciudad => ciudad.estado,
  )
  ciudad?: CiudadEntity;

}