import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { AsigUserPerEntity } from './asig-user-per.entity';
import { UsuarioRolEntity } from './usuario-rol.entity';
import { DetallePedidoEntity } from './detalle-pedido.entity';
import { CancelAnulaEntity } from './cancel-anula.entity';
import { ReunionEntity } from './reunion.entity';
import { DetaDiaHoraEntity } from './deta-dia-hora.entity';

@Entity('pedido')
export class PedidoEntity {

  @PrimaryGeneratedColumn({ type: 'integer' })
  id_pedido?: number;

  @Column({ type: 'varchar', length: 300 })
  detalle_pedido: string;

  @Column({ type: 'varchar' })
  fecha_inicio: string;

  @Column({ type: 'boolean' })
  estado: boolean;

  @Column()
  id_asig_user_per?: number;

  @Column()
  id_usuario_rol?: number;

  @Column()
  id_deta_dia_hora: number;
  @Column()
  id_usuario_profesor: number;

  @ManyToOne(
    () => AsigUserPerEntity,
    asig_user_per => asig_user_per.pedido,
    { eager: true, onDelete: 'CASCADE' },
  )
  @JoinColumn({ name: 'id_asig_user_per' })
  asig_user_per?: AsigUserPerEntity;

  @ManyToOne(
    () => UsuarioRolEntity,
    usuario_rol => usuario_rol.pedido,
    { eager: true, onDelete: 'CASCADE' },
  )
  @JoinColumn({ name: 'id_usuario_rol' })
  usuario_rol?: UsuarioRolEntity;

  @ManyToOne(
    () => DetaDiaHoraEntity,
    deta_dia_hora => deta_dia_hora.pedido,
    { eager: true, onDelete: 'CASCADE' },
  )
  @JoinColumn({ name: 'id_deta_dia_hora' })
  deta_dia_hora?: DetaDiaHoraEntity;

  @OneToMany(
    () => DetallePedidoEntity,
    detalle => detalle.pedido,
  )
  detalle?: DetallePedidoEntity;

  @OneToMany(
    () => ReunionEntity,
    reunion => reunion.pedido,
  )
  reunion?: ReunionEntity;

  @OneToMany(
    () => CancelAnulaEntity,
    cancel_anula => cancel_anula.pedido,
  )
  cancel_anula?: CancelAnulaEntity;

}