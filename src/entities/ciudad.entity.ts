import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { EstadoEntity } from './estado.entity';
import { UsuarioEntity } from './usuario.entity';

@Entity('ciudad')
export class CiudadEntity {

  @PrimaryGeneratedColumn({ type: 'integer' })
  id_ciudad?: number;

  @Column({ type: 'varchar', length: 20 })
  nombre_ciudad: string;

  @Column({ type: 'integer' })
  id_estado?: number;

  @ManyToOne(
    () => EstadoEntity,
    estado => estado.ciudad,
    { eager: true, onDelete: 'CASCADE' },
  )
  @JoinColumn({ name: 'id_estado' })
  estado?: EstadoEntity;

  @OneToMany(
    () => UsuarioEntity,
    usuario => usuario.ciudad,
  )
  usuario?: UsuarioEntity;

}