import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { UsuarioRolEntity } from './usuario-rol.entity';
import { MensajeEntity } from './mensaje.entity';

@Entity('emisor_receptor')
export class EmisorReceptorEntity {

  @PrimaryGeneratedColumn({ type: 'integer' })
  id_emi_rec?: number;

  @Column()
  id_emisor: number;

  @Column()
  id_receptor: number;

  @Column()
  id_mensaje: number;

  @ManyToOne(
    () => UsuarioRolEntity,
    usuario_rol => usuario_rol.emisor_receptor,
    { eager: false, onDelete: 'CASCADE' },
  )
  @JoinColumn({ name: 'id_emisor' })
  usuario_rol?: UsuarioRolEntity;

  @ManyToOne(
    () => MensajeEntity,
    mensaje => mensaje.emisor_receptor,
    { eager: false, onDelete: 'CASCADE' },
  )
  @JoinColumn({ name: 'id_mensaje' })
  mensaje?: MensajeEntity;

}