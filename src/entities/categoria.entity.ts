import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { MateriaEntity } from './materia.entity';

@Entity('categoria')
export class CategoriaEntity {

  @PrimaryGeneratedColumn({ type: 'integer' })
  id_categoria?: number;

  @Column({ nullable: true })
  nombre_categoria: string;

  @Column({ nullable: true })
  descripcion_categoria: string;

  @Column({ nullable: true })
  imagen_categoria: string;

  @OneToMany(
    () => MateriaEntity,
    materia => materia.categoria
  )
  materia?: MateriaEntity

  /*@ManyToOne(
    () => CursoEntity,
    curso => curso.categoria,
    { eager: true, onDelete: 'CASCADE' },
  )
  @JoinColumn({ name: 'id_curso' })
  curso?: CursoEntity | number;*/


}