export enum OptionQueryEnum {
  COUNT,
  SUM_BY_MONTH,
  SUM_BY_DAY,
  AVG,
  SUM,
  MIN,
  MAX
}

export enum FilterTypeEnum {
  CONTAIN,
  EQUAL,
  NOT_EQUAL,
}

export enum LogicalOperatorEnum {
  AND = 'Y',
  OR = 'O',
}

export enum OrderModeEnum {
  ASC,
  DESC,
}

export enum DatabaseEnum {
  LATINIUM = 1,
  DATAPASS = 2,
  PARQUEADEROS = 3,
}

export enum TypeDatabaseEnum {
  POSTGRES = 1,
  MYSQL = 2,
  SQLSERVER = 3
}