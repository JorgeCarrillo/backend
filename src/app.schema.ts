const joi = require('joi');

export class AppSchema {
  static readonly USUARIO_SCHEMA = joi
    .object()
    .keys({
      nombre: joi.string().min(2).max(100).required(),
      apellido: joi.string().min(2).max(100).required(),
      email: joi.string().min(2).max(100).required(),
      nick: joi.string().min(4).max(50).required(),
      password: joi.string().min(4).max(64).required(),
      numeroContacto: joi.string().min(7).max(12).required(),
      cedula: joi.string().min(4).max(64).required(),
      activo: joi.string().required(),
      rol: joi.string().min(2).max(25).required(),
      entidad: joi.string().required(),
    });

  static readonly AUTENTICACION_SCHEMA = joi
    .object()
    .keys({
      nombre_login: joi.string().alphanum().min(3).max(26).required(),
      password_login: joi.string().alphanum().min(3).max(32).required(),
    });
}
