import { BadRequestException, HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { JwtService } from './jwt.service';
import { UsuarioAutenticacionDto } from '../dtos/usuario-autenticacion.dto';
import { LoginUsuarioRepository } from '../repositories/login-usuario.repository';
import { UsuarioRolRepository } from '../repositories/usuario-rol.repository';
import { RolEntity } from '../entities/rol.entity';
import { UsuarioEntity } from '../entities/usuario.entity';
import { MoodleService } from './moodle.service';

@Injectable()
export class AutenticacionService {
  payload: any;

  constructor(
    private readonly loginUsuarioRepositorio: LoginUsuarioRepository,
    private readonly usuarioRolUsuario: UsuarioRolRepository,
    private readonly jwtService: JwtService,
    private readonly moodleService: MoodleService,
  ) {
  }

  async autenticarUsuario(usuarioAutenticacionDto: UsuarioAutenticacionDto) {
    const usuarioEntity = await this.loginUsuarioRepositorio.selectByName(usuarioAutenticacionDto.nombre_login);
    if (usuarioEntity) {
      if (usuarioAutenticacionDto.nombre_login === usuarioEntity.nombre_login && usuarioAutenticacionDto.password_login === usuarioEntity.password_login) {
        if (usuarioEntity.usuario.estado_usuario) {
          //if (usuarioEntity.usuario.estado_usuario_cuenta) {
          const rolEntity = await this.usuarioRolUsuario.selectByIdUsuario(usuarioEntity.id_usuario);
          if (rolEntity) {
            this.payload = {
              id_login: usuarioEntity.id_login,
              nombre_login: usuarioEntity.nombre_login,
              id_usuario: usuarioEntity.id_usuario,
              rol: (rolEntity.rol as RolEntity).nombre_rol,
              estado: (usuarioEntity.usuario as UsuarioEntity).estado_usuario,
              estado_cuenta: (usuarioEntity.usuario as UsuarioEntity).estado_usuario_cuenta,
              nombres: (usuarioEntity.usuario as UsuarioEntity).nombres_usuario,
              apellidos: (usuarioEntity.usuario as UsuarioEntity).apellidos_usuario,
              id_usuario_rol: rolEntity.id_usuario_rol,
              picture: (usuarioEntity.usuario as UsuarioEntity).foto_usuario,
            };
            return {
              accessToken: this.jwtService.emitirToken(this.payload),
              usuario: usuarioEntity.usuario as UsuarioEntity,
            };
          } else {
            throw new HttpException('¡Usted no posee ningún rol en el sistema!', HttpStatus.FORBIDDEN);
          }

          //} else {
          //  throw new HttpException('¡Cuenta no activada: el Adminsitrador le notificará cuando su cuenta sea activada!', HttpStatus.FORBIDDEN);
          //}
        } else {
          throw new HttpException('¡Su usuario aún no ha sido activado!', HttpStatus.FORBIDDEN);
        }
      } else {
        throw new HttpException('¡Credenciales inválidas!', HttpStatus.FORBIDDEN);
      }
    } else {
      const usuarioMoodle = await this.moodleService.getUserByEmail({
        email: usuarioAutenticacionDto.nombre_login,
      });

      if (usuarioMoodle && usuarioMoodle.users.length > 0 && usuarioEntity) {
        return {
          accessToken: this.jwtService.emitirToken(this.payload),
          usuario: usuarioEntity.usuario as UsuarioEntity,
        };
      } else {
        if (usuarioMoodle && usuarioMoodle.users.length > 0) {
          throw new BadRequestException(`¡Por favor ingresa una contraseña ${usuarioAutenticacionDto.nombre_login}!`);
        } else {
          throw new BadRequestException(`¡Usted no posee ninguna cuenta registrada en Moodle y Amauta con ${usuarioAutenticacionDto.nombre_login}!`);
        }
      }
    }
    throw new BadRequestException(`¡Usted no posee ninguna cuenta en Amauta registrada con ${usuarioAutenticacionDto.nombre_login}!`);
  }

  verificarToken(jwt: string, callback: (error, data) => void) {
    this.jwtService.verificarToken(jwt, (error, data) => {
      if (error) {
        callback({
          mensaje: '¡Jwt inválido!',
          error: error,
        }, null);
      } else {
        callback(null, {
          mensaje: 'Token válido',
          data: data,
        });
      }
    });
  }
}
