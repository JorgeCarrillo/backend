import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { UsuarioRolRepository } from '../repositories/usuario-rol.repository';
import { RolRepository } from '../repositories/rol.repository';
import { CrearCuentaDto } from '../dtos/create/crear-cuenta.dto';
import { CrearUsuarioDto } from '../dtos/create/crear-usuario.dto';
import { CrearLoginUsuarioDto } from '../dtos/create/crear-login-usuario.dto';
import { getConnection } from 'typeorm';
import { UsuarioEntity } from '../entities/usuario.entity';
import { LoginUsuarioEntity } from '../entities/login-usuario.entity';
import { UsuarioRolEntity } from '../entities/usuario-rol.entity';
import { AsigUserPerEntity } from '../entities/asig-user-per.entity';

@Injectable()
export class RegisterService {

  constructor(
    private readonly rolUsuarioRepository: UsuarioRolRepository,
    private readonly rolRepository: RolRepository,
  ) {
  }

  async createAccount(cuenta: CrearCuentaDto) {
    const connection = getConnection();
    const queryRunner = connection.createQueryRunner();
    const user: CrearUsuarioDto = cuenta.user;
    let login: CrearLoginUsuarioDto = cuenta.login;
    const nombre_rol = cuenta.nombre_rol;

    if (queryRunner && queryRunner != undefined)
      try {
        await queryRunner.connect();
        await queryRunner.startTransaction();
        const rol = await this.rolRepository.selectByName(nombre_rol)
          .then(result => result)
          .catch((e) => {
            throw (e);
          });
        const userInsert = await queryRunner.manager.insert(UsuarioEntity, user);
        login.id_usuario = userInsert.identifiers[0].id_usuario;
        await queryRunner.manager.insert(LoginUsuarioEntity, login);
        await queryRunner.manager.insert(UsuarioRolEntity, {
          idRol: rol.id_rol,
          usuario: login.id_usuario,
        });
        await queryRunner.commitTransaction();
        return {
          id_usuario: login.id_usuario
        }
      }
      catch (err) {
        //console.log(err)
        try {
          await queryRunner.rollbackTransaction();
          throw new HttpException('¡Error al intentar crear una nueva cuenta se realizo un rollback!', HttpStatus.FORBIDDEN);
        } catch (e) {
          throw new HttpException('¡Error al intentar crear una nueva cuenta!', HttpStatus.FORBIDDEN);
        }
      }
      finally {
        try {
          await queryRunner.release();
        } catch (e) {
          throw new HttpException('¡Error al intentar crear una nueva cuenta!', HttpStatus.FORBIDDEN);
        }
      }
  }

}
