import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { EmailService } from './email.service';
import { LoginUsuarioRepository } from '../repositories/login-usuario.repository';
import { LoginUsuarioEntity } from '../entities/login-usuario.entity';
import {sha256} from 'js-sha256';

@Injectable()
export class RecoveryPasswordService {

  constructor(
    private readonly _usuarioService: LoginUsuarioRepository,
    private readonly _emailService: EmailService
  ) {}

  async recoveryPassword(data) {

    var respuesta: LoginUsuarioEntity = await this._usuarioService.selectByName(data.nombre_login).then(result => result);
    if (respuesta) {
      return await this.changePasswordAndSendEmail(respuesta);
    } else {
      throw new HttpException('¡No existe ningún usuario registrado con el correo '+ data.nombre_login +' !', HttpStatus.FORBIDDEN);
    }
  }

  generatePassword() {
    var password: string = '';
    var lista_de_caracteres = '$+=?.@_0123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnpqrstuvwxyz';
    var longitud: number = 10;
    var aleatorio: number = 0;

    for (let i = 0; i <= longitud; i++) {
      aleatorio = Math.floor(Math.random() * 65);
      password += lista_de_caracteres.charAt(aleatorio);
    }
    return password;
  }

  async changePasswordAndSendEmail(respuesta: LoginUsuarioEntity) {
    var passNew = this.generatePassword();
    //respuesta.password_login = sha256(passNew);
    respuesta.password_login = passNew;
    var respuestaUpdate = await this._usuarioService.update(respuesta.id_login, {
      password_login: respuesta.password_login
    });
    if (respuestaUpdate.affected > 0) {
      await this._emailService.sendEmailPassword(respuesta.nombre_login, 'Recuperar Contraseña', 'Su nueva contraseña es: '+ passNew + '   ingrese al siguiente link: http://www.amautaec.education/assets/images/logos/LOGO_AMAUTA.png/pages/auth/reset-password');
      return respuestaUpdate;
    }
    else {
      throw new HttpException('¡No se pudo cambiar su contraseña comuníquese con el adminsitrador!', HttpStatus.FORBIDDEN);
    }
  }

}
