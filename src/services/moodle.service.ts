import { BadRequestException, HttpService, Injectable } from '@nestjs/common';
import { UsuarioAutenticacionMoodleDto } from '../dtos/usuario-autenticacion-moodle.dto';

@Injectable()
export class MoodleService {

  urlMoodle = 'http://lms.amautaec.education/lms/webservice/restjson/server.php';
  wstoken = 'f6000a351c5d987c98645d7246cdc967';

  constructor(
    private readonly _httpService: HttpService,
  ) {
  }

  async autenticationMoodle(credenciales: UsuarioAutenticacionMoodleDto) {

    /*Get Credenciales*/
    const res = await this.getToken('admin', 'Av123456@', 'ws_profesor');
    if (res && res.token) {

      return await this._httpService.post(this.urlMoodle, {
        'wstoken': res.token,
        'wsfunction': 'auth_userkey_request_login_url',
        'user': {
          'email': credenciales.email,
        },
      })
        .toPromise()
        .then(r => r.data)
        .catch(e => e);

    } else {
      throw new BadRequestException({
        mensaje: '¡No se pudo obtener un token para autenticar al usuario!',
        error: res.error,
      });
    }
  }

  /*Get Token*/
  async getToken(username, password, service) {
    return await this._httpService
      .get(`http://lms.amautaec.education/lms/login/token.php?username=${username}&password=${password}&service=${service}`)
      .toPromise()
      .then(r => r.data)
      .catch(e => e);
  }

  /*Get url auth*/
  async getUserByEmail(credenciales: UsuarioAutenticacionMoodleDto) {
    try {
      return await this._httpService
        .post(this.urlMoodle,
          {
            'wstoken': this.wstoken,
            'wsfunction': 'core_user_get_users',
            'criteria': [
              {
                'key': 'email',
                'value': credenciales.email,
              },
            ],
          },
        )
        .toPromise()
        .then(r => r.data)
        .catch(e => e);
    } catch (e) {
      new BadRequestException('Error al obtener la url para autenticación - moodle', e);
    }
  }

  /*Get course categories*/
  async getAllCategories() {
    try {
      return await this._httpService.post(this.urlMoodle, {
        'wstoken': this.wstoken,
        'wsfunction': 'core_course_get_categories',
      })
        .toPromise()
        .then(r => r.data)
        .catch(e => e);
    } catch (e) {
      new BadRequestException('Error al obtener las categorias - moodle', e);
    }
  }

  /*Get all courses*/
  async getAllCourses() {
    try {
      return await this._httpService.post(this.urlMoodle, {
        'wstoken': this.wstoken,
        'wsfunction': 'core_course_get_courses',
      })
        .toPromise()
        .then(r => r.data)
        .catch(e => e);
    } catch (e) {
      new BadRequestException('Error al obtener los cursos - moodle', e);
    }
  }

  /*Get all courses by fields*/
  async getAllCoursesByFields(field: string, value: string) {
    try {
      return await this._httpService.post(this.urlMoodle, {
        'wstoken': this.wstoken,
        'wsfunction': 'core_course_get_courses_by_field',
        "field": field,
        "value": value
      })
        .toPromise()
        .then(r => r.data)
        .catch(e => e);
    } catch (e) {
      new BadRequestException('Error al obtener los cursos - moodle', e);
    }
  }

  /*Get all course content by courseid*/
  async getAllCourseContent(courseid: number) {
    try {
      return await this._httpService.post(this.urlMoodle, {
        'wstoken': this.wstoken,
        'wsfunction': 'core_course_get_contents',
        'courseid': courseid,
      })
        .toPromise()
        .then(r => r.data)
        .catch(e => e);
    } catch (e) {
      new BadRequestException('Error al obtener el contenido de un curso - moodle', e);
    }
  }

  /*Get all content items by courseid*/
  async getAllContentItems(courseid: number) {
    try {
      return await this._httpService.post(this.urlMoodle, {
        'wstoken': this.wstoken,
        'wsfunction': 'core_course_get_course_content_items',
        'courseid': courseid,
      })
        .toPromise()
        .then(r => r.data)
        .catch(e => e);
    } catch (e) {
      new BadRequestException('Error al obtener el contenido de los items de un curso - moodle', e);
    }
  }

  /*Returns information about a course module*/
  async getCourseModule(cmid: number) {
    try {
      return await this._httpService.post(this.urlMoodle, {
        'wstoken': this.wstoken,
        'wsfunction': 'core_course_get_course_module',
        'cmid': cmid,
      })
        .toPromise()
        .then(r => r.data)
        .catch(e => e);
    } catch (e) {
      new BadRequestException('Error al obtener la información de un módulo de un curso by cmid - moodle', e);
    }
  }

  /*Returns information about a course module by modname and instance*/
  async getCourseModuleByInstance(instance: number, modname: string) {
    try {
      return await this._httpService.post(this.urlMoodle, {
        'wstoken': this.wstoken,
        'wsfunction': 'core_course_get_course_module_by_instance',
        'module': modname,
        'instance': instance,
      })
        .toPromise()
        .then(r => r.data)
        .catch(e => e);
    } catch (e) {
      new BadRequestException('Error al obtener la información de un módulo de un curso by instance and modname - moodle', e);
    }
  }

  /*Returns html with one activity module on course page*/
  async getHtmlModule(course_module_id: number) {
    try {
      return await this._httpService.post(this.urlMoodle, {
        'wstoken': this.wstoken,
        'wsfunction': 'core_course_get_module',
        'id': course_module_id,
      })
        .toPromise()
        .then(r => r.data)
        .catch(e => e);
    } catch (e) {
      new BadRequestException('Error al obtener el html de un curso - moodle', e);
    }
  }

  /*Get the list of courses where a user is enrolled in*/
  async getAllCoursesByAUser(user_id: number) {
    try {
      return await this._httpService.post(this.urlMoodle, {
        'wstoken': this.wstoken,
        'wsfunction': 'core_enrol_get_users_courses',
        'userid': user_id,
        'returnusercount': 1,
      })
        .toPromise()
        .then(r => r.data)
        .catch(e => e);
    } catch (e) {
      new BadRequestException('Error al obtener la lista de cursos inscritos de un usuario - moodle', e);
    }
  }

  /*Returns group details*/
  async getGroups(groupids: number[]) {
    try {
      return await this._httpService.post(this.urlMoodle, {
        'wstoken': this.wstoken,
        'wsfunction': 'core_group_get_groups',
        'groupids': groupids,
      })
        .toPromise()
        .then(r => r.data)
        .catch(e => e);
    } catch (e) {
      new BadRequestException('Error al obtener la información de determinados grupos - moodle', e);
    }
  }

  /*Returns group members.*/
  async getAllMembersByGroups(groupids: number[]) {
    try {
      return await this._httpService.post(this.urlMoodle, {
        'wstoken': this.wstoken,
        'wsfunction': 'core_group_get_group_members',
        'groupids': groupids,
      })
        .toPromise()
        .then(r => r.data)
        .catch(e => e);
    } catch (e) {
      new BadRequestException('Error al obtener los miembros de determinados cursos - moodle', e);
    }
  }

  /*Returns all notes in specified course (or site), for the specified user.*/
  async getNotesUserByCourse(courseid: number, userid: number) {
    try {
      return await this._httpService.post(this.urlMoodle, {
        'wstoken': this.wstoken,
        'wsfunction': 'core_notes_get_course_notes',
        'courseid': courseid,
        'userid': userid,
      })
        .toPromise()
        .then(r => r.data)
        .catch(e => e);
    } catch (e) {
      new BadRequestException('Error al obtener las notas de un usuario - moodle', e);
    }
  }

  /*Get notes*/
  async getNotesByIds(notes: number[]) {
    try {
      return await this._httpService.post(this.urlMoodle, {
        'wstoken': this.wstoken,
        'wsfunction': 'core_notes_get_notes',
        'notes': notes,
      })
        .toPromise()
        .then(r => r.data)
        .catch(e => e);
    } catch (e) {
      new BadRequestException('Error al obtener las notas solicitadas - moodle', e);
    }
  }

  /*Return user preferences - name => (name, empty, all)*/
  async getUserPreferences(name: string, userid: number) {
    try {
      return await this._httpService.post(this.urlMoodle, {
        'wstoken': this.wstoken,
        'wsfunction': 'core_user_get_user_preferences',
        'name': name,
        'userid': userid,
      })
        .toPromise()
        .then(r => r.data)
        .catch(e => e);
    } catch (e) {
      new BadRequestException('Error al obtener las preferencias de un usuario - moodle', e);
    }
  }

  /*search for users matching the parameters - construir de mejor forma*/
  async searchUserByParameters(criteria: any[]) {
    try {
      return await this._httpService.post(this.urlMoodle, {
        'wstoken': this.wstoken,
        'wsfunction': 'core_user_get_users',
        'criteria': criteria,
      })
        .toPromise()
        .then(r => r.data)
        .catch(e => e);
    } catch (e) {
      new BadRequestException('Error al buscar los usuario por ciertos parámetros - moodle', e);
    }
  }

  /*Retrieve users'information for a specified unique field - If you want to do a user search, use core_user_get_users()*/

  /*Definir los field por los cuales consultar*/
  async searchUserByField(field: string, values: any[]) {
    try {
      return await this._httpService.post(this.urlMoodle, {
        'wstoken': this.wstoken,
        'wsfunction': 'core_user_get_users_by_field',
        'field': field,
        'values': values,
      })
        .toPromise()
        .then(r => r.data)
        .catch(e => e);
    } catch (e) {
      new BadRequestException('Error al buscar los usuarios por determinado campo - moodle', e);
    }
  }

  /*Get the given user courses final grades*/
  async getFinalGradesByUser(userid: number) {
    try {
      return await this._httpService.post(this.urlMoodle, {
        'wstoken': this.wstoken,
        'wsfunction': 'gradereport_overview_get_course_grades',
        'userid': userid
      })
        .toPromise()
        .then(r => r.data)
        .catch(e => e);
    } catch (e) {
      new BadRequestException('Error al obtener las calificaciones de los usuarios - moodle', e);
    }
  }

  /*Get list users by course*/
  async getListUsersByCourseId(courseid: number) {
    try {
      return await this._httpService.post(this.urlMoodle, {
        'wstoken': this.wstoken,
        'wsfunction': 'core_enrol_get_enrolled_users',
        'courseid': courseid
      })
        .toPromise()
        .then(r => r.data)
        .catch(e => e);
    } catch (e) {
      new BadRequestException(`Error al obtener los usuarios del curso solicitado - moodle`, e);
    }
  }

  async getListId(courseid: number) {
    try {
      return await this._httpService.post(this.urlMoodle, {
        'wstoken': this.wstoken,
        'wsfunction': '',
        'courseid': courseid
      })
        .toPromise()
        .then(r => r.data)
        .catch(e => e);
    } catch (e) {
      new BadRequestException(`Error al obtener los usuarios del curso solicitado - moodle`, e);
    }
  }

}
