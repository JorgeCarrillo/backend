import { Injectable } from '@nestjs/common';

const jwtPaquete = require('jsonwebtoken');

@Injectable()
export class JwtService {
  private readonly secreto = 'WRADLMI2018jyd4e';
  private readonly jwt = jwtPaquete;
  private readonly tiempoVidaToken = '1d';

  emitirToken(payload: any) {
    return this.jwt.sign(
      {
        data: payload,
      },
      this.secreto,
      {
        expiresIn: this.tiempoVidaToken,
      },
    );
  }

  emitirTokenRegister(payload: any) {
    return this.jwt.sign(
      {
        data: payload,
      },
      this.secreto,
      {
        expiresIn: 60*5,
      },
    );
  }


  verificarToken(token: string, callback) {
    return this.jwt.verify(
      token,
      this.secreto,
      callback,
    );
  }

  verificarTokenSync(token: string) {
    try {
      return this.jwt.verify(token, this.secreto);
    } catch (e) {
      return false;
    }
  }

}
