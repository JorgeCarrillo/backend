import { Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/common';
import { Observable } from 'rxjs';
import { AxiosResponse } from 'axios';

@Injectable()
export class ZoomApiService {
  private TOKEN = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOm51bGwsImlzcyI6IkJYRDlGSVJnVGpxSkQwemdhNjlVRkEiLCJleHAiOjE2OTQ0MzkxODQsImlhdCI6MTU5NDQzMTc4NH0.X81J2br2JGmKBfJlojteSs2djbzJeSwhyyPYgs402gg';
  private URL_ZOOM = 'https://api.zoom.us/v2';
  idUser: any;

  constructor(private readonly _httpService: HttpService) {
  }

  getHttpUser(): Observable<AxiosResponse<any[]>> {
    return this._httpService.get(this.URL_ZOOM + '/users', {
      headers: {
        Authorization: 'Bearer' + this.TOKEN,
      },
    });
  }

  idUsuarioFun(body): void {
    //console.log(body)
    let ejem: any;
    ejem = body;
    this.idUser = ejem.idUsuario;
    //console.log(this.idUser);
  }

  //crear los meet
  postMeeting(body): Observable<AxiosResponse<any[]>> {

    return this._httpService.post(this.URL_ZOOM + '/users/' + body.idusuario + '/meetings', body, {
      headers: {
        Authorization: 'Bearer' + this.TOKEN,
      },
    });


  }
  //detalles los meet
  getMettingDetalle(idMeeting) {

    return this._httpService.get(this.URL_ZOOM + '/meetings/' + idMeeting, {
      headers: {
        Authorization: 'Bearer' + this.TOKEN,
      },
    });

  }
  //grabaciones los meet
  getMettingLive(idMeeting) {

    return this._httpService.get(this.URL_ZOOM + '/meetings/' + idMeeting + '/recordings', {
      headers: {
        Authorization: 'Bearer' + this.TOKEN,
      },
    });

  }
  //cambiar contraseña usuarios zoom
  putPassword(body): Observable<AxiosResponse<any[]>> {
    //console.log(this.idUser, body)
    if (this.idUser) {
      return this._httpService.put(this.URL_ZOOM + '/users/' + this.idUser + '/password', body, {
        headers: {
          Authorization: 'Bearer' + this.TOKEN,
        },
      });
    }

  }

}
