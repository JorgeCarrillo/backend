// @ts-ignore
import { Injectable, MiddlewareFunction, NestMiddleware } from '@nestjs/common';
import { AppLogger } from '../../app.logger';
import { AppConstant } from '../../app.constant';
import { AppUtil } from '../../app.util';

const fs = require('fs');

@Injectable()
export class LogMiddleware implements NestMiddleware {

  resolve(nivelDeLog: string): MiddlewareFunction {
    return (request, response, next) => {
      const peticion = {
        baseUrl: request.baseUrl,
        hostname: request.hostname,
        subdomains: request.subdomains,
        ip: request.ip,
        method: request.method,
        originalUrl: request.originalUrl,
        path: request.path,
        protocol: request.protocol,
        headers: request.headers,
      };

      switch (nivelDeLog) {
        case 'archivo':
          this.addRequestToFile(peticion);
          break;
        case 'consola':
          AppLogger.log(peticion);
          break;
        case 'todo':
          this.addRequestToFile(peticion);
          AppLogger.log(peticion);
          break;
        default:
          AppLogger.error('¡Nivel de Log desconocido!');
      }
      next();
    };
  }

  private addRequestToFile(peticion: any) {
    AppUtil.appendFile(AppConstant.LOGS_REQUESTS_ARCHIVO_PATH, `${new Date()}: ${JSON.stringify(peticion)}\n`);
  }

  use(req: Request, res: Response, next: () => void): any {
  }
}
