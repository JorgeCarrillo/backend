import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs/internal/Observable';
import { Reflector } from '@nestjs/core';
import { JwtService } from '../../services/jwt.service';
import { UsuarioRepository } from '../../repositories/usuario.repository';
import { UsuarioRolRepository } from '../../repositories/usuario-rol.repository';
import { RolEntity } from '../../entities/rol.entity';

@Injectable()
export class RolGuard implements CanActivate {
  constructor(
    private readonly reflector: Reflector,
    private readonly jwtService: JwtService,
    private readonly usuarioRepository: UsuarioRepository,
    private readonly usuarioRolUsuario: UsuarioRolRepository,
  ) {
  }

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();
    let jwtValue = request.headers.authorization;
    if (!jwtValue) {
      return false;
    } else {
      jwtValue = jwtValue.replace('Bearer ', '');
      const resultToken = this.jwtService.verificarTokenSync(jwtValue);
      if (resultToken) {
        /*return this.usuarioRepository
          .selectByCodigo(resultToken.data.id_usuario)
          .then(usuarioEntity => {
             return this.usuarioRolUsuario.selectByIdUsuario(usuarioEntity.id_usuario).then(
               (rolEntity) => {
                 if (usuarioEntity && usuarioEntity.estado_usuario  && (rolEntity.rol as RolEntity).nombre_rol === resultToken.data.rol) {
                    return (typeof (rolEntity.rol as RolEntity).permisos.find(it => (it.url_permiso === request.route.path && it.metodo === request.method)) !== 'undefined');
                   return true;
                 } else {
                   return false;
                 }
               }
             )
          });*/
        return true;
      } else {
        return false;
      }
    }
  }
}