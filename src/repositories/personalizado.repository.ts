import { Injectable } from '@nestjs/common';
import { DeleteResult, InsertResult, Repository, UpdateResult } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { PersonalizadoEntity } from '../entities/personalizado.entity';

@Injectable()
export class PersonalizadoRepository {

  constructor(
    @InjectRepository(PersonalizadoEntity)
    private readonly repository: Repository<PersonalizadoEntity>,
  ) {
  }

  async insert(entity: PersonalizadoEntity): Promise<InsertResult> {
    return this.repository.insert(entity);
  }
  

  async selectAll(): Promise<PersonalizadoEntity[]> {
    return await this.repository.find();
  }

  async selectById(id_tutoria: number): Promise<PersonalizadoEntity | undefined> {
    return await this.repository.findOne({ id_tutoria });
  }

  async update(id: number, fieldEntity: object): Promise<UpdateResult> {
    return this.repository.update(id, fieldEntity);
  }

  delete(id: number): Promise<DeleteResult> {
    return this.repository.delete(id);
  }

}