import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CiudadEntity } from '../entities/ciudad.entity';
import { DeleteResult, InsertResult, Repository, UpdateResult } from 'typeorm';

@Injectable()
export class CiudadRepository {

  constructor(
    @InjectRepository(CiudadEntity)
    private readonly repository: Repository<CiudadEntity>,
  ) {
  }

  async insert(entity: CiudadEntity): Promise<InsertResult> {
    return this.repository.insert(entity);
  }

  async selectAll(): Promise<CiudadEntity[]> {
    return await this.repository.find();
  }

  async selectById(id_ciudad: number): Promise<CiudadEntity | undefined> {
    return await this.repository.findOne({id_ciudad});
  }

  async update(id: number, fieldEntity: object): Promise<UpdateResult> {
    return this.repository.update(id, fieldEntity);
  }

  delete(id: number): Promise<DeleteResult> {
    return this.repository.delete(id);
  }

}