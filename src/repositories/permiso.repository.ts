import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, InsertResult, Repository, UpdateResult } from 'typeorm';
import { PermisoEntity } from '../entities/permiso.entity';

@Injectable()
export class PermisoRepository {
  constructor(
    @InjectRepository(PermisoEntity)
    private readonly repository: Repository<PermisoEntity>,
  ) {
  }

  insert(entity: PermisoEntity): Promise<InsertResult> {
    return this.repository.insert(entity);
  }

  selectAll(): Promise<PermisoEntity[]> {
    return this.repository.find();
  }

  selectById(id_permiso: number): Promise<PermisoEntity | undefined> {
    return this.repository.findOne({ id_permiso });
  }

  update(id: number, fieldEntity: object): Promise<UpdateResult> {
    return this.repository.update(id, fieldEntity);
  }

  delete(id: number): Promise<DeleteResult> {
    return this.repository.delete(id);
  }
}
