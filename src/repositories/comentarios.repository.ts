import { Injectable } from '@nestjs/common';
import { DeleteResult, InsertResult, Repository, UpdateResult } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { ComentariosEntity } from '../entities/comentarios.entity';

@Injectable()
export class ComentariosRepository {

  constructor(
    @InjectRepository(ComentariosEntity)
    private readonly repository: Repository<ComentariosEntity>,
  ) {
  }

  async insert(entity: ComentariosEntity): Promise<InsertResult> {
    return this.repository.insert(entity);
  }

  async selectAll(): Promise<ComentariosEntity[]> {
    return await this.repository.find();
  }

  async selectById(id_comentario: number): Promise<ComentariosEntity | undefined> {
    return await this.repository.findOne({ id_comentario });
  }
  async selectByIdProfesor(idprofesor: number): Promise<ComentariosEntity | undefined> {
    return await this.repository.query("select * from comentarios where id_usuario_rol_profesor = "+idprofesor);
  }

  async update(id: number, fieldEntity: object): Promise<UpdateResult> {
    return this.repository.update(id, fieldEntity);
  }

  delete(id: number): Promise<DeleteResult> {
    return this.repository.delete(id);
  }

}