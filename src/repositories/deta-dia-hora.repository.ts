import { Injectable } from '@nestjs/common';
import { DeleteResult, InsertResult, Repository, UpdateResult } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { DetaDiaHoraEntity } from '../entities/deta-dia-hora.entity';

@Injectable()
export class DetaDiaHoraRepository {

  constructor(
    @InjectRepository(DetaDiaHoraEntity)
    private readonly repository: Repository<DetaDiaHoraEntity>,
  ) {
  }

  async insert(entity: DetaDiaHoraEntity){
    return this.repository.query("select * from deta_dia_hora where id_usuario_rol='"+entity.id_usuario_rol +"' and hora_inicio='"+entity.hora_inicio+"'").then(res=>{
      //console.log("res",res.length);
      
        if(res.length==0){
        return this.repository.insert(entity)
         
       }
       
      })
  }

  async selectAll(): Promise<DetaDiaHoraEntity[]> {
    return await this.repository.find();
  }

  async selectById(id_deta_dia_hora: number): Promise<DetaDiaHoraEntity | undefined> {
    return await this.repository.findOne({ id_deta_dia_hora });
  }
  async selectByIdRol(idrol: number): Promise<DetaDiaHoraEntity | undefined> {
    return await this.repository.query("select * from deta_dia_hora where id_usuario_rol ="+idrol);
  }
  async deleteHora(idhora: number): Promise<DetaDiaHoraEntity | undefined> {
    return await this.repository.query("delete from deta_dia_hora where id_deta_dia_hora ="+idhora);
  }
  async actualizarEstado(idhora: number, estado:number): Promise<DetaDiaHoraEntity | undefined> {
    return await this.repository.query("UPDATE deta_dia_hora SET estado_reserva =" +estado+" WHERE id_deta_dia_hora="+idhora);
  }
  async validarHoras(idrol:number): Promise<DetaDiaHoraEntity | undefined> {
    return await this.repository.query("SELECT * FROM deta_dia_hora INNER JOIN pedido ON pedido.id_deta_dia_hora = deta_dia_hora.id_deta_dia_hora INNER JOIN detalle_pedido ON pedido.id_pedido = detalle_pedido.id_pedido where estado_reserva=2 and deta_dia_hora.id_usuario_rol="+idrol);
  }
  async update(id: number, fieldEntity: object): Promise<UpdateResult> {
    return this.repository.update(id, fieldEntity);
  }
  async selectByFecha(hora_inicio: string, id:number): Promise<DetaDiaHoraEntity | undefined> {
    //console.log("select * from deta_dia_hora  where hora_inicio >"+hora_inicio+" and estado_reserva=1 and id_usuario_rol="+id+"")
    return await this.repository.query("select * from deta_dia_hora where hora_inicio >"+hora_inicio+" and estado_reserva=1 and id_usuario_rol="+id+"");
  }

  delete(id: number): Promise<DeleteResult> {
    return this.repository.delete(id);
  }

}