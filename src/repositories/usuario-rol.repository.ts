import { Injectable } from '@nestjs/common';
import { DeleteResult, InsertResult, Repository, UpdateResult } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { UsuarioRolEntity } from '../entities/usuario-rol.entity';

@Injectable()
export class UsuarioRolRepository {

  constructor(
    @InjectRepository(UsuarioRolEntity)
    private readonly repository: Repository<UsuarioRolEntity>
  ) {
  }

  async insert(entity: UsuarioRolEntity): Promise<InsertResult> {
    return this.repository.insert(entity)
  }

  async selectAll(): Promise<UsuarioRolEntity[]> {
    return await this.repository.find();
  }

  async selectByIdUsuarioRol(id_usuario_rol: number): Promise<UsuarioRolEntity | undefined> {
    return await this.repository.findOne({id_usuario_rol})
  }
  async selectByRolIdUsuario(id_usuario: number): Promise<UsuarioRolEntity | undefined> {
    return await this.repository.query("select * from usuario_rol where id_usuario="+id_usuario)
  }

  async selectByUsuarioId(id: number): Promise<UsuarioRolEntity | undefined> {
    return await this.repository.findOne({
      select: ["id_usuario_rol", "idRol"],
      where: {
        usuario: id
      }
    })
  }

  async selectByIdUsuario(id: number): Promise<UsuarioRolEntity | undefined> {
    return await this.repository.findOne({
      usuario: id
    })
  }

  async update(id: number, fieldEntity: object): Promise<UpdateResult> {
    return this.repository.update(id, fieldEntity);
  }

  delete(id: number): Promise<DeleteResult> {
    return this.repository.delete(id);
  }

}
