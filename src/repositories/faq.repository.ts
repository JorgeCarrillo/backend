import { Injectable } from '@nestjs/common';
import { DeleteResult, InsertResult, Repository, UpdateResult } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { FaqEntity } from '../entities/faq.entity';

@Injectable()
export class FaqRepository {

  constructor(
    @InjectRepository(FaqEntity)
    private readonly repository: Repository<FaqEntity>,
  ) {
  }

  async insert(entity: FaqEntity): Promise<InsertResult> {
    return this.repository.insert(entity);
  }

  async selectAll(): Promise<FaqEntity[]> {
    return await this.repository.find();
  }

  async selectById(id_faq: number): Promise<FaqEntity | undefined> {
    return await this.repository.findOne({ id_faq });
  }

  async update(id: number, fieldEntity: object): Promise<UpdateResult> {
    return this.repository.update(id, fieldEntity);
  }

  delete(id: number): Promise<DeleteResult> {
    return this.repository.delete(id);
  }

}