import { BadRequestException, HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { LoginUsuarioEntity } from '../entities/login-usuario.entity';
import { DeleteResult, InsertResult, Repository, UpdateResult } from 'typeorm';
import { ActualizarPasswordDto } from '../dtos/update/actualizar-password.dto';

@Injectable()
export class LoginUsuarioRepository {

  constructor(
    @InjectRepository(LoginUsuarioEntity)
    private readonly repository: Repository<LoginUsuarioEntity>,
  ) {
  }

  async insert(entity: LoginUsuarioEntity): Promise<InsertResult> {
    return this.repository.insert(entity);
  }

  async selectAll(): Promise<LoginUsuarioEntity[]> {
    return await this.repository.find();
  }

  async selectById(id_login: number): Promise<LoginUsuarioEntity | undefined> {
    return await this.repository.findOne({ id_login });
  }

  async selectByName(nombre_login: string): Promise<LoginUsuarioEntity | undefined> {
    return await this.repository.findOne({ nombre_login });
  }

  async update(id: number, fieldEntity: object): Promise<UpdateResult> {
    return this.repository.update(id, fieldEntity);
  }

  async changeOldPassword(fieldEntity: ActualizarPasswordDto): Promise<any> {
    try {
      const user_login = await this.selectById(fieldEntity.id_login).catch(r => r);

      if (user_login) {

        if (user_login.password_login === fieldEntity.old_pass) {

          return await this.repository.update(fieldEntity.id_login, {
            password_login: fieldEntity.new_pass,
          });

        } else {
          throw new HttpException('La actual contraseña no es igual a la ingresada, la contraseña no se ha podido cambiar', HttpStatus.FORBIDDEN);
        }
      }

    } catch (e) {
      throw new BadRequestException({ mensaje: e.response ? e.response : `Error al obtener los datos sobre la cuenta del usuario` });
    }
  }

  delete(id: number): Promise<DeleteResult> {
    return this.repository.delete(id);
  }

}