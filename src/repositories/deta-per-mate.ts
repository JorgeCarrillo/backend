import { Injectable } from '@nestjs/common';
import { DeleteResult, InsertResult, Repository, UpdateResult } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { DetaPerMateEntity } from '../entities/deta-per-mate';

@Injectable()
export class DetaPerMateRepository {

  constructor(
    @InjectRepository(DetaPerMateEntity)
    private readonly repository: Repository<DetaPerMateEntity>,
  ) {
  }

  async insert(entity: DetaPerMateEntity): Promise<InsertResult> {
    return this.repository.insert(entity);
  }

  async selectAll(): Promise<DetaPerMateEntity[]> {
    return await this.repository.find();
  }

  async selectById(id_deta_per_mate: number): Promise<DetaPerMateEntity | undefined> {
    return await this.repository.findOne({ id_deta_per_mate });
  }

  async selectByIdTutoria(id_tutoria: number): Promise<DetaPerMateEntity | undefined> {
    return await this.repository.findOne({ id_tutoria });
  }

  async update(id: number, fieldEntity: object): Promise<UpdateResult> {
    return this.repository.update(id, fieldEntity);
  }

  delete(id: number): Promise<DeleteResult> {
    return this.repository.delete(id);
  }

}