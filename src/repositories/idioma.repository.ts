import { Injectable } from '@nestjs/common';
import { DeleteResult, InsertResult, Repository, UpdateResult } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { IdiomaEntity } from '../entities/idioma.entity';

@Injectable()
export class IdiomaRepository {

  constructor(
    @InjectRepository(IdiomaEntity)
    private readonly repository: Repository<IdiomaEntity>,
  ) {
  }

  async insert(entity: IdiomaEntity): Promise<InsertResult> {
    return this.repository.insert(entity);
  }

  async selectAll(): Promise<IdiomaEntity[]> {
    return await this.repository.find();
  }

  async selectTodo(): Promise<IdiomaEntity[]> {
    return await this.repository.query("SELECT DISTINCT idioma,nivel FROM idioma")
  }

  async selectById(id_idioma: number): Promise<IdiomaEntity | undefined> {
    return await this.repository.findOne({ id_idioma });
  }

  async selectByIdUsuario(id_usuario: number): Promise<IdiomaEntity[] | undefined> {

    return await this.repository.find({
      id_usuario: id_usuario
    });
  }

  async update(id: number, fieldEntity: object): Promise<UpdateResult> {
    return this.repository.update(id, fieldEntity);
  }

  delete(id: number): Promise<DeleteResult> {
    return this.repository.delete(id);
  }

}