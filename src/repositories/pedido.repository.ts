import { Injectable } from '@nestjs/common';
import { DeleteResult, InsertResult, Repository, UpdateResult } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { PedidoEntity } from '../entities/pedido.entity';

@Injectable()
export class PedidoRepository {

  constructor(
    @InjectRepository(PedidoEntity)
    private readonly repository: Repository<PedidoEntity>,
  ) {
  }

  async insert(entity: PedidoEntity): Promise<InsertResult> {
    return this.repository.insert(entity);
  }

  async selectAll(): Promise<PedidoEntity[]> {
    return await this.repository.query("SELECT * FROM pedido INNER JOIN detalle_pedido ON pedido.id_pedido = detalle_pedido.id_pedido;");
  }

  async selectById(id_pedido: number): Promise<PedidoEntity | undefined> {
    return await this.repository.findOne({ id_pedido });
  }
  async selectByIdRol(id_rol: number): Promise<PedidoEntity | undefined> {
    return await this.repository.query("SELECT * FROM pedido INNER JOIN detalle_pedido ON pedido.id_pedido = detalle_pedido.id_pedido where id_usuario_rol ="+id_rol);
  }
  async updateEstadoPedido(estado: Boolean, idestado:number): Promise<PedidoEntity | undefined> {
    return await this.repository.query("UPDATE pedido SET estado ="+ estado +" WHERE id_pedido= "+ idestado);
  }

  async ubdateByFecha(id:number,fecha:string,idhora:number): Promise<PedidoEntity[]> {
    //console.log("update reunion set fecha_inicio="+fecha+", id_deta_dia_hora="+idhora+"where id_pedido = "+id);
    
    return await this.repository.query("update pedido set fecha_inicio="+fecha+", id_deta_dia_hora="+idhora+"where id_pedido = "+id);
  }
  async getPedidosProf(fecha:string,idprof:number): Promise<PedidoEntity[]> {
    return await this.repository.query("select * from pedido where fecha_inicio >="+fecha+"and id_usuario_profesor="+idprof);
  }
 
 

  async update(id: number, fieldEntity: object): Promise<UpdateResult> {
    return this.repository.update(id, fieldEntity);
  }

  delete(id: number): Promise<DeleteResult> {
    return this.repository.delete(id);
  }

}