import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, InsertResult, Repository, UpdateResult } from 'typeorm';
import { CuentaZoomEntity } from '../entities/cuenta-zoom.entity';

@Injectable()
export class CuentaZoomRepository {

  constructor(
    @InjectRepository(CuentaZoomEntity)
    private readonly repository: Repository<CuentaZoomEntity>,
  ) {
  }

  async insert(entity: CuentaZoomEntity): Promise<InsertResult> {
    return this.repository.insert(entity);
  }

  async selectAll(): Promise<CuentaZoomEntity[]> {
    return await this.repository.find();
  }

  async selectById(id_cuenta_zoom: number): Promise<CuentaZoomEntity | undefined> {
    return await this.repository.findOne({ id_cuenta_zoom });
  }

  async update(id: number, fieldEntity: object): Promise<UpdateResult> {
    return this.repository.update(id, fieldEntity);
  }

  delete(id: number): Promise<DeleteResult> {
    return this.repository.delete(id);
  }

}