import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ConfiguracionEntity } from '../entities/configuracion.entity';
import { DeleteResult, InsertResult, Repository, UpdateResult } from 'typeorm';

@Injectable()
export class ConfiguracionRepository {

  constructor(
    @InjectRepository(ConfiguracionEntity)
    private readonly repository: Repository<ConfiguracionEntity>
  ) {
  }

  async insert(entity: ConfiguracionEntity): Promise<InsertResult> {
    return this.repository.insert(entity)
  }

  async selectAll(): Promise<ConfiguracionEntity[]> {
    return await this.repository.find();
  }

  async selectById(id_configuracion: number): Promise<ConfiguracionEntity | undefined> {
    return await this.repository.findOne({id_configuracion})
  }

  async update(id: number, fieldEntity: object): Promise<UpdateResult> {
    return this.repository.update(id, fieldEntity);
  }

  delete(id: number): Promise<DeleteResult> {
    return this.repository.delete(id);
  }

}