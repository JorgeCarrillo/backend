import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { EstadoEntity } from '../entities/estado.entity';
import { DeleteResult, InsertResult, Repository, UpdateResult } from 'typeorm';

@Injectable()
export class EstadoRepository {

  constructor(
    @InjectRepository(EstadoEntity)
    private readonly repository: Repository<EstadoEntity>
  ){}

  async insert(entity: EstadoEntity): Promise<InsertResult> {
    return this.repository.insert(entity)
  }

  async selectAll(): Promise<EstadoEntity[]> {
    return await this.repository.find();
  }

  async selectById(id_estado: number): Promise<EstadoEntity | undefined> {
    return await this.repository.findOne({id_estado})
  }

  async update(id: number, fieldEntity: object): Promise<UpdateResult> {
    return this.repository.update(id, fieldEntity);
  }

  delete(id: number): Promise<DeleteResult> {
    return this.repository.delete(id);
  }

}