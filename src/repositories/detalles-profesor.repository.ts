import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DetallesProfesorEntity } from '../entities/detalles-profesor.entity';
import { DeleteResult, InsertResult, Repository, UpdateResult } from 'typeorm';

@Injectable()
export class DetallesProfesorRepository {

  constructor(
    @InjectRepository(DetallesProfesorEntity)
    private readonly repository: Repository<DetallesProfesorEntity>,
  ) {
  }

  async insert(entity: DetallesProfesorEntity): Promise<InsertResult> {
    return this.repository.insert(entity)
  }

  async selectAll(): Promise<DetallesProfesorEntity[]> {
    return await this.repository.find();
  }

  async selectById(id_deta_profe: number): Promise<DetallesProfesorEntity | undefined> {
    return await this.repository.findOne({id_deta_profe})
  }

  async selectByIdUsuarioRol(id_usuario_rol: number): Promise<DetallesProfesorEntity | undefined> {
    return await this.repository.findOne({id_usuario_rol})
  }

  async update(id: number, fieldEntity: object): Promise<UpdateResult> {
    return this.repository.update(id, fieldEntity);
  }

  delete(id: number): Promise<DeleteResult> {
    return this.repository.delete(id);
  }

}