import { Injectable } from '@nestjs/common';
import { DeleteResult, InsertResult, Repository, UpdateResult } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { TajetaPagoEntity } from './../entities/tarjeta-pago.entity';

@Injectable()
export class TajetaPagoRepository {

  constructor(
    @InjectRepository(TajetaPagoEntity)
    private readonly repository: Repository<TajetaPagoEntity>,
  ) {
  }

  async insert(entity: TajetaPagoEntity): Promise<InsertResult> {
    return this.repository.insert(entity);
  }

  async selectAll(): Promise<TajetaPagoEntity[]> {
    return await this.repository.find();
  }

  async selectById(id_tarjeta_pago: number): Promise<TajetaPagoEntity | undefined> {
    return await this.repository.findOne({ id_tarjeta_pago });
  }

  async selectByIdRol(idrol: number): Promise<TajetaPagoEntity | undefined> {
    return await this.repository.query("select * from tarjeta_pago where id_usuario_rol =" + idrol);
  }

  async update(id: number, fieldEntity: object): Promise<UpdateResult> {
    return this.repository.update(id, fieldEntity);
  }

  delete(id: number): Promise<DeleteResult> {
    return this.repository.delete(id);
  }

}