import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectEntityManager, InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, EntityManager, Repository } from 'typeorm';
import { RolEntity } from '../entities/rol.entity';
import { CrearRolDto } from '../dtos/create/crear-rol.dto';
import { PermisoEntity } from '../entities/permiso.entity';

@Injectable()
export class RolRepository {
  constructor(
    @InjectRepository(RolEntity) private readonly repository: Repository<RolEntity>,
    @InjectRepository(PermisoEntity) private readonly repositoryPermiso: Repository<PermisoEntity>,
    @InjectEntityManager() private readonly entityManager: EntityManager,
  ) {
  }

  async insert(crearRolDto: CrearRolDto): Promise<RolEntity> {
    try {
      const rolEntity: RolEntity = {
        nombre_rol: crearRolDto.nombre_rol,
        descripcion: crearRolDto.descripcion,
      };
      rolEntity.permisos = await this.repositoryPermiso.findByIds(crearRolDto.idsPermisos);
      await this.repository.save(rolEntity);
      return rolEntity;
    } catch (e) {
      throw new BadRequestException(e.toString());
    }
  }

  selectAll(): Promise<RolEntity[]> {
    return this.repository.find();
  }

  selectById(id_rol: number): Promise<RolEntity | undefined> {
    return this.repository.findOne({ id_rol });
  }

  selectByName(nombre_rol: string): Promise<RolEntity | undefined> {
    return this.repository.findOne({ nombre_rol });
  }

  async update(id: number, fieldEntity: any) {
    const entidad = await this.repository.findOne(id);
    entidad.nombre_rol = fieldEntity.nombre_rol;
    entidad.descripcion = fieldEntity.descripcion;
    entidad.permisos = fieldEntity.idsPermisos.map(it => ({ id_permiso: it }));
    return this.repository.save(entidad);
  }

  delete(id: number): Promise<DeleteResult> {
    return this.repository.delete(id);
  }
}
