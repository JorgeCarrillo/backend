"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.UsuarioRepository = void 0;
var common_1 = require("@nestjs/common");
var typeorm_1 = require("@nestjs/typeorm");
var usuario_entity_1 = require("../entities/usuario.entity");
var js_sha256_1 = require("js-sha256");
var UsuarioRepository = /** @class */ (function () {
    function UsuarioRepository(repository, detalleService, usuarioRolService, rolService, _emailService) {
        this.repository = repository;
        this.detalleService = detalleService;
        this.usuarioRolService = usuarioRolService;
        this.rolService = rolService;
        this._emailService = _emailService;
    }
    UsuarioRepository.prototype.insert = function (entity) {
        return __awaiter(this, void 0, Promise, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.repository.insert(entity)];
            });
        });
    };
    UsuarioRepository.prototype.actualizarUsuario = function (entity) {
        return __awaiter(this, void 0, Promise, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        //console.log("UPDATE usuario SET nombres_usuario='" + entity.nombres_usuario + "',apellidos_usuario='" + entity.apellidos_usuario + "',identificacion_usuario='" + entity.identificacion_usuario + "',fecha_naci_usuario='" + entity.fecha_naci_usuario + "',telefono_usuario='" + entity.telefono_usuario + "',direccion_usuario='" + entity.direccion_usuario + "',ciudad_nombre='" + entity.ciudad_nombre + "',id_ciudad='" + entity.id_ciudad + "' where id_usuario= " + entity.id_usuario);
                        this.repository.query("UPDATE usuario SET nombres_usuario='" + entity.nombres_usuario + "',apellidos_usuario='" + entity.apellidos_usuario + "',identificacion_usuario='" + entity.identificacion_usuario + "',fecha_naci_usuario='" + entity.fecha_naci_usuario + "',telefono_usuario='" + entity.telefono_usuario + "',direccion_usuario='" + entity.direccion_usuario + "',ciudad_nombre='" + entity.ciudad_nombre + "',id_ciudad='" + entity.id_ciudad + "',sexo_usuario='" + entity.sexo_usuario + "' where id_usuario= " + entity.id_usuario);
                        return [4 /*yield*/, this.repository.query("UPDATE usuario SET nombres_usuario='" + entity.nombres_usuario + "',apellidos_usuario='" + entity.apellidos_usuario + "',identificacion_usuario='" + entity.identificacion_usuario + "',fecha_naci_usuario='" + entity.fecha_naci_usuario + "',telefono_usuario='" + entity.telefono_usuario + "',direccion_usuario='" + entity.direccion_usuario + "',ciudad_nombre='" + entity.ciudad_nombre + "',id_ciudad='" + entity.id_ciudad + "',sexo_usuario='" + entity.sexo_usuario + "' where id_usuario= " + entity.id_usuario)];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    UsuarioRepository.prototype.crearTokenRecuperarContraseña = function (data) {
        var codigo;
        var caracteres = "ABCDEFGHJKMNPQRTUVWXYZ2346789";
        var contraseña = "";
        for (var i = 0; i < 6; i++)
            contraseña += caracteres.charAt(Math.floor(Math.random() * caracteres.length));
        codigo = contraseña;
        var query = "UPDATE usuario SET codigo_password='" + contraseña + "' where correo_usuario='" + data.usuario + "'";
        //console.log("queru", query);
        this.repository.query("UPDATE usuario SET codigo_password='" + js_sha256_1.sha256(contraseña) + "', estado_codigo=true where correo_usuario='" + data.usuario + "'");
        return this._emailService.sendCodigoContraseña(codigo, data).then(function (res) {
            return res;
        });
    };
    UsuarioRepository.prototype.cambiarContraseña = function (body) {
        var _this = this;
        return this.repository.query("select * from usuario where correo_usuario='" + body.correo + "' and codigo_password='" + body.codigo + "' and estado_codigo=true").then(function (res) {
            if (res.length > 0) {
                _this.repository.query("update login_usuario set password_login='" + body.contraseña + "' where nombre_login='" + body.correo + "'");
                _this.repository.query("update usuario set estado_codigo = false where correo_usuario='" + body.correo + "'");
                return true;
            }
            else {
                return false;
            }
        });
    };
    UsuarioRepository.prototype.cancelarClase = function (body) {
        return this._emailService.sendNotificacionMeetingCancel(body).then(function (res) {
            return res;
        });
    };
    UsuarioRepository.prototype.selectAll = function () {
        return __awaiter(this, void 0, Promise, function () {
            var usuariosEntities;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.repository.find()];
                    case 1:
                        usuariosEntities = _a.sent();
                        return [2 /*return*/, usuariosEntities];
                }
            });
        });
    };
    UsuarioRepository.prototype.selectById = function (id_usuario) {
        return this.repository.findOne({ id_usuario: id_usuario });
    };
    UsuarioRepository.prototype.selectByIdRol = function (id_usuario_rol) {
        return this.repository.query("select * from usuario INNER JOIN usuario_rol on usuario_rol.id_usuario=usuario.id_usuario where usuario_rol.id_usuario_rol=" + id_usuario_rol);
    };
    UsuarioRepository.prototype.selectByHoraInicio = function (hora) {
        return this.repository.query("SELECT * from  usuario INNER JOIN usuario_rol ON usuario.id_usuario = usuario_rol.id_usuario INNER JOIN deta_dia_hora ON usuario_rol.id_usuario_rol = deta_dia_hora.id_usuario_rol where deta_dia_hora.hora_inicio>" + hora);
    };
    UsuarioRepository.prototype.updateConexion = function (hora, idusuario) {
        return this.repository.query("update  usuario set ultima_conexion=" + hora + " where id_usuario=" + idusuario);
    };
    UsuarioRepository.prototype.updateFoto = function (nombre, idusuario) {
        return this.repository.query("update  usuario set foto_usuario=" + nombre + " where id_usuario=" + idusuario);
    };
    UsuarioRepository.prototype.updateVideo = function (nombre, idusuario) {
        return this.repository.query("update  detalles_profesor set video_presentacion=" + nombre + " where id_usuario_rol=" + idusuario);
    };
    UsuarioRepository.prototype.precioMaximo = function () {
        return this.repository.query("SELECT MAX(preciofijo) preciofijo FROM detalles_profesor;");
    };
    UsuarioRepository.prototype.selectAllTeachers = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.repository.query("SELECT * FROM public.usuario_rol as ru join public.usuario as u on ru.id_usuario =  u.id_usuario join public.rol as r on r.id_rol = ru.id_rol where r.nombre_rol = 'PROFESOR'").then(function (res) { return __awaiter(_this, void 0, void 0, function () {
                            var b, _i, res_1, a, _a;
                            var _this = this;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        _i = 0, res_1 = res;
                                        _b.label = 1;
                                    case 1:
                                        if (!(_i < res_1.length)) return [3 /*break*/, 4];
                                        a = res_1[_i];
                                        _a = a;
                                        return [4 /*yield*/, this.promedio(a.id_usuario_rol).then(function (ress) { return __awaiter(_this, void 0, void 0, function () {
                                                var b;
                                                return __generator(this, function (_a) {
                                                    ////console.log("resddfsd",ress.length)
                                                    if (ress.length > 0) {
                                                        b = ress[0];
                                                    }
                                                    else {
                                                        b = {
                                                            "promedio": "1.0000000000000000"
                                                        };
                                                    }
                                                    return [2 /*return*/, b];
                                                });
                                            }); })];
                                    case 2:
                                        _a.estrellas = _b.sent();
                                        _b.label = 3;
                                    case 3:
                                        _i++;
                                        return [3 /*break*/, 1];
                                    case 4: return [2 /*return*/, res];
                                }
                            });
                        }); })];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    UsuarioRepository.prototype.promedio = function (idrol) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.repository.query("SELECT  avg(valoracion) as promedio FROM comentarios where id_usuario_rol_profesor=" + idrol + " GROUP BY id_usuario_rol_profesor")];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    UsuarioRepository.prototype.update = function (id, fieldEntity) {
        return __awaiter(this, void 0, Promise, function () {
            var usuario_rol, rol, dir, res;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.usuarioRolService.selectByUsuarioId(id)];
                    case 1:
                        usuario_rol = _a.sent();
                        return [4 /*yield*/, this.rolService.selectById(usuario_rol.idRol)];
                    case 2:
                        rol = _a.sent();
                        return [4 /*yield*/, this.createDirectory(fieldEntity.nombres_usuario, fieldEntity.apellidos_usuario, fieldEntity.identificacion_usuario)];
                    case 3:
                        dir = _a.sent();
                        if (!(rol.nombre_rol == 'PROFESOR' && dir)) return [3 /*break*/, 6];
                        return [4 /*yield*/, this.detalleService.selectByIdUsuarioRol(usuario_rol.id_usuario_rol)];
                    case 4:
                        res = _a.sent();
                        if (!!res) return [3 /*break*/, 6];
                        return [4 /*yield*/, this.detalleService.insert({
                                cv_deta_profe: null,
                                penales_deta_profe: null,
                                descri_deta_profe: null,
                                ruta_deta_profe: dir,
                                id_usuario_rol: usuario_rol.id_usuario_rol
                            })];
                    case 5:
                        _a.sent();
                        _a.label = 6;
                    case 6: return [2 /*return*/, this.repository.update(id, fieldEntity)];
                }
            });
        });
    };
    UsuarioRepository.prototype["delete"] = function (id) {
        return this.repository["delete"](id);
    };
    UsuarioRepository.prototype.createDirectory = function (nombre, apellido, ci) {
        if (nombre && apellido && ci) {
            var dir = ci + '_' + nombre + '_' + apellido;
            return dir;
        }
        else {
            return null;
        }
    };
    UsuarioRepository = __decorate([
        common_1.Injectable(),
        __param(0, typeorm_1.InjectRepository(usuario_entity_1.UsuarioEntity))
    ], UsuarioRepository);
    return UsuarioRepository;
}());
exports.UsuarioRepository = UsuarioRepository;
