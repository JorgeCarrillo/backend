import { Injectable } from '@nestjs/common';
import { DeleteResult, InsertResult, Repository, UpdateResult } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { PaisEntity } from '../entities/pais.entity';

@Injectable()
export class PaisRepository {

  constructor(
    @InjectRepository(PaisEntity)
    private readonly repository: Repository<PaisEntity>
  ) {
  }

  async insert(entity: PaisEntity): Promise<InsertResult> {
    return this.repository.insert(entity)
  }

  async selectAll(): Promise<PaisEntity[]> {
    return await this.repository.find();
  }

  async selectById(id_pais: number): Promise<PaisEntity | undefined> {
    return await this.repository.findOne({id_pais})
  }

  async update(id: number, fieldEntity: object): Promise<UpdateResult> {
    return this.repository.update(id, fieldEntity);
  }

  delete(id: number): Promise<DeleteResult> {
    return this.repository.delete(id);
  }

}