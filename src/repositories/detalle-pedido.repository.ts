import { Injectable } from '@nestjs/common';
import { DeleteResult, InsertResult, Repository, UpdateResult } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { DetallePedidoEntity } from '../entities/detalle-pedido.entity';

@Injectable()
export class DetallePedidoRepository {

  constructor(
    @InjectRepository(DetallePedidoEntity)
    private readonly repository: Repository<DetallePedidoEntity>,
  ) {
  }

  async insert(entity: DetallePedidoEntity): Promise<InsertResult> {
    return this.repository.insert(entity);
  }

  async selectAll(): Promise<DetallePedidoEntity[]> {
    return await this.repository.find();
  }

  async selectById(id_detalle_pedido: number): Promise<DetallePedidoEntity | undefined> {
    return await this.repository.findOne({ id_detalle_pedido });
  }

  async selectByIdPedido(id_pedido: number): Promise<DetallePedidoEntity | undefined> {
    return await this.repository.query("select * from detalle_pedido where id_pedido = " + id_pedido);
  }




  async update(id: number, fieldEntity: object): Promise<UpdateResult> {
    return this.repository.update(id, fieldEntity);
  }

  delete(id: number): Promise<DeleteResult> {
    return this.repository.delete(id);
  }

}