import { Injectable } from '@nestjs/common';
import { DeleteResult, InsertResult, Repository, UpdateResult } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { ReunionEntity } from '../entities/reunion.entity';

@Injectable()
export class ReunionRepository {

  constructor(
    @InjectRepository(ReunionEntity)
    private readonly repository: Repository<ReunionEntity>,
  ) {
  }

  async insert(entity: ReunionEntity): Promise<InsertResult> {
    return this.repository.insert(entity);
  }

  async selectAll(): Promise<ReunionEntity[]> {
    return await this.repository.find();
  }
  async selectByfecha_inicio(fecha: string): Promise<ReunionEntity[]> {
    return await this.repository.query("select * from reunion where fecha_inicio = " + fecha);
  }
  async ubdateByFecha(id: number, fecha: string): Promise<ReunionEntity[]> {

    return await this.repository.query("update reunion set fecha_inicio=" + fecha + ",estado='false'" + "where id_pedido = " + id);
  }

  async selectByIdActivate(id_activado: number): Promise<ReunionEntity | undefined> {
    return await this.repository.findOne({ id_activado });
  }
  async selectByPedido(id: number): Promise<ReunionEntity | undefined> {
    return await this.repository.query("select * from reunion where id_pedido=" + id + " and fecha_inicio !='Cancelada'");
  }
  async selectByIdReunion(id_reunion: string): Promise<ReunionEntity | undefined> {
    return await this.repository.findOne({ id_reunion });
  }

  async update(id: number, fieldEntity: object): Promise<UpdateResult> {
    return this.repository.update(id, fieldEntity);
  }

  delete(id: number): Promise<DeleteResult> {
    return this.repository.delete(id);
  }

}