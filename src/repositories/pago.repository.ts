import { Injectable } from '@nestjs/common';
import { DeleteResult, InsertResult, Repository, UpdateResult } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { PagoEntity } from '../entities/pago.entity';

@Injectable()
export class PagoRepository {

  constructor(
    @InjectRepository(PagoEntity)
    private readonly repository: Repository<PagoEntity>,
  ) {
  }

  async insert(entity: PagoEntity): Promise<InsertResult> {
    return this.repository.insert(entity);
  }

  async selectAll(): Promise<PagoEntity[]> {
    return await this.repository.find();
  }

  async selectById(id_pago: number): Promise<PagoEntity | undefined> {
    return await this.repository.findOne({ id_pago });
  }

  async update(id: number, fieldEntity: object): Promise<UpdateResult> {
    return this.repository.update(id, fieldEntity);
  }

  delete(id: number): Promise<DeleteResult> {
    return this.repository.delete(id);
  }

}