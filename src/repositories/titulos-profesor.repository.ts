import { Injectable } from '@nestjs/common';
import { DeleteResult, InsertResult, Like, Repository, UpdateResult } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { TitulosProfesorEntity } from '../entities/titulos-profesor.entity';

@Injectable()
export class TitulosProfesorRepository {

  constructor(
    @InjectRepository(TitulosProfesorEntity)
    private readonly repository: Repository<TitulosProfesorEntity>,
  ) {
  }

  async insert(entity: TitulosProfesorEntity): Promise<InsertResult> {
    return this.repository.insert(entity);
  }

  async selectAll(): Promise<TitulosProfesorEntity[]> {
    return await this.repository.find();
  }

  async selectById(id_titulos_profe: number): Promise<TitulosProfesorEntity | undefined> {
    return await this.repository.findOne({ id_titulos_profe });
  }

  async selectByIdUsuarioRol(id_usuario_rol: number): Promise<TitulosProfesorEntity[] | undefined> {

    return await this.repository.find({
      id_usuario_rol: id_usuario_rol,
      senescyt_titulos_profe: Like('%TIT%'),
    });
  }

  async update(id: number, fieldEntity: object): Promise<UpdateResult> {
    return this.repository.update(id, fieldEntity);
  }

  delete(id: number): Promise<DeleteResult> {
    return this.repository.delete(id);
  }

}