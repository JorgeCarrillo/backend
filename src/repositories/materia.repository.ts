import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MateriaEntity } from '../entities/materia.entity';
import { DeleteResult, InsertResult, Repository, UpdateResult } from 'typeorm';

@Injectable()
export class MateriaRepository {

  constructor(
    @InjectRepository(MateriaEntity)
    private readonly repository: Repository<MateriaEntity>
  ) {
  }

  async insert(entity: MateriaEntity): Promise<InsertResult> {
    return this.repository.insert(entity)
  }

  async selectAll(): Promise<MateriaEntity[]> {
    return await this.repository.find();
  }

  async selectById(id_materia: number): Promise<MateriaEntity | undefined> {
    return await this.repository.findOne({id_materia})
  }

  async update(id: number, fieldEntity: object): Promise<UpdateResult> {
    return this.repository.update(id, fieldEntity);
  }

  delete(id: number): Promise<DeleteResult> {
    return this.repository.delete(id);
  }

}