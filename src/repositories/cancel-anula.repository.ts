import { Injectable } from '@nestjs/common';
import { DeleteResult, InsertResult, Repository, UpdateResult } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { CancelAnulaEntity } from '../entities/cancel-anula.entity';

@Injectable()
export class CancelAnulaRepository {

  constructor(
    @InjectRepository(CancelAnulaEntity)
    private readonly repository: Repository<CancelAnulaEntity>,
  ) {
  }

  async insert(entity: CancelAnulaEntity): Promise<InsertResult> {
    return this.repository.insert(entity);
  }

  async selectAll(): Promise<CancelAnulaEntity[]> {
    return await this.repository.query("select * from cancel_anula");
  }

  async selectById(id_cambia_anula: number): Promise<CancelAnulaEntity | undefined> {
    return await this.repository.findOne({ id_cambia_anula });
  }



  async update(id: number, fieldEntity: object): Promise<UpdateResult> {
    return this.repository.update(id, fieldEntity);
  }

  delete(id: number): Promise<DeleteResult> {
    return this.repository.delete(id);
  }

}