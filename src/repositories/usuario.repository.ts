import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, InsertResult, Repository, UpdateResult } from 'typeorm';
import { UsuarioEntity } from '../entities/usuario.entity';
import { DetallesProfesorRepository } from './detalles-profesor.repository';
import { UsuarioRolRepository } from './usuario-rol.repository';
import { RolRepository } from './rol.repository';
import { log } from 'console';
import { EmailService } from '../services/email.service';
import { sha256 } from 'js-sha256';

@Injectable()
export class UsuarioRepository {

  constructor(
    @InjectRepository(UsuarioEntity)
    private readonly repository: Repository<UsuarioEntity>,
    private readonly detalleService: DetallesProfesorRepository,
    private readonly usuarioRolService: UsuarioRolRepository,
    private readonly rolService: RolRepository,
    private readonly _emailService: EmailService,
  ) {
  }

  async insert(entity: UsuarioEntity): Promise<InsertResult> {
    return this.repository.insert(entity);
  }
  async actualizarUsuario(entity): Promise<InsertResult> {
    //console.log("UPDATE usuario SET nombres_usuario='" + entity.nombres_usuario + "',apellidos_usuario='" + entity.apellidos_usuario + "',identificacion_usuario='" + entity.identificacion_usuario + "',fecha_naci_usuario='" + entity.fecha_naci_usuario + "',telefono_usuario='" + entity.telefono_usuario + "',direccion_usuario='" + entity.direccion_usuario + "',ciudad_nombre='" + entity.ciudad_nombre + "',id_ciudad='" + entity.id_ciudad + "' where id_usuario= " + entity.id_usuario)
    this.repository.query("UPDATE usuario SET nombres_usuario='" + entity.nombres_usuario + "',apellidos_usuario='" + entity.apellidos_usuario + "',identificacion_usuario='" + entity.identificacion_usuario + "',fecha_naci_usuario='" + entity.fecha_naci_usuario + "',telefono_usuario='" + entity.telefono_usuario + "',direccion_usuario='" + entity.direccion_usuario + "',ciudad_nombre='" + entity.ciudad_nombre + "',id_ciudad='" + entity.id_ciudad + "',sexo_usuario='" + entity.sexo_usuario + "' where id_usuario= " + entity.id_usuario);
    return await this.repository.query("UPDATE usuario SET nombres_usuario='" + entity.nombres_usuario + "',apellidos_usuario='" + entity.apellidos_usuario + "',identificacion_usuario='" + entity.identificacion_usuario + "',fecha_naci_usuario='" + entity.fecha_naci_usuario + "',telefono_usuario='" + entity.telefono_usuario + "',direccion_usuario='" + entity.direccion_usuario + "',ciudad_nombre='" + entity.ciudad_nombre + "',id_ciudad='" + entity.id_ciudad + "',sexo_usuario='" + entity.sexo_usuario + "' where id_usuario= " + entity.id_usuario);
  }
  crearTokenRecuperarContraseña(data) {
    let codigo
    var caracteres = "ABCDEFGHJKMNPQRTUVWXYZ2346789";
    var contraseña = "";
    for (let i = 0; i < 6; i++) contraseña += caracteres.charAt(Math.floor(Math.random() * caracteres.length));
    codigo = contraseña

    var query = "UPDATE usuario SET codigo_password='" + contraseña + "' where correo_usuario='" + data.usuario + "'";
    //console.log("queru", query);
    this.repository.query("UPDATE usuario SET codigo_password='" + sha256(contraseña) + "', estado_codigo=true where correo_usuario='" + data.usuario + "'")
    return this._emailService.sendCodigoContraseña(codigo, data).then(res => {
      return res
    })



  }
  cambiarContraseña(body) {
    return this.repository.query("select * from usuario where correo_usuario='" + body.correo + "' and codigo_password='" + body.codigo + "' and estado_codigo=true").then(res => {

      if (res.length > 0) {
        this.repository.query("update login_usuario set password_login='" + body.contraseña + "' where nombre_login='" + body.correo + "'")
        this.repository.query("update usuario set estado_codigo = false where correo_usuario='" + body.correo + "'")
        return true
      }
      else {
        return false
      }
    })

  }
  cancelarClase(body) {
    return this._emailService.sendNotificacionMeetingCancel(body).then(res => {
      return res
    })
  }

  async selectAll(): Promise<UsuarioEntity[]> {
    const usuariosEntities = await this.repository.find();
    return usuariosEntities;
  }

  selectById(id_usuario: number): Promise<UsuarioEntity | undefined> {
    return this.repository.findOne({ id_usuario });
  }

  selectByIdRol(id_usuario_rol: number): Promise<UsuarioEntity | undefined> {
    return this.repository.query("select * from usuario INNER JOIN usuario_rol on usuario_rol.id_usuario=usuario.id_usuario where usuario_rol.id_usuario_rol=" + id_usuario_rol);
  }
  selectByHoraInicio(hora: String): Promise<UsuarioEntity | undefined> {
    return this.repository.query("SELECT * from  usuario INNER JOIN usuario_rol ON usuario.id_usuario = usuario_rol.id_usuario INNER JOIN deta_dia_hora ON usuario_rol.id_usuario_rol = deta_dia_hora.id_usuario_rol where deta_dia_hora.hora_inicio>" + hora);
  }
  updateConexion(hora: String, idusuario: number, valid: String): Promise<UsuarioEntity | undefined> {

    return this.repository.query("update  usuario set ultima_conexion=" + hora + ",estado_usuario_cuenta=" + valid + " where id_usuario=" + idusuario);
  }
  updateFoto(nombre: String, idusuario: number): Promise<UsuarioEntity | undefined> {

    return this.repository.query("update  usuario set foto_usuario=" + nombre + " where id_usuario=" + idusuario);
  }

  updateVideo(nombre: String, idusuario: number): Promise<UsuarioEntity | undefined> {

    return this.repository.query("update  detalles_profesor set video_presentacion=" + nombre + " where id_usuario_rol=" + idusuario);
  }
  precioMaximo(): Promise<UsuarioEntity | undefined> {

    return this.repository.query("SELECT MAX(preciofijo) preciofijo FROM detalles_profesor;");
  }


  async selectAllTeachers() {
    return await this.repository.query("SELECT * FROM public.usuario_rol as ru join public.usuario as u on ru.id_usuario =  u.id_usuario join public.rol as r on r.id_rol = ru.id_rol where r.nombre_rol = 'PROFESOR'").then(async res => {
      let b;
      for (let a of res) {
        a.estrellas = await this.promedio(a.id_usuario_rol).then(async ress => {
          let b;
          ////console.log("resddfsd",ress.length)
          if (ress.length > 0) {
            b = ress[0];
          }
          else {
            b = {
              "promedio": "1.0000000000000000"
            }
          }

          return b
        })
      }
      return res
    })

  }

  async promedio(idrol) {

    return await this.repository.query("SELECT  avg(valoracion) as promedio FROM comentarios where id_usuario_rol_profesor=" + idrol + " GROUP BY id_usuario_rol_profesor")
  }


  async update(id: number, fieldEntity: UsuarioEntity): Promise<UpdateResult> {
    const usuario_rol = await this.usuarioRolService.selectByUsuarioId(id);
    const rol = await this.rolService.selectById(usuario_rol.idRol);
    const dir = await this.createDirectory(fieldEntity.nombres_usuario, fieldEntity.apellidos_usuario, fieldEntity.identificacion_usuario);

    if (rol.nombre_rol == 'PROFESOR' && dir) {

      const res = await this.detalleService.selectByIdUsuarioRol(usuario_rol.id_usuario_rol);

      if (!res) {
        await this.detalleService.insert({
          cv_deta_profe: null,
          penales_deta_profe: null,
          descri_deta_profe: null,
          ruta_deta_profe: dir,
          id_usuario_rol: usuario_rol.id_usuario_rol,
        });
      }

    }
    return this.repository.update(id, fieldEntity);
  }

  delete(id: number): Promise<DeleteResult> {
    return this.repository.delete(id);
  }

  createDirectory(nombre: string, apellido: string, ci: string) {
    if (nombre && apellido && ci) {
      const dir = ci + '_' + nombre + '_' + apellido;
      return dir;
    } else {
      return null;
    }
  }

}
