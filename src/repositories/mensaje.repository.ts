import { Injectable } from '@nestjs/common';
import { DeleteResult, InsertResult, Repository, UpdateResult } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { MensajeEntity } from '../entities/mensaje.entity';

@Injectable()
export class MensajeRepository {

  constructor(
    @InjectRepository(MensajeEntity)
    private readonly repository: Repository<MensajeEntity>,
  ) {
  }

  async insert(entity: MensajeEntity): Promise<InsertResult> {
    return this.repository.insert(entity);
  }

  async selectAll(): Promise<MensajeEntity[]> {
    return await this.repository.find();
  }

  async selectById(id_mensaje: number): Promise<MensajeEntity | undefined> {
    return await this.repository.findOne({ id_mensaje });
  }

  async update(id: number, fieldEntity: object): Promise<UpdateResult> {
    return this.repository.update(id, fieldEntity);
  }

  delete(id: number): Promise<DeleteResult> {
    return this.repository.delete(id);
  }

}