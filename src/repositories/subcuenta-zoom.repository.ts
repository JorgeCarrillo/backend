import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, InsertResult, Repository, UpdateResult } from 'typeorm';
import { SubcuentaZoomEntity } from '../entities/subcuenta-zoom.entity';

@Injectable()
export class SubcuentaZoomRepository {

  constructor(
    @InjectRepository(SubcuentaZoomEntity)
    private readonly repository: Repository<SubcuentaZoomEntity>,
  ) {
  }

  async insert(entity: SubcuentaZoomEntity): Promise<InsertResult> {
    return this.repository.insert(entity);
  }

  async selectAll(): Promise<SubcuentaZoomEntity[]> {
    return await this.repository.find();
  }
  async selectByDisponibilidadHora(disponibilidad:string): Promise<SubcuentaZoomEntity[]> {
    return await this.repository.query("select * from subcuenta_zoom where disponibilidad_hora="+disponibilidad);
  }

  async selectById(id_subcuenta: number): Promise<SubcuentaZoomEntity | undefined> {
    return await this.repository.findOne({ id_subcuenta });
  }

  async selectByIdCuenta(cuenta: number): Promise<SubcuentaZoomEntity [] | undefined> {
    return await this.repository.find({ cuenta });
  }

  async update(id: number, fieldEntity: object): Promise<UpdateResult> {
    return this.repository.update(id, fieldEntity);
  }

  delete(id: number): Promise<DeleteResult> {
    return this.repository.delete(id);
  }

}