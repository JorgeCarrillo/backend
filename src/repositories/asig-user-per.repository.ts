import { Injectable } from '@nestjs/common';
import { DeleteResult, InsertResult, Repository, UpdateResult } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { AsigUserPerEntity } from '../entities/asig-user-per.entity';

@Injectable()
export class AsigUserPerRepository {

  constructor(
    @InjectRepository(AsigUserPerEntity)
    private readonly repository: Repository<AsigUserPerEntity>,
  ) {
  }

  async insert(entity: AsigUserPerEntity): Promise<InsertResult> {
    return this.repository.insert(entity);
  }

  async selectAll(): Promise<AsigUserPerEntity[]> {
    return await this.repository.find();
  }

  async selectById(id_asig_user_per: number): Promise<AsigUserPerEntity | undefined> {
    return await this.repository.findOne({ id_asig_user_per });
  }

  async update(id: number, fieldEntity: object): Promise<UpdateResult> {
    return this.repository.update(id, fieldEntity);
  }

  delete(id: number): Promise<DeleteResult> {
    return this.repository.delete(id);
  }

}