import { Injectable } from '@nestjs/common';
import { DeleteResult, InsertResult, Repository, UpdateResult } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { EmisorReceptorEntity } from '../entities/emisor-receptor.entity';
import { MensajeRepository } from './mensaje.repository';
@Injectable()
export class EmisorReceptorRepository {
  constructor(
    @InjectRepository(EmisorReceptorEntity)
    private readonly repository: Repository<EmisorReceptorEntity>,
    private readonly mensaje: MensajeRepository,

  ) {
  }

  async insert(entity: EmisorReceptorEntity): Promise<InsertResult> {
    return this.repository.insert(entity);
  }

  async selectAll(): Promise<EmisorReceptorEntity[]> {
    return await this.repository.find();
  }

  async selectById(id_emi_rec: number): Promise<EmisorReceptorEntity | undefined> {
    return await this.repository.findOne({ id_emi_rec });
  }

  async selectByIdemi(id_emisor: number, id_receptor: number): Promise<EmisorReceptorEntity[]> {

    const mensaje = await this.repository.query("SELECT * FROM emisor_receptor INNER JOIN mensaje ON emisor_receptor.id_mensaje = mensaje.id_mensaje where (id_receptor=" + id_receptor + " and id_emisor=" + id_emisor + ") or (id_receptor=" + id_emisor + " and id_emisor=" + id_receptor + ") order by mensaje.id_mensaje asc");



    return mensaje
  }



  async selectByIdrecep(id_receptor: number): Promise<EmisorReceptorEntity | undefined> {
    return await this.repository.query("select * from emisor_receptor where id_receptor=" + id_receptor + " or id_emisor=" + id_receptor);
  }
  async findByIdUltimoMensaj(id_emisor: number, id_receptor: number): Promise<EmisorReceptorEntity | undefined> {
    return await this.repository.query("select * from emisor_receptor inner join mensaje on mensaje.id_mensaje=emisor_receptor.id_mensaje where id_receptor=" + id_receptor + " and id_emisor=" + id_emisor + " or id_receptor=" + id_emisor + " and id_emisor=" + id_receptor + " order by id_emi_rec desc limit 1");
  }

  /*async selectByIds(query: string): Promise<EmisorReceptorEntity | undefined> {
    return await this.repository.findOne(query);
  }*/


  async update(id: number, fieldEntity: object): Promise<UpdateResult> {
    return this.repository.update(id, fieldEntity);
  }

  delete(id: number): Promise<DeleteResult> {
    return this.repository.delete(id);
  }
}