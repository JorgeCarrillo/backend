import { Injectable } from '@nestjs/common';
import { DeleteResult, InsertResult, Repository, UpdateResult } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { AsigPerServEntity } from '../entities/asig-per-serv.entity';

@Injectable()
export class AsigPerServRepository {

  constructor(
    @InjectRepository(AsigPerServEntity)
    private readonly repository: Repository<AsigPerServEntity>,
  ) {
  }

  async insert(entity: AsigPerServEntity): Promise<InsertResult> {
    return this.repository.insert(entity);
  }

  async selectAll(): Promise<AsigPerServEntity[]> {
    return await this.repository.find();
  }

  async selectById(id_asig_per_serv: number): Promise<AsigPerServEntity | undefined> {
    return await this.repository.findOne({ id_asig_per_serv });
  }

  async update(id: number, fieldEntity: object): Promise<UpdateResult> {
    return this.repository.update(id, fieldEntity);
  }

  delete(id: number): Promise<DeleteResult> {
    return this.repository.delete(id);
  }

}