import { Injectable } from '@nestjs/common';
import { DeleteResult, InsertResult, Repository, UpdateResult } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { CategoriaEntity } from '../entities/categoria.entity';

@Injectable()
export class CategoriaRepository {

  constructor(
    @InjectRepository(CategoriaEntity)
    private readonly repository: Repository<CategoriaEntity>,
  ) {
  }

  async insert(entity: CategoriaEntity): Promise<InsertResult> {
    return this.repository.insert(entity);
  }

  async selectAll(): Promise<CategoriaEntity[]> {
    return await this.repository.find();
  }

  async selectById(id_categoria: number): Promise<CategoriaEntity | undefined> {
    return await this.repository.findOne({ id_categoria });
  }

  async update(id: number, fieldEntity: object): Promise<UpdateResult> {
    return this.repository.update(id, fieldEntity);
  }

  delete(id: number): Promise<DeleteResult> {
    return this.repository.delete(id);
  }

}