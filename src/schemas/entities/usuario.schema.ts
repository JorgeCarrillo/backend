import * as Joi from 'joi';

export const USUARIO_SCHEMA = Joi
  .object()
  .keys({
    nick: Joi.string().min(4).max(50).required(),
    password: Joi.string().min(4).max(16).required(),
    rol: Joi.number().required(),
  });