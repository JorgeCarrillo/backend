import * as Joi from 'joi';

export const PERMISO_SCHEMA = Joi
  .object()
  .keys({
    desc_permiso: Joi.string().min(4).max(50).required(),
    detalle_permiso: Joi.string().min(4).max(200).required(),

  });