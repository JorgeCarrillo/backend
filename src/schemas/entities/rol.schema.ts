import * as Joi from 'joi';

export const ROL_SCHEMA = Joi
  .object()
  .keys({
    desc_rol: Joi.string().min(4).max(50).required(),
  });