import * as Joi from 'joi';

export const AUTENTICACION_SCHEMA = Joi
  .object()
  .keys({
    nick: Joi
      .string()
      .alphanum()
      .min(3)
      .max(26)
      .required(),
    password: Joi
      .string()
      .alphanum()
      .min(3)
      .max(32)
      .required(),
  });

