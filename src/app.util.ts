import * as fileSytem from 'fs';

export class AppUtil {
  static appendFile(pathFile: string, content: any) {
    return new Promise(
      (resolve, reject) => {
        fileSytem.appendFile(
          pathFile,
          content,
          (err) => {
            if (err) reject(err);
            resolve(`File ${pathFile} has been appended.`);
          });
      },
    );

  }
}