"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.AppModule = void 0;
var common_1 = require("@nestjs/common");
var app_controller_1 = require("./app.controller");
var app_service_1 = require("./app.service");
var usuario_entity_1 = require("./entities/usuario.entity");
var rol_entity_1 = require("./entities/rol.entity");
var typeorm_1 = require("@nestjs/typeorm");
var autenticacion_controller_1 = require("./controllers/autenticacion.controller");
var autenticacion_service_1 = require("./services/autenticacion.service");
var jwt_service_1 = require("./services/jwt.service");
var permiso_entity_1 = require("./entities/permiso.entity");
var usuario_repository_1 = require("./repositories/usuario.repository");
var rol_repository_1 = require("./repositories/rol.repository");
var rol_controller_1 = require("./controllers/rol.controller");
var permiso_repository_1 = require("./repositories/permiso.repository");
var permiso_controller_1 = require("./controllers/permiso.controller");
var usuario_controller_1 = require("./controllers/usuario.controller");
var ciudad_entity_1 = require("./entities/ciudad.entity");
var configuracion_entity_1 = require("./entities/configuracion.entity");
var detalles_profesor_entity_1 = require("./entities/detalles-profesor.entity");
var estado_entity_1 = require("./entities/estado.entity");
var login_usuario_entity_1 = require("./entities/login-usuario.entity");
var materia_entity_1 = require("./entities/materia.entity");
var pais_entity_1 = require("./entities/pais.entity");
var titulos_profesor_entity_1 = require("./entities/titulos-profesor.entity");
var usuario_rol_entity_1 = require("./entities/usuario-rol.entity");
var ciudad_controller_1 = require("./controllers/ciudad.controller");
var configuracion_controller_1 = require("./controllers/configuracion.controller");
var detalles_profesor_controller_1 = require("./controllers/detalles-profesor.controller");
var estado_controller_1 = require("./controllers/estado.controller");
var login_usuario_controller_1 = require("./controllers/login-usuario.controller");
var materia_controller_1 = require("./controllers/materia.controller");
var pais_controller_1 = require("./controllers/pais.controller");
var titulos_profesor_controller_1 = require("./controllers/titulos-profesor.controller");
var usuario_rol_controller_1 = require("./controllers/usuario-rol.controller");
var ciudad_repository_1 = require("./repositories/ciudad.repository");
var configuracion_repository_1 = require("./repositories/configuracion.repository");
var detalles_profesor_repository_1 = require("./repositories/detalles-profesor.repository");
var estado_repository_1 = require("./repositories/estado.repository");
var login_usuario_repository_1 = require("./repositories/login-usuario.repository");
var materia_repository_1 = require("./repositories/materia.repository");
var pais_repository_1 = require("./repositories/pais.repository");
var titulos_profesor_repository_1 = require("./repositories/titulos-profesor.repository");
var usuario_rol_repository_1 = require("./repositories/usuario-rol.repository");
var recovery_password_controller_1 = require("./controllers/recovery-password.controller");
var recovery_password_service_1 = require("./services/recovery-password.service");
var email_service_1 = require("./services/email.service");
var register_service_1 = require("./services/register.service");
var categoria_entity_1 = require("./entities/categoria.entity");
var categoria_controller_1 = require("./controllers/categoria.controller");
var categoria_repository_1 = require("./repositories/categoria.repository");
var zoomApi_controller_1 = require("./controllers/external/zoomApi.controller");
var zoomApi_service_1 = require("./services/zoomApi.service");
var cuenta_zoom_entity_1 = require("./entities/cuenta-zoom.entity");
var subcuenta_zoom_entity_1 = require("./entities/subcuenta-zoom.entity");
var cuenta_zoom_controller_1 = require("./controllers/cuenta-zoom.controller");
var subcuenta_zoom_controller_1 = require("./controllers/subcuenta-zoom.controller");
var cuenta_zoom_repository_1 = require("./repositories/cuenta-zoom.repository");
var subcuenta_zoom_repository_1 = require("./repositories/subcuenta-zoom.repository");
var moodle_service_1 = require("./services/moodle.service");
var moodle_controller_1 = require("./controllers/external/moodle.controller");
var asig_per_serv_entity_1 = require("./entities/asig-per-serv.entity");
var asig_user_per_entity_1 = require("./entities/asig-user-per.entity");
var cancel_anula_entity_1 = require("./entities/cancel-anula.entity");
var comentarios_entity_1 = require("./entities/comentarios.entity");
var conclusion_entity_1 = require("./entities/conclusion.entity");
var deta_dia_hora_entity_1 = require("./entities/deta-dia-hora.entity");
var tarjeta_pago_entity_1 = require("./entities/tarjeta-pago.entity");
var detalle_pedido_entity_1 = require("./entities/detalle-pedido.entity");
var disponibilidad_entity_1 = require("./entities/disponibilidad.entity");
var emisor_receptor_entity_1 = require("./entities/emisor-receptor.entity");
var faq_entity_1 = require("./entities/faq.entity");
var horario_dia_entity_1 = require("./entities/horario-dia.entity");
var horario_hora_entity_1 = require("./entities/horario-hora.entity");
var idioma_entity_1 = require("./entities/idioma.entity");
var intereses_entity_1 = require("./entities/intereses.entity");
var mensaje_entity_1 = require("./entities/mensaje.entity");
var pago_entity_1 = require("./entities/pago.entity");
var pasatiempos_entity_1 = require("./entities/pasatiempos.entity");
var pedido_entity_1 = require("./entities/pedido.entity");
var personalizado_entity_1 = require("./entities/personalizado.entity");
var precio_descuento_entity_1 = require("./entities/precio-descuento.entity");
var reunion_entity_1 = require("./entities/reunion.entity");
var servicios_entity_1 = require("./entities/servicios.entity");
var asig_per_serv_repository_1 = require("./repositories/asig-per-serv.repository");
var asig_user_per_repository_1 = require("./repositories/asig-user-per.repository");
var cancel_anula_repository_1 = require("./repositories/cancel-anula.repository");
var comentarios_repository_1 = require("./repositories/comentarios.repository");
var conclusion_repository_1 = require("./repositories/conclusion.repository");
var deta_dia_hora_repository_1 = require("./repositories/deta-dia-hora.repository");
var tarjeta_pago_repository_1 = require("./repositories/tarjeta-pago.repository");
var detalle_pedido_repository_1 = require("./repositories/detalle-pedido.repository");
var disponibilidad_repository_1 = require("./repositories/disponibilidad.repository");
var emisor_receptor_repository_1 = require("./repositories/emisor-receptor.repository");
var faq_repository_1 = require("./repositories/faq.repository");
var horario_dia_repository_1 = require("./repositories/horario-dia.repository");
var horario_hora_repository_1 = require("./repositories/horario-hora.repository");
var idioma_repository_1 = require("./repositories/idioma.repository");
var intereses_repository_1 = require("./repositories/intereses.repository");
var mensaje_repository_1 = require("./repositories/mensaje.repository");
var pago_repository_1 = require("./repositories/pago.repository");
var pasatiempos_repository_1 = require("./repositories/pasatiempos.repository");
var pedido_repository_1 = require("./repositories/pedido.repository");
var personalizado_repository_1 = require("./repositories/personalizado.repository");
var precio_descuento_repository_1 = require("./repositories/precio-descuento.repository");
var reunion_repository_1 = require("./repositories/reunion.repository");
var servicios_repository_1 = require("./repositories/servicios.repository");
var asig_per_serv_controller_1 = require("./controllers/asig-per-serv.controller");
var asig_user_per_controller_1 = require("./controllers/asig-user-per.controller");
var cancel_anula_controller_1 = require("./controllers/cancel-anula.controller");
var comentarios_controller_1 = require("./controllers/comentarios.controller");
var conclusion_controller_1 = require("./controllers/conclusion.controller");
var deta_dia_hora_controller_1 = require("./controllers/deta-dia-hora.controller");
var tarjeta_pago_controller_1 = require("./controllers/tarjeta-pago.controller");
var detalle_pedido_controller_1 = require("./controllers/detalle-pedido.controller");
var disponibilidad_controller_1 = require("./controllers/disponibilidad.controller");
var emisor_receptor_controller_1 = require("./controllers/emisor-receptor.controller");
var faq_controller_1 = require("./controllers/faq.controller");
var horario_dia_controller_1 = require("./controllers/horario-dia.controller");
var horario_hora_controller_1 = require("./controllers/horario-hora.controller");
var idioma_controller_1 = require("./controllers/idioma.controller");
var intereses_controller_1 = require("./controllers/intereses.controller");
var mensaje_controller_1 = require("./controllers/mensaje.controller");
var pago_controller_1 = require("./controllers/pago.controller");
var pasatiempos_controller_1 = require("./controllers/pasatiempos.controller");
var pedido_controller_1 = require("./controllers/pedido.controller");
var personalizado_controller_1 = require("./controllers/personalizado.controller");
var precio_descuento_controller_1 = require("./controllers/precio-descuento.controller");
var reunion_controller_1 = require("./controllers/reunion.controller");
var servicios_controller_1 = require("./controllers/servicios.controller");
var deta_per_mate_1 = require("./entities/deta-per-mate");
var deta_per_mate_2 = require("./controllers/deta-per-mate");
var deta_per_mate_3 = require("./repositories/deta-per-mate");
var ENTIDADES = [
    asig_per_serv_entity_1.AsigPerServEntity,
    asig_user_per_entity_1.AsigUserPerEntity,
    cancel_anula_entity_1.CancelAnulaEntity,
    categoria_entity_1.CategoriaEntity,
    ciudad_entity_1.CiudadEntity,
    comentarios_entity_1.ComentariosEntity,
    conclusion_entity_1.ConclusionEntity,
    configuracion_entity_1.ConfiguracionEntity,
    cuenta_zoom_entity_1.CuentaZoomEntity,
    deta_dia_hora_entity_1.DetaDiaHoraEntity,
    tarjeta_pago_entity_1.TajetaPagoEntity,
    deta_per_mate_1.DetaPerMateEntity,
    detalle_pedido_entity_1.DetallePedidoEntity,
    detalles_profesor_entity_1.DetallesProfesorEntity,
    disponibilidad_entity_1.DisponibilidadEntity,
    emisor_receptor_entity_1.EmisorReceptorEntity,
    estado_entity_1.EstadoEntity,
    faq_entity_1.FaqEntity,
    horario_dia_entity_1.HorarioDiaEntity,
    horario_hora_entity_1.HorarioHoraEntity,
    idioma_entity_1.IdiomaEntity,
    intereses_entity_1.InteresesEntity,
    login_usuario_entity_1.LoginUsuarioEntity,
    materia_entity_1.MateriaEntity,
    mensaje_entity_1.MensajeEntity,
    pago_entity_1.PagoEntity,
    pais_entity_1.PaisEntity,
    pasatiempos_entity_1.PasatiemposEntity,
    pedido_entity_1.PedidoEntity,
    permiso_entity_1.PermisoEntity,
    personalizado_entity_1.PersonalizadoEntity,
    precio_descuento_entity_1.PrecioDescuentoEntity,
    reunion_entity_1.ReunionEntity,
    rol_entity_1.RolEntity,
    servicios_entity_1.ServiciosEntity,
    subcuenta_zoom_entity_1.SubcuentaZoomEntity,
    titulos_profesor_entity_1.TitulosProfesorEntity,
    usuario_entity_1.UsuarioEntity,
    usuario_rol_entity_1.UsuarioRolEntity
];
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        common_1.Module({
            imports: [
                common_1.HttpModule,
                typeorm_1.TypeOrmModule.forRootAsync({
                    useFactory: function () { return ({
                        type: 'postgres',
                        host: process.env.DB_HOST || 'localhost',
                        port: +process.env.DB_PORT || 5432,
                        username: process.env.DB_USER || 'postgres',
                        password: process.env.DB_PASS || 'lara123',
                        database: process.env.DB_DATABASE || 'amauta_tutorias_back',
                        entities: ENTIDADES,
                        synchronize: true
                    }); }
                }),
                typeorm_1.TypeOrmModule.forFeature(ENTIDADES),
            ],
            controllers: [
                app_controller_1.AppController,
                asig_per_serv_controller_1.AsigPerServController,
                asig_user_per_controller_1.AsigUserPerController,
                autenticacion_controller_1.AutenticacionController,
                cancel_anula_controller_1.CancelAnulaController,
                categoria_controller_1.CategoriaController,
                ciudad_controller_1.CiudadController,
                comentarios_controller_1.ComentariosController,
                conclusion_controller_1.ConclusionController,
                configuracion_controller_1.ConfiguracionController,
                cuenta_zoom_controller_1.CuentaZoomController,
                deta_dia_hora_controller_1.DetaDiaHoraController,
                tarjeta_pago_controller_1.TajetaPagoController,
                deta_per_mate_2.DetaPerMateController,
                detalle_pedido_controller_1.DetallePedidoController,
                detalles_profesor_controller_1.DetallesProfesorController,
                disponibilidad_controller_1.DisponibilidadController,
                emisor_receptor_controller_1.EmisorReceptorController,
                estado_controller_1.EstadoController,
                faq_controller_1.FaqController,
                horario_dia_controller_1.HorarioDiaController,
                horario_hora_controller_1.HorarioHoraController,
                idioma_controller_1.IdiomaController,
                intereses_controller_1.InteresesController,
                login_usuario_controller_1.LoginUsuarioController,
                materia_controller_1.MateriaController,
                mensaje_controller_1.MensajeController,
                pago_controller_1.PagoController,
                pais_controller_1.PaisController,
                pasatiempos_controller_1.PasatiemposController,
                pedido_controller_1.PedidoController,
                permiso_controller_1.PermisoController,
                personalizado_controller_1.PersonalizadoController,
                precio_descuento_controller_1.PrecioDescuentoController,
                recovery_password_controller_1.RecoveryPasswordController,
                reunion_controller_1.ReunionController,
                rol_controller_1.RolController,
                servicios_controller_1.ServiciosController,
                subcuenta_zoom_controller_1.SubcuentaZoomController,
                titulos_profesor_controller_1.TitulosProfesorController,
                usuario_controller_1.UsuarioController,
                usuario_rol_controller_1.UsuarioRolController,
                /*EXTERNALS*/
                moodle_controller_1.MoodleController,
                zoomApi_controller_1.ZoomApiController,
            ],
            providers: [
                app_service_1.AppService,
                autenticacion_service_1.AutenticacionService,
                jwt_service_1.JwtService,
                moodle_service_1.MoodleService,
                recovery_password_service_1.RecoveryPasswordService,
                email_service_1.EmailService,
                register_service_1.RegisterService,
                zoomApi_service_1.ZoomApiService,
                asig_per_serv_repository_1.AsigPerServRepository,
                asig_user_per_repository_1.AsigUserPerRepository,
                cancel_anula_repository_1.CancelAnulaRepository,
                categoria_repository_1.CategoriaRepository,
                ciudad_repository_1.CiudadRepository,
                comentarios_repository_1.ComentariosRepository,
                conclusion_repository_1.ConclusionRepository,
                configuracion_repository_1.ConfiguracionRepository,
                cuenta_zoom_repository_1.CuentaZoomRepository,
                deta_dia_hora_repository_1.DetaDiaHoraRepository,
                tarjeta_pago_repository_1.TajetaPagoRepository,
                deta_per_mate_3.DetaPerMateRepository,
                detalle_pedido_repository_1.DetallePedidoRepository,
                detalles_profesor_repository_1.DetallesProfesorRepository,
                disponibilidad_repository_1.DisponibilidadRepository,
                emisor_receptor_repository_1.EmisorReceptorRepository,
                estado_repository_1.EstadoRepository,
                faq_repository_1.FaqRepository,
                horario_dia_repository_1.HorarioDiaRepository,
                horario_hora_repository_1.HorarioHoraRepository,
                idioma_repository_1.IdiomaRepository,
                intereses_repository_1.InteresesRepository,
                login_usuario_repository_1.LoginUsuarioRepository,
                materia_repository_1.MateriaRepository,
                mensaje_repository_1.MensajeRepository,
                pago_repository_1.PagoRepository,
                pais_repository_1.PaisRepository,
                pasatiempos_repository_1.PasatiemposRepository,
                pedido_repository_1.PedidoRepository,
                permiso_repository_1.PermisoRepository,
                personalizado_repository_1.PersonalizadoRepository,
                precio_descuento_repository_1.PrecioDescuentoRepository,
                reunion_repository_1.ReunionRepository,
                rol_repository_1.RolRepository,
                servicios_repository_1.ServiciosRepository,
                subcuenta_zoom_repository_1.SubcuentaZoomRepository,
                titulos_profesor_repository_1.TitulosProfesorRepository,
                usuario_repository_1.UsuarioRepository,
                usuario_rol_repository_1.UsuarioRolRepository
            ]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
