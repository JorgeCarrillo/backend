import { Body, Controller, Delete, Get, Param, Post, Put, Res } from '@nestjs/common';
import { AsigUserPerRepository } from '../repositories/asig-user-per.repository';
import { CrearAsigUserPerDto } from '../dtos/create/crear-asig-user-per.dto';

@Controller('asig-user-per')
export class AsigUserPerController {

  constructor(
    private readonly asigUserPerRepository: AsigUserPerRepository,
  ) {}

  @Post()
  create(@Body() crearNuevo: CrearAsigUserPerDto) {
    return this.asigUserPerRepository.insert(crearNuevo);
  }

  @Get()
  async findAll(@Res() response) {
    return response.send(await this.asigUserPerRepository.selectAll());
  }

  @Get('id/:id')
  async findById(@Res() response, @Param('id') id) {
    return response.send(await  this.asigUserPerRepository.selectById(id));
  }

  @Put(':id')
  async update(@Param('id') id, @Body() nuevo) {
    return await this.asigUserPerRepository.update(id, nuevo);
  }

  @Delete(':id')
  async remove(@Param('id') id) {
    return await this.asigUserPerRepository.delete(id);
  }

}
