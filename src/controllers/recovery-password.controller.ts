import { Body, Controller, HttpException, HttpStatus, Post, Res } from '@nestjs/common';
import { RecoveryPasswordService } from '../services/recovery-password.service';
import { RecoveryPasswordDto } from '../dtos/update/recovery-password.dto';
import { EmailService } from '../services/email.service';
import { LoginUsuarioEntity } from '../entities/login-usuario.entity';
import { LoginUsuarioRepository } from '../repositories/login-usuario.repository';

@Controller('sendemail')
export class RecoveryPasswordController {

  constructor(
    private readonly _recoveryService: RecoveryPasswordService,
    private readonly _emailService: EmailService,
    private readonly _usuarioService: LoginUsuarioRepository,
  ) {
  }

  @Post('registro')
  async registroUsuario(@Body() dato: RecoveryPasswordDto, @Res() response) {
    return response.send(await this._emailService.sendEmailActivacion(dato, 'Activación de Cuenta'));

  }

  @Post('moodle')

  async registroUsuarioMoodle(@Body() dato: RecoveryPasswordDto, @Res() response) {
    return response.send(await this._emailService.sendEmailActivacionMoodle(dato, 'Activación de Cuenta'));

  }

  @Post('registrocurso')
  async sendEmail(@Body()body, @Res() response) {
    return response.send(await this._emailService.sendEmailInscripcionCurso(body.to, body.enlace, body.codigo, body.curso, body.url));
  }

  @Post('cursoMoodle')
  async sendEmailRegistroMoodle(@Body()body, @Res() response) {
    return response.send(await this._emailService.sendEmailInscripcionMoodle(body.to, body.curso, body.url));
  }

  @Post('meeting')
  async sendEmailMeeting(@Body()body, @Res() response) {
    return response.send(await this._emailService.sendNotificacionMeeting(body));
  }

  @Post('recovery')
  async recoveryPass(@Body() dato, @Res() response) {
    var respuesta: LoginUsuarioEntity = await this._usuarioService.selectByName(dato.nombre_login).then(result => result);
    if (respuesta) {
      dato.id_usuario = respuesta.id_login;
      return response.send(await this._emailService.sendEmailPassword(dato, 'Recuperación de Contraseña'));
    } else {
      throw new HttpException('¡No existe ningún usuario registrado con el correo ' + dato.nombre_login + ' !', HttpStatus.FORBIDDEN);
    }

  }

}
