import { Body, Controller, Delete, Get, Param, Post, Put, Res } from '@nestjs/common';
import { PersonalizadoRepository } from '../repositories/personalizado.repository';
import { CrearPersonalizadoDto } from '../dtos/create/crear-personalizado.dto';

@Controller('personalizado')
export class PersonalizadoController {

  constructor(
    private readonly personalizadoRepository: PersonalizadoRepository
  ) {}

  @Post()
  create(@Body() crearNuevo: CrearPersonalizadoDto) {
    return this.personalizadoRepository.insert(crearNuevo);
  }

  @Get()
  async findAll(@Res() response) {
    return response.send(await this.personalizadoRepository.selectAll());
  }

  @Get('id/:id')
  async findById(@Res() response, @Param('id') id) {
    return response.send(await  this.personalizadoRepository.selectById(id));
  }

  @Put(':id')
  async update(@Param('id') id, @Body() nuevo) {
    return await this.personalizadoRepository.update(id, nuevo);
  }

  @Delete(':id')
  async remove(@Param('id') id) {
    return await this.personalizadoRepository.delete(id);
  }

}