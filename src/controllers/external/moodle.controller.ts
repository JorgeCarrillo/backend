import { Body, Controller, Get, Param, Post, Res } from '@nestjs/common';
import { MoodleService } from '../../services/moodle.service';
import { UsuarioAutenticacionMoodleDto } from '../../dtos/usuario-autenticacion-moodle.dto';

@Controller('moodle')
export class MoodleController {

  constructor(
    private readonly _moodleService: MoodleService,
  ) {
  }

  @Post('login')
  async login(@Body() usuario: UsuarioAutenticacionMoodleDto, @Res() res) {
    return await res.send(await this._moodleService.autenticationMoodle(usuario));
  }

  /*
  * Example: {"notes": [1,2,3, ... ids]}
  */
  @Post('notas')
  async getNotesByIds(@Body() data: any, @Res() res) {
    return await res.send(await this._moodleService.getNotesByIds(data.notes));
  }

  /*
  * Example: {"criteria": [{"key": key, "value": value}, {"key": key, "value": value}, ...]}
  */
  @Post('users')
  async searchUserByParameters(@Body() data: any, @Res() res) {
    return await res.send(await this._moodleService.searchUserByParameters(data.criteria));
  }

  /*
  * Example: {"groupids": [1,2,3, ... , ids]}
  */
  @Post('grupos')
  async getGroups(@Res() res, @Body() data: any) {
    return await res.send(await this._moodleService.getGroups(data.groupids));
  }

  /*
  * field: string => "ids", "id", "shortname", etc.
  * value: string => "1,2,3, ..", "1", "shortname"
  * Example: {"field": "ids", "value": "1,2,3,4,..., ids"}
  */
  @Post('cursos')
  async getAllCoursesByFields(@Res() res, @Body() data: any) {
    return await res.send(await this._moodleService.getAllCoursesByFields(data.field, data.value));
  }

  /*
  * Example: field = id - {"values": [1,3,4]}
  */
  @Post('users/:field')
  async searchUserByField(@Body() data: any, @Param('field') field, @Res() res) {
    return await res.send(await this._moodleService.searchUserByField(field, data.values));
  }

  /*
  * Example: {"": [1,2,3, ... , ids]}
  */
  @Post('grupos/miembros')
  async getAllMembersByGroups(@Res() res, @Body() data: any) {
    return await res.send(await this._moodleService.getAllMembersByGroups(data.groupids));
  }

  @Get('categorias')
  async getAllCategories(@Res() res) {
    return await res.send(await this._moodleService.getAllCategories());
  }

  @Get('cursos')
  async getAllCourses(@Res() res) {
    return await res.send(await this._moodleService.getAllCourses());
  }

  @Get('users_list/:courseid')
  async getListUsersByCourseId(@Res() res, @Param('courseid') courseid) {
    return await res.send(await this._moodleService.getListUsersByCourseId(courseid));
  }

  @Get('notas/:userid')
  async getFinalGradesByUser(@Res() res, @Param('userid') userid) {
    return await res.send(await this._moodleService.getFinalGradesByUser(userid));
  }

  @Get('curso/:id')
  async getAllCourseContent(@Res() res, @Param('id') id) {
    return await res.send(await this._moodleService.getAllCourseContent(id));
  }

  @Get('contenido/:id')
  async getAllContentItems(@Res() res, @Param('id') id) {
    return await res.send(await this._moodleService.getAllContentItems(id));
  }

  @Get('module/:id')
  async getCourseModule(@Res() res, @Param('id') cmid) {
    return await res.send(await this._moodleService.getCourseModule(cmid));
  }

  @Get('module/activity/:id')
  async getHtmlModule(@Res() res, @Param('id') id) {
    return await res.send(await this._moodleService.getHtmlModule(id));
  }

  /*
  * Example instance:(number) = 1, modname:(string) = workshop
  */
  @Get('module/:instance/:modname')
  async getCourseModuleByInstance(@Res() res, @Param('instance') instance, @Param('modname') modname) {
    return await res.send(await this._moodleService.getCourseModuleByInstance(instance, modname));
  }

  @Get('cursos/inscritos/:user_id')
  async getAllCoursesByAUser(@Res() res, @Param('user_id') user_id) {
    return await res.send(await this._moodleService.getAllCoursesByAUser(user_id));
  }


  @Get('notas/curso/:courseid/usuario/:userid')
  async getNotesUserByCourse(@Res() res, @Param('courseid') groupids, @Param('userid') userid) {
    return await res.send(await this._moodleService.getNotesUserByCourse(groupids, userid));
  }

  /*
  * Example name: string => {name or all or empty}
  */
  @Get('preferencias/:name/usuario/:userid')
  async getUserPreferences(@Res() res, @Param('name') name, @Param('userid') userid) {
    return await res.send(await this._moodleService.getUserPreferences(name, userid));
  }

}