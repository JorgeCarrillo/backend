import { Body, Controller, Get, Param, Post, Res } from '@nestjs/common';
import { ZoomApiService } from '../../services/zoomApi.service';

@Controller('zoom-api')
export class ZoomApiController {
  constructor(private readonly _zoomApiService: ZoomApiService) {
  }

  @Get('usuarios')
  readAllIncidentes(@Res() res): any {
    this._zoomApiService.getHttpUser().subscribe(
      value => res.send(value.data),
      (error) => res.send(error),
    );
  }

  @Post('id')
  obtenerIdIncidente(@Res() res, @Body() body): any {
    this._zoomApiService.idUsuarioFun(body)
    res.send({estado: 'ok'});
  }

  @Post('meeting')
  createIncidente(@Res() res, @Body() body): any {

    this._zoomApiService.postMeeting(body).subscribe(
      value => res.send(value.data) ,
      (eror) => res.send(eror),
    );
  }

  @Get('meeting/:id')
  verMeeting(@Res() res, @Param('id') id): any {

    this._zoomApiService.getMettingDetalle(id).subscribe(
      value => res.send(value.data) ,
      (eror) => res.send(eror),
    );
  }

  @Get('videos/:id')
  verVideos(@Res() res, @Param('id') id): any {

    this._zoomApiService.getMettingLive(id).subscribe(
      value => res.send(value.data) ,
      (eror) => res.send(eror),
    );
  }

  @Post('password')
  updatePassword(@Res() res, @Body() body): any {

    this._zoomApiService.putPassword(body).subscribe(
      value => res.send(value.data) ,
      (eror) => res.send(eror),
    );
  }


}
