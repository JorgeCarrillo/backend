import { Body, Controller, Delete, Get, Param, Post, Put, Res } from '@nestjs/common';
import { CrearDetallePedidoDto } from '../dtos/create/crear-detalle-pedido.dto';
import { DetallePedidoRepository } from '../repositories/detalle-pedido.repository';

@Controller('detalle_pedido')
export class DetallePedidoController {

  constructor(
    private readonly detallePedidoRepository: DetallePedidoRepository,
  ) {
  }

  @Post()
  create(@Body() crearNuevo: CrearDetallePedidoDto) {
    return this.detallePedidoRepository.insert(crearNuevo);
  }

  @Get()
  async findAll(@Res() response) {
    return response.send(await this.detallePedidoRepository.selectAll());
  }

  @Get('id/:id')
  async findById(@Res() response, @Param('id') id) {
    return response.send(await this.detallePedidoRepository.selectById(id));
  }

  @Get('idPedido/:id')
  async findByIdPedido(@Res() response, @Param('id') id) {
    return response.send(await this.detallePedidoRepository.selectByIdPedido(id));
  }

  @Put(':id')
  async update(@Param('id') id, @Body() nuevo) {
    return await this.detallePedidoRepository.update(id, nuevo);
  }

  @Delete(':id')
  async remove(@Param('id') id) {
    return await this.detallePedidoRepository.delete(id);
  }

}