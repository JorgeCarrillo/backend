import { Body, Controller, Delete, Get, Param, Post, Put, Res } from '@nestjs/common';
import { UsuarioRolRepository } from '../repositories/usuario-rol.repository';
import { CrearUsuarioRolDto } from '../dtos/create/crear-usuario-rol.dto';

@Controller('usuario-rol')
export class UsuarioRolController {

  constructor(
    private readonly usuarioRolRepository: UsuarioRolRepository
  ) {}

  @Post()
  create(@Body() crearNuevo: CrearUsuarioRolDto) {
    return this.usuarioRolRepository.insert(crearNuevo);
  }

  @Get()
  async findAll(@Res() response) {
    return response.send(await this.usuarioRolRepository.selectAll());
  }

  @Get('id/:id')
  async findByIdUsuarioRol(@Res() response, @Param('id') id) {
    return response.send(await  this.usuarioRolRepository.selectByIdUsuarioRol(id));
  }

  @Get('usuario/:id')
  async findByIdUsuario(@Res() response, @Param('id') id) {
    return response.send(await  this.usuarioRolRepository.selectByUsuarioId(id));
  }
  @Get('usuarioid/:id')
  async selectByRolIdUsuario(@Res() response, @Param('id') idusuario) {
    return response.send(await  this.usuarioRolRepository.selectByRolIdUsuario(idusuario));
  }


  @Put(':id')
  async update(@Param('id') id, @Body() nuevo) {
    return await this.usuarioRolRepository.update(id, nuevo);
  }

  @Delete(':id')
  async remove(@Param('id') id) {
    return await this.usuarioRolRepository.delete(id);
  }
}
