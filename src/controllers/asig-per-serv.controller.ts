import { Body, Controller, Delete, Get, Param, Post, Put, Res } from '@nestjs/common';
import { AsigPerServRepository } from '../repositories/asig-per-serv.repository';
import { CrearAsigPerServDto } from '../dtos/create/crear-asig-per-serv.dto';

@Controller('asig-servicio-per')
export class AsigPerServController {

  constructor(
    private readonly asigPerServRepository: AsigPerServRepository,
  ) {
  }

  @Post()
  create(@Body() crearNuevo: CrearAsigPerServDto) {
    return this.asigPerServRepository.insert(crearNuevo);
  }

  @Get()
  async findAll(@Res() response) {
    return response.send(await this.asigPerServRepository.selectAll());
  }

  @Get('id/:id')
  async findById(@Res() response, @Param('id') id) {
    return response.send(await  this.asigPerServRepository.selectById(id));
  }

  @Put(':id')
  async update(@Param('id') id, @Body() nuevo) {
    return await this.asigPerServRepository.update(id, nuevo);
  }

  @Delete(':id')
  async remove(@Param('id') id) {
    return await this.asigPerServRepository.delete(id);
  }

}