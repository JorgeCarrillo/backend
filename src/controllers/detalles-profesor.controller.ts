import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Res,
} from '@nestjs/common';
import { DetallesProfesorRepository } from '../repositories/detalles-profesor.repository';
import { CrearDetallesProfesorDto } from '../dtos/create/crear-detalles-profesor.dto';

@Controller('detalles_profesor')
export class DetallesProfesorController {

  constructor(
    private readonly detallesRepository: DetallesProfesorRepository,
  ) {
  }

  @Post()
  create(@Body() crearNuevo: CrearDetallesProfesorDto) {
    return this.detallesRepository.insert(crearNuevo);
  }

  @Get()
  async findAll(@Res() response) {
    return response.send(await this.detallesRepository.selectAll());
  }

  @Get('id/:id')
  async findById(@Res() response, @Param('id') id) {
    return response.send(await  this.detallesRepository.selectById(id));
  }

  @Get('usuario/:id')
  async findByIdUsuarioRol(@Res() response, @Param('id') id) {
    return response.send(await  this.detallesRepository.selectByIdUsuarioRol(id));
  }

  @Put(':id')
  async update(@Param('id') id, @Body() nuevo) {
    return await this.detallesRepository.update(id, nuevo);
  }

  @Delete(':id')
  async remove(@Param('id') id) {
    return await this.detallesRepository.delete(id);
  }

}