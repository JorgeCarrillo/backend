import { Body, Controller, Delete, Get, Param, Post, Put, Res } from '@nestjs/common';
import { CiudadRepository } from '../repositories/ciudad.repository';
import { CrearCiudadDto } from '../dtos/create/crear-ciudad.dto';

@Controller('ciudad')
export class CiudadController {

  constructor(
    private readonly ciudadRepository: CiudadRepository,
  ) {
  }

  @Post()
  create(@Body() crearNuevo: CrearCiudadDto) {
    return this.ciudadRepository.insert(crearNuevo);
  }

  @Get()
  async findAll(@Res() response) {
    return response.send(await this.ciudadRepository.selectAll());
  }

  @Get('id/:id')
  async findById(@Res() response, @Param('id') id) {
    return response.send(await  this.ciudadRepository.selectById(id));
  }

  @Put(':id')
  async update(@Param('id') id, @Body() nuevo) {
    return await this.ciudadRepository.update(id, nuevo);
  }

  @Delete(':id')
  async remove(@Param('id') id) {
    return await this.ciudadRepository.delete(id);
  }

}