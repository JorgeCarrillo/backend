import { Body, Controller, Delete, Get, Param, Post, Put, Res } from '@nestjs/common';
import { CuentaZoomRepository } from '../repositories/cuenta-zoom.repository';
import { CrearCuentaZoomDto } from '../dtos/create/crear-cuenta-zoom.dto';

@Controller('cuenta')
export class CuentaZoomController {

  constructor(
    private readonly cuentaZoomRepository: CuentaZoomRepository
  ) {}

  @Post()
  create(@Body() crearNuevo: CrearCuentaZoomDto) {
    return this.cuentaZoomRepository.insert(crearNuevo);
  }

  @Get()
  async findAll(@Res() response) {
    return response.send(await this.cuentaZoomRepository.selectAll());
  }

  @Get('id/:id')
  async findById(@Res() response, @Param('id') id) {
    return response.send(await  this.cuentaZoomRepository.selectById(id));
  }

  @Put(':id')
  async update(@Param('id') id, @Body() nuevo) {
    return await this.cuentaZoomRepository.update(id, nuevo);
  }

  @Delete(':id')
  async remove(@Param('id') id) {
    return await this.cuentaZoomRepository.delete(id);
  }

}