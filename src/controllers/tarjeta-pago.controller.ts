import { Body, Controller, Delete, Get, Param, Post, Put, Res } from '@nestjs/common';
import { TajetaPagoRepository } from '../repositories/tarjeta-pago.repository';
import { TarjetaPagoDto } from '../dtos/create/crear-tarjeta-pago.dto';

@Controller('tarjeta_pago')
export class TajetaPagoController {

  constructor(
    private readonly tajetaPagoRepository: TajetaPagoRepository
  ) { }

  @Post()
  create(@Body() crearNuevo: TarjetaPagoDto) {
    return this.tajetaPagoRepository.insert(crearNuevo);
  }

  @Get()
  async findAll(@Res() response) {
    return response.send(await this.tajetaPagoRepository.selectAll());
  }

  @Get('id/:id')
  async findById(@Res() response, @Param('id') id) {
    return response.send(await this.tajetaPagoRepository.selectById(id));
  }
  @Get('idrol/:id')
  async findByIdRol(@Res() response, @Param('id') idrol) {
    return response.send(await this.tajetaPagoRepository.selectByIdRol(idrol));
  }

  @Put(':id')
  async update(@Param('id') id, @Body() nuevo) {
    return await this.tajetaPagoRepository.update(id, nuevo);
  }

  @Delete(':id')
  async remove(@Param('id') id) {
    return await this.tajetaPagoRepository.delete(id);
  }

}