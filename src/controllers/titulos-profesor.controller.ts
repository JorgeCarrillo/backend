import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Put,
  Res, UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { TitulosProfesorRepository } from '../repositories/titulos-profesor.repository';
import { CrearTitulosProfesorDto } from '../dtos/create/crear-titulos-profesor.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { extname } from 'path';

@Controller('titulos-profesor')
export class TitulosProfesorController {

  constructor(
    private readonly titulosProfesorRepository: TitulosProfesorRepository,
  ) {
  }

  @Post()
  create(@Body() crearNuevo: CrearTitulosProfesorDto) {
    return this.titulosProfesorRepository.insert(crearNuevo);
  }

  @Get()
  async findAll(@Res() response) {
    return response.send(await this.titulosProfesorRepository.selectAll());
  }

  @Get('id/:id')
  async findById(@Res() response, @Param('id') id) {
    return response.send(await  this.titulosProfesorRepository.selectById(id));
  }

  @Get('usuario/:id')
  async findByIdUsuarioRol(@Res() response, @Param('id') id) {
    return response.send(await  this.titulosProfesorRepository.selectByIdUsuarioRol(id));
  }

  @Put(':id')
  async update(@Param('id') id, @Body() nuevo) {
    return await this.titulosProfesorRepository.update(id, nuevo);
  }

  @Delete(':id')
  async remove(@Param('id') id) {
    return await this.titulosProfesorRepository.delete(id);
  }

  @Get('archivos/:path/:name')
  async getFile(@Res() response, @Param('name') name, @Param('path') path) {
    return response.sendFile(`./${path}/${name}`, {root: 'public/profesores'},function(error) {
      if(error) {
        return response.send('');
      } else {
        return response;
      }});
  }

  /*Ingreso de los titulos del Profesor*/
  @Post('upload-file')
  @UseInterceptors(FileInterceptor('file', {
    storage: diskStorage({
      destination(req, file, cb) {
        const token = req.headers.authorization.replace('Bearer ', '');
        //const tokenValue = new JwtService().verificarTokenSync(token);
        const fs = require('fs');
        const dir = req.body.ruta;
        let path = './public/profesores/' + dir + '/';
        if (!fs.existsSync('./public')) {
          fs.mkdirSync('./public');
          fs.mkdirSync('./public/profesores/');
          fs.mkdirSync(path);
        } else if (!fs.existsSync('./public/profesores/')) {
          fs.mkdirSync('./public/profesores/');
          fs.mkdirSync(path);
        } else if (!fs.existsSync('./public/profesores/' + dir + '/')) {
          fs.mkdirSync(path);
        } else {
          // //console.log('¡Directorio creado anteriormente!')
        }

        cb(null, path);
      },
      filename: (req, file, cb) => {
        cb(null, `${file.originalname}`);
      },
    }),
    limits: { fileSize: 10485760 },
    fileFilter: function fileFilter(req, file, cb) {
      const filetypes = /pdf/;
      const mimetype = filetypes.test(file.mimetype);
      const extname2 = filetypes.test(extname(file.originalname).toLowerCase());

      if (mimetype && extname2) {
        return cb(null, true);
      } else {
        cb(null, false);
      }
    },
  }))
  async uploadFile(@UploadedFile() file, @Body() obj) {
    if (file) {
      return await {
        response: '¡Archivo guardado con éxito!',
        path: `${file.filename}`,
      };
    } else {
      throw new HttpException('¡Formato de archivo no soportado!', HttpStatus.BAD_REQUEST);
    }
  }

}