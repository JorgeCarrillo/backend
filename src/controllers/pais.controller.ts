import { Body, Controller, Delete, Get, Param, Post, Put, Res } from '@nestjs/common';
import { PaisRepository } from '../repositories/pais.repository';
import { CrearPaisDto } from '../dtos/create/crear-pais.dto';

@Controller('pais')
export class PaisController {

  constructor(
    private readonly paisRepository: PaisRepository
  ) {
  }

  @Post()
  create(@Body() crearNuevo: CrearPaisDto) {
    return this.paisRepository.insert(crearNuevo);
  }

  @Get()
  async findAll(@Res() response) {
    return response.send(await this.paisRepository.selectAll());
  }

  @Get('id/:id')
  async findById(@Res() response, @Param('id') id) {
    return response.send(await  this.paisRepository.selectById(id));
  }

  @Put(':id')
  async update(@Param('id') id, @Body() nuevo) {
    return await this.paisRepository.update(id, nuevo);
  }

  @Delete(':id')
  async remove(@Param('id') id) {
    return await this.paisRepository.delete(id);
  }
}