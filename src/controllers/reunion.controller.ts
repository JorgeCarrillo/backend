import { Body, Controller, Delete, Get, Param, Post, Put, Res } from '@nestjs/common';
import { ReunionRepository } from '../repositories/reunion.repository';
import { CrearReunionDto } from '../dtos/create/crear-reunion.dto';

@Controller('reunion')
export class ReunionController {

  constructor(
    private readonly reunionRepository: ReunionRepository
  ) {}

  @Post()
  create(@Body() crearNuevo: CrearReunionDto) {
    return this.reunionRepository.insert(crearNuevo);
  }

  @Get()
  async findAll(@Res() response) {
    return response.send(await this.reunionRepository.selectAll());
  }

  @Get('reunion/:id')
  async findByIdReunion(@Res() response, @Param('id') id) {
    return response.send(await  this.reunionRepository.selectByIdReunion(id));
  }
  @Get('fecha_inicio/:fecha')
  async findByfecha_inicio(@Res() response, @Param('fecha') fecha) {
    return response.send(await  this.reunionRepository.selectByfecha_inicio(fecha));
  }
  @Get('cancelarReunion/:idpedido/:fecha')
  async cancelarReunion(@Res() response, @Param('idpedido') idpedido, @Param('fecha') fecha) {
    return response.send(await  this.reunionRepository.ubdateByFecha(idpedido,fecha));
  }

  @Get('activado/:id')
  async findByIdRActivado(@Res() response, @Param('id') id) {
    return response.send(await  this.reunionRepository.selectByIdActivate(id));
  }
  @Get('idpedido/:id')
  async findByIdPedido(@Res() response, @Param('id') id) {
    return response.send(await  this.reunionRepository.selectByPedido(id));
  }

  @Put(':id')
  async update(@Param('id') id, @Body() nuevo) {
    return await this.reunionRepository.update(id, nuevo);
  }

  @Delete(':id')
  async remove(@Param('id') id) {
    return await this.reunionRepository.delete(id);
  }

}