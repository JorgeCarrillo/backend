import { Body, Controller, Delete, Get, Param, Post, Put, Res } from '@nestjs/common';
import { EstadoRepository } from '../repositories/estado.repository';
import { CrearEstadoDto } from '../dtos/create/crear-estado.dto';

@Controller('estado')
export class EstadoController {

  constructor(
    private readonly estadoRespository: EstadoRepository
  ){}

  @Post()
  create(@Body() crearNuevo: CrearEstadoDto) {
    return this.estadoRespository.insert(crearNuevo);
  }

  @Get()
  async findAll(@Res() response) {
    return response.send(await this.estadoRespository.selectAll());
  }

  @Get('id/:id')
  async findById(@Res() response, @Param('id') id) {
    return response.send(await  this.estadoRespository.selectById(id));
  }

  @Put(':id')
  async update(@Param('id') id, @Body() nuevo) {
    return await this.estadoRespository.update(id, nuevo);
  }

  @Delete(':id')
  async remove(@Param('id') id) {
    return await this.estadoRespository.delete(id);
  }

}