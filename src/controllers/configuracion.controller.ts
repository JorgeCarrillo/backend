import { Body, Controller, Delete, Get, Param, Post, Put, Res } from '@nestjs/common';
import { ConfiguracionRepository } from '../repositories/configuracion.repository';
import { CrearConfiguracionDto } from '../dtos/create/crear-configuracion.dto';

@Controller('configuracion')
export class ConfiguracionController {

  constructor(
    private readonly configuracionRepository: ConfiguracionRepository
  ) {}

  @Post()
  create(@Body() crearNuevo: CrearConfiguracionDto) {
    return this.configuracionRepository.insert(crearNuevo);
  }

  @Get()
  async findAll(@Res() response) {
    return response.send(await this.configuracionRepository.selectAll());
  }

  @Get('id/:id')
  async findById(@Res() response, @Param('id') id) {
    return response.send(await  this.configuracionRepository.selectById(id));
  }

  @Put(':id')
  async update(@Param('id') id, @Body() nuevo) {
    return await this.configuracionRepository.update(id, nuevo);
  }

  @Delete(':id')
  async remove(@Param('id') id) {
    return await this.configuracionRepository.delete(id);
  }

}