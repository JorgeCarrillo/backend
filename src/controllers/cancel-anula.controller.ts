import { Body, Controller, Delete, Get, Param, Post, Put, Res } from '@nestjs/common';
import { CancelAnulaRepository } from '../repositories/cancel-anula.repository';
import { CrearCancelAnulaDto } from '../dtos/create/crear-cancel-anula.dto';

@Controller('cancelacion')
export class CancelAnulaController {

  constructor(
    private readonly cancelAnulaRepository: CancelAnulaRepository
  ) {
  }

  @Post()
  create(@Body() crearNuevo: CrearCancelAnulaDto) {
    return this.cancelAnulaRepository.insert(crearNuevo);
  }

  @Get()
  async findAll(@Res() response) {
    return response.send(await this.cancelAnulaRepository.selectAll());
  }

  @Get('id/:id')
  async findById(@Res() response, @Param('id') id) {
    return response.send(await  this.cancelAnulaRepository.selectById(id));
  }

  @Put(':id')
  async update(@Param('id') id, @Body() nuevo) {
    return await this.cancelAnulaRepository.update(id, nuevo);
  }

  @Delete(':id')
  async remove(@Param('id') id) {
    return await this.cancelAnulaRepository.delete(id);
  }

}