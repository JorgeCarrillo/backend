import { Body, Controller, Delete, Get, Param, Post, Put, Res, UseGuards } from '@nestjs/common';
import { LoginUsuarioRepository } from '../repositories/login-usuario.repository';
import { CrearLoginUsuarioDto } from '../dtos/create/crear-login-usuario.dto';
import { RolGuard } from '../common/guards/rol.guard';
import { ActualizarPasswordDto } from '../dtos/update/actualizar-password.dto';

@Controller('login')
export class LoginUsuarioController {

  constructor(
    private readonly loginRepository: LoginUsuarioRepository,
  ) {
  }

  @Post()
  create(@Body() crearNuevo: CrearLoginUsuarioDto) {
    return this.loginRepository.insert(crearNuevo);
  }

  @Post('change_password')
  changePassword(@Body() cambiarPassword: ActualizarPasswordDto) {
    return this.loginRepository.changeOldPassword(cambiarPassword);
  }

  @Get()
  async findAll(@Res() response) {
    return response.send(await this.loginRepository.selectAll());
  }

  @Get('id/:id')
  async findById(@Res() response, @Param('id') id) {
    return response.send(await  this.loginRepository.selectById(id));
  }

  @UseGuards(RolGuard)
  @Put(':id')
  async update(@Param('id') id, @Body() nuevo) {
    return await this.loginRepository.update(id, nuevo);
  }

  @Delete(':id')
  async remove(@Param('id') id) {
    return await this.loginRepository.delete(id);
  }

}