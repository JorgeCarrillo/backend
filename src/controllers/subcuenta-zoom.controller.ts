import { Body, Controller, Delete, Get, Param, Post, Put, Res } from '@nestjs/common';
import { SubcuentaZoomRepository } from '../repositories/subcuenta-zoom.repository';
import { CrearSubcuentaZoomDto } from '../dtos/create/crear-subcuenta-zoom.dto';

@Controller('subcuenta-zoom')
export class SubcuentaZoomController {

  constructor(
    private readonly subcuentaZoomRepository: SubcuentaZoomRepository
  ) {
  }

  @Post()
  create(@Body() crearNuevo: CrearSubcuentaZoomDto) {
    return this.subcuentaZoomRepository.insert(crearNuevo);
  }

  @Get()
  async findAll(@Res() response) {
    return response.send(await this.subcuentaZoomRepository.selectAll());
  }

  @Get('id/:id')
  async findById(@Res() response, @Param('id') id) {
    return response.send(await  this.subcuentaZoomRepository.selectById(id));
  }

  @Get('cuenta/:id')
  async findByIdCuenta(@Res() response, @Param('id') id) {
    return response.send(await  this.subcuentaZoomRepository.selectByIdCuenta(id));
  }
  @Get('disponibilidad/:disponibilidad')
  async findByDisponibilidadHora(@Res() response, @Param('disponibilidad') disponibilidad) {
    return response.send(await  this.subcuentaZoomRepository.selectByDisponibilidadHora(disponibilidad));
  }

  @Put(':id')
  async update(@Param('id') id, @Body() nuevo) {
    return await this.subcuentaZoomRepository.update(id, nuevo);
  }

  @Delete(':id')
  async remove(@Param('id') id) {
    return await this.subcuentaZoomRepository.delete(id);
  }

}