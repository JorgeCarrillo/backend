import { Body, Controller, Delete, Get, Param, Post, Put, Res } from '@nestjs/common';
import { FaqRepository } from '../repositories/faq.repository';
import { CrearFaqDto } from '../dtos/create/crear-faq.dto';

@Controller('faq')
export class FaqController {

  constructor(
    private readonly faqRepository: FaqRepository
  ) {}

  @Post()
  create(@Body() crearNuevo: CrearFaqDto) {
    return this.faqRepository.insert(crearNuevo);
  }

  @Get()
  async findAll(@Res() response) {
    return response.send(await this.faqRepository.selectAll());
  }

  @Get('id/:id')
  async findById(@Res() response, @Param('id') id) {
    return response.send(await  this.faqRepository.selectById(id));
  }

  @Put(':id')
  async update(@Param('id') id, @Body() nuevo) {
    return await this.faqRepository.update(id, nuevo);
  }

  @Delete(':id')
  async remove(@Param('id') id) {
    return await this.faqRepository.delete(id);
  }

}