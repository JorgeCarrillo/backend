import { Body, Controller, Delete, Get, Param, Post, Put, Res } from '@nestjs/common';
import { DetaDiaHoraRepository } from '../repositories/deta-dia-hora.repository';
import { CrearDetaDiaHoraDto } from '../dtos/create/crear-deta-dia-hora.dto';

@Controller('detalle_horario')
export class DetaDiaHoraController {

  constructor(
    private readonly detalleHorarioRepository: DetaDiaHoraRepository
  ) { }

  @Post()
  create(@Body() crearNuevo: CrearDetaDiaHoraDto) {
    return this.detalleHorarioRepository.insert(crearNuevo);
  }

  @Get()
  async findAll(@Res() response) {
    return response.send(await this.detalleHorarioRepository.selectAll());
  }

  @Get('id/:id')
  async findById(@Res() response, @Param('id') id) {
    return response.send(await this.detalleHorarioRepository.selectById(id));
  }
  @Get('idrol/:id')
  async findByIdRol(@Res() response, @Param('id') idrol) {
    return response.send(await this.detalleHorarioRepository.selectByIdRol(idrol));
  }
  @Get('delete/:id')
  async deleteHora(@Res() response, @Param('id') idhora) {
    return response.send(await this.detalleHorarioRepository.deleteHora(idhora));
  }
  @Get('validarhora/:idrol')
  async findValidar(@Res() response,@Param('idrol') idrol) {
    return response.send(await this.detalleHorarioRepository.validarHoras(idrol));
  }
  @Get('hora/:hora/:id')
  async findHora(@Res() response,@Param('hora') hora,@Param('id') id) {
    return response.send(await this.detalleHorarioRepository.selectByFecha(hora,id));
  }

  @Get('updateestado/:idhora/:estado')
  async updateEstado(@Res() response, @Param('idhora') idhora, @Param('estado') estado) {
    return response.send(await this.detalleHorarioRepository.actualizarEstado(idhora,estado));
  }

  @Put(':id')
  async update(@Param('id') id, @Body() nuevo) {
    return await this.detalleHorarioRepository.update(id, nuevo);
  }

  @Delete(':id')
  async remove(@Param('id') id) {
    return await this.detalleHorarioRepository.delete(id);
  }

}