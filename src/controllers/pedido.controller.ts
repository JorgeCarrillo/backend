import { Body, Controller, Delete, Get, Param, Post, Put, Res } from '@nestjs/common';
import { PedidoRepository } from '../repositories/pedido.repository';
import { CrearPedidoDto } from '../dtos/create/crear-pedido.dto';

@Controller('pedido')
export class PedidoController {

  constructor(
    private readonly pedidoRepository: PedidoRepository
  ) {}

  @Post()
  create(@Body() crearNuevo: CrearPedidoDto) {
    return this.pedidoRepository.insert(crearNuevo);
  }

  @Get()
  async findAll(@Res() response) {
    return response.send(await this.pedidoRepository.selectAll());
  }

  @Get('id/:id')
  async findById(@Res() response, @Param('id') id) {
    return response.send(await  this.pedidoRepository.selectById(id));
  }

  @Get('idrol/:id')
  async findByIdRol(@Res() response, @Param('id') idrol) {
    return response.send(await  this.pedidoRepository.selectByIdRol(idrol));
  }
  @Get('updateestadopedido/:estado/:idpedido')
  async updateEstado(@Res() response, @Param('estado') estado , @Param('idpedido') idpedido) {
    return response.send(await  this.pedidoRepository.updateEstadoPedido(estado,idpedido));
  }
  @Get('cancelarReunion/:idpedido/:fecha/:idhora')
  async cancelarReunion(@Res() response, @Param('idpedido') idpedido, @Param('fecha') fecha, @Param('idhora') idhora) {
    return response.send(await  this.pedidoRepository.ubdateByFecha(idpedido,fecha,idhora));
  }

  @Get('pedidosbyprof/:fecha/:idprof')
  async traerPedidosProf(@Res() response, @Param('fecha') fecha, @Param('idprof') idprof) {
    return response.send(await  this.pedidoRepository.getPedidosProf(fecha,idprof));
  }
  @Put(':id')
  async update(@Param('id') id, @Body() nuevo) {
    return await this.pedidoRepository.update(id, nuevo);
  }

  @Delete(':id')
  async remove(@Param('id') id) {
    return await this.pedidoRepository.delete(id);
  }

}