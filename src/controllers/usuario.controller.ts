import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Put,
  Res,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { extname } from 'path';
import { diskStorage } from 'multer';
import { CrearUsuarioDto } from '../dtos/create/crear-usuario.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { UsuarioRepository } from '../repositories/usuario.repository';
import { CrearCuentaDto } from '../dtos/create/crear-cuenta.dto';
import { RegisterService } from '../services/register.service';

@Controller('usuario')
export class UsuarioController {

  constructor(
    private readonly usuarioRepository: UsuarioRepository,
    private readonly registerService: RegisterService) {
  }

  @Post()
  create(@Body() crearNuevo: CrearUsuarioDto) {
    return this.usuarioRepository.insert(crearNuevo);
  }
  @Post('actualizar')
  actualizar(@Body() actualizar) {
    return this.usuarioRepository.actualizarUsuario(actualizar);
  }

  @Post('registro')
  register(@Body() crearNuevo: CrearCuentaDto) {
    return this.registerService.createAccount(crearNuevo);
  }

  @Get()
  async findAll(@Res() response) {
    return response.send(await this.usuarioRepository.selectAll());
  }

  @Get('profesores')
  async findAllTeachers(@Res() response) {
    return response.send(await this.usuarioRepository.selectAllTeachers());
  }

  @Get('id/:id')
  async findOne(@Param('id') id, @Res() response) {
    return response.send(await this.usuarioRepository.selectById(id));
  }
  @Get('idrol/:idrol')
  async findIdRol(@Param('idrol') idrol, @Res() response) {
    return response.send(await this.usuarioRepository.selectByIdRol(idrol));
  }

  @Post('recuperarcontrasenia')
  async recuperarContraseña(@Body() data) {
    return this.usuarioRepository.crearTokenRecuperarContraseña(data).then(res => {
      return res
    });
  }
  @Post('cambiarcontrasenia')
  async cambiarContraseña(@Body() data) {
    return this.usuarioRepository.cambiarContraseña(data).then(res => {
      return res
    });
  }
  @Post('cancelarclase')
  async cancelClass(@Body() data) {
    return this.usuarioRepository.cancelarClase(data).then(res => {
      return res
    });
  }
  @Get('filtrarhorario/:hora')
  async findHoraInicio(@Param('hora') hora, @Res() response) {
    return response.send(await this.usuarioRepository.selectByHoraInicio(hora));
  }

  @Get('enlinea/:hora/:id/:valid')
  async enlinea(@Param('hora') hora, @Param('id') id, @Param('valid') valid, @Res() response) {
    return response.send(await this.usuarioRepository.updateConexion(hora, id, valid));
  }
  @Get('actualizarfoto/:nombre/:id')
  async actualizarFoto(@Param('nombre') nombre, @Param('id') id, @Res() response) {
    return response.send(await this.usuarioRepository.updateFoto(nombre, id));
  }

  @Get('actualizarvideo/:nombre/:id')
  async actualizarVideo(@Param('nombre') nombre, @Param('id') id, @Res() response) {
    return response.send(await this.usuarioRepository.updateVideo(nombre, id));
  }

  @Get('maxprecio')
  async precioMaximo(@Res() response) {
    return response.send(await this.usuarioRepository.precioMaximo());
  }


  //@UseGuards(RolGuard)
  @Put(':id')
  async update(@Param('id') id, @Body() nuevo: CrearUsuarioDto) {
    return await this.usuarioRepository.update(id, nuevo);
  }

  @Delete(':id')
  async remove(@Param('id') id) {
    return await this.usuarioRepository.delete(id);
  }

  @Post('upload-image')
  @UseInterceptors(FileInterceptor('filename', {

    storage: diskStorage({

      destination: function (req, file, cb) {
        //console.log("file 2", file);

        const fs = require('fs');
        const dir = `./public/usuario`;

        if (!fs.existsSync('./public')) {
          fs.mkdirSync('./public');
          fs.mkdirSync(dir);
        } else if (!fs.existsSync(dir)) {
          fs.mkdirSync(dir);
        }
        cb(null, dir);
      },
      filename: (req, file, cb) => {
        cb(null, `${file.originalname}`);
      },
    }),

    limits: { fileSize: 104857600 },

    fileFilter: function fileFilter(req, file, cb) {
      //console.log("file 1", file);

      const filetypes = /.jpg|.png|.jpeg|.PNG|.mp4/;
      const mimetype = filetypes.test(file.mimetype);
      const extname2 = filetypes.test(extname(file.originalname).toLowerCase());

      if (mimetype && extname2) {
        return cb(null, true);
      } else {
        cb(null, false);
      }
    }

  }))


  async uploadImage(@UploadedFile() file) {

    //console.log("file que llega", file)
    if (file) {

      return {
        response: '¡Imagen guardada con éxito!',
        imagePath: `${file.destination}/${file.filename}`.replace('./public/usuario/', ''),
      };

    } else {
      throw new HttpException('¡Formato de imagen no soportado!', HttpStatus.BAD_REQUEST);
    }
  }

  @Get('imagen/:name')
  async getFileCurso(@Res() response, @Param('name') name) {
    return response.sendFile(`./${name}`, { root: 'public/usuario' }, function (error) {
      if (error) {
        return response.send('');
      } else {
        return response;
      }
    });
  }

}
