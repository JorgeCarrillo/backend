import { Body, Controller, Delete, Get, Param, Post, Put, Res } from '@nestjs/common';
import { ServiciosRepository } from '../repositories/servicios.repository';
import { CrearServicioDto } from '../dtos/create/crear-servicio.dto';

@Controller('servicios')
export class ServiciosController {

  constructor(
    private readonly serviciosRepository: ServiciosRepository
  ) {}

  @Post()
  create(@Body() crearNuevo: CrearServicioDto) {
    return this.serviciosRepository.insert(crearNuevo);
  }

  @Get()
  async findAll(@Res() response) {
    return response.send(await this.serviciosRepository.selectAll());
  }

  @Get('id/:id')
  async findById(@Res() response, @Param('id') id) {
    return response.send(await  this.serviciosRepository.selectById(id));
  }

  @Put(':id')
  async update(@Param('id') id, @Body() nuevo) {
    return await this.serviciosRepository.update(id, nuevo);
  }

  @Delete(':id')
  async remove(@Param('id') id) {
    return await this.serviciosRepository.delete(id);
  }

}