"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.UsuarioController = void 0;
var common_1 = require("@nestjs/common");
var path_1 = require("path");
var multer_1 = require("multer");
var platform_express_1 = require("@nestjs/platform-express");
var UsuarioController = /** @class */ (function () {
    function UsuarioController(usuarioRepository, registerService) {
        this.usuarioRepository = usuarioRepository;
        this.registerService = registerService;
    }
    UsuarioController.prototype.create = function (crearNuevo) {
        return this.usuarioRepository.insert(crearNuevo);
    };
    UsuarioController.prototype.actualizar = function (actualizar) {
        return this.usuarioRepository.actualizarUsuario(actualizar);
    };
    UsuarioController.prototype.register = function (crearNuevo) {
        return this.registerService.createAccount(crearNuevo);
    };
    UsuarioController.prototype.findAll = function (response) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, _b;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        _b = (_a = response).send;
                        return [4 /*yield*/, this.usuarioRepository.selectAll()];
                    case 1: return [2 /*return*/, _b.apply(_a, [_c.sent()])];
                }
            });
        });
    };
    UsuarioController.prototype.findAllTeachers = function (response) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, _b;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        _b = (_a = response).send;
                        return [4 /*yield*/, this.usuarioRepository.selectAllTeachers()];
                    case 1: return [2 /*return*/, _b.apply(_a, [_c.sent()])];
                }
            });
        });
    };
    UsuarioController.prototype.findOne = function (id, response) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, _b;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        _b = (_a = response).send;
                        return [4 /*yield*/, this.usuarioRepository.selectById(id)];
                    case 1: return [2 /*return*/, _b.apply(_a, [_c.sent()])];
                }
            });
        });
    };
    UsuarioController.prototype.findIdRol = function (idrol, response) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, _b;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        _b = (_a = response).send;
                        return [4 /*yield*/, this.usuarioRepository.selectByIdRol(idrol)];
                    case 1: return [2 /*return*/, _b.apply(_a, [_c.sent()])];
                }
            });
        });
    };
    UsuarioController.prototype.recuperarContraseña = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.usuarioRepository.crearTokenRecuperarContraseña(data).then(function (res) {
                        return res;
                    })];
            });
        });
    };
    UsuarioController.prototype.cambiarContraseña = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.usuarioRepository.cambiarContraseña(data).then(function (res) {
                        return res;
                    })];
            });
        });
    };
    UsuarioController.prototype.cancelClass = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.usuarioRepository.cancelarClase(data).then(function (res) {
                        return res;
                    })];
            });
        });
    };
    UsuarioController.prototype.findHoraInicio = function (hora, response) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, _b;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        _b = (_a = response).send;
                        return [4 /*yield*/, this.usuarioRepository.selectByHoraInicio(hora)];
                    case 1: return [2 /*return*/, _b.apply(_a, [_c.sent()])];
                }
            });
        });
    };
    UsuarioController.prototype.enlinea = function (hora, id, response) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, _b;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        _b = (_a = response).send;
                        return [4 /*yield*/, this.usuarioRepository.updateConexion(hora, id)];
                    case 1: return [2 /*return*/, _b.apply(_a, [_c.sent()])];
                }
            });
        });
    };
    UsuarioController.prototype.actualizarFoto = function (nombre, id, response) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, _b;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        _b = (_a = response).send;
                        return [4 /*yield*/, this.usuarioRepository.updateFoto(nombre, id)];
                    case 1: return [2 /*return*/, _b.apply(_a, [_c.sent()])];
                }
            });
        });
    };
    UsuarioController.prototype.actualizarVideo = function (nombre, id, response) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, _b;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        _b = (_a = response).send;
                        return [4 /*yield*/, this.usuarioRepository.updateVideo(nombre, id)];
                    case 1: return [2 /*return*/, _b.apply(_a, [_c.sent()])];
                }
            });
        });
    };
    UsuarioController.prototype.precioMaximo = function (response) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, _b;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        _b = (_a = response).send;
                        return [4 /*yield*/, this.usuarioRepository.precioMaximo()];
                    case 1: return [2 /*return*/, _b.apply(_a, [_c.sent()])];
                }
            });
        });
    };
    //@UseGuards(RolGuard)
    UsuarioController.prototype.update = function (id, nuevo) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.usuarioRepository.update(id, nuevo)];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    UsuarioController.prototype.remove = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.usuarioRepository["delete"](id)];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    UsuarioController.prototype.uploadImage = function (file) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                //console.log("file que llega", file);
                if (file) {
                    return [2 /*return*/, {
                            response: '¡Imagen guardada con éxito!',
                            imagePath: (file.destination + "/" + file.filename).replace('./public/usuario/', '')
                        }];
                }
                else {
                    throw new common_1.HttpException('¡Formato de imagen no soportado!', common_1.HttpStatus.BAD_REQUEST);
                }
                return [2 /*return*/];
            });
        });
    };
    UsuarioController.prototype.getFileCurso = function (response, name) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, response.sendFile("./" + name, { root: 'public/usuario' }, function (error) {
                        if (error) {
                            return response.send('');
                        }
                        else {
                            return response;
                        }
                    })];
            });
        });
    };
    __decorate([
        common_1.Post(),
        __param(0, common_1.Body())
    ], UsuarioController.prototype, "create");
    __decorate([
        common_1.Post('actualizar'),
        __param(0, common_1.Body())
    ], UsuarioController.prototype, "actualizar");
    __decorate([
        common_1.Post('registro'),
        __param(0, common_1.Body())
    ], UsuarioController.prototype, "register");
    __decorate([
        common_1.Get(),
        __param(0, common_1.Res())
    ], UsuarioController.prototype, "findAll");
    __decorate([
        common_1.Get('profesores'),
        __param(0, common_1.Res())
    ], UsuarioController.prototype, "findAllTeachers");
    __decorate([
        common_1.Get('id/:id'),
        __param(0, common_1.Param('id')), __param(1, common_1.Res())
    ], UsuarioController.prototype, "findOne");
    __decorate([
        common_1.Get('idrol/:idrol'),
        __param(0, common_1.Param('idrol')), __param(1, common_1.Res())
    ], UsuarioController.prototype, "findIdRol");
    __decorate([
        common_1.Post('recuperarcontrasenia'),
        __param(0, common_1.Body())
    ], UsuarioController.prototype, "recuperarContrase\u00F1a");
    __decorate([
        common_1.Post('cambiarcontrasenia'),
        __param(0, common_1.Body())
    ], UsuarioController.prototype, "cambiarContrase\u00F1a");
    __decorate([
        common_1.Post('cancelarclase'),
        __param(0, common_1.Body())
    ], UsuarioController.prototype, "cancelClass");
    __decorate([
        common_1.Get('filtrarhorario/:hora'),
        __param(0, common_1.Param('hora')), __param(1, common_1.Res())
    ], UsuarioController.prototype, "findHoraInicio");
    __decorate([
        common_1.Get('enlinea/:hora/:id'),
        __param(0, common_1.Param('hora')), __param(1, common_1.Param('id')), __param(2, common_1.Res())
    ], UsuarioController.prototype, "enlinea");
    __decorate([
        common_1.Get('actualizarfoto/:nombre/:id'),
        __param(0, common_1.Param('nombre')), __param(1, common_1.Param('id')), __param(2, common_1.Res())
    ], UsuarioController.prototype, "actualizarFoto");
    __decorate([
        common_1.Get('actualizarvideo/:nombre/:id'),
        __param(0, common_1.Param('nombre')), __param(1, common_1.Param('id')), __param(2, common_1.Res())
    ], UsuarioController.prototype, "actualizarVideo");
    __decorate([
        common_1.Get('maxprecio'),
        __param(0, common_1.Res())
    ], UsuarioController.prototype, "precioMaximo");
    __decorate([
        common_1.Put(':id'),
        __param(0, common_1.Param('id')), __param(1, common_1.Body())
    ], UsuarioController.prototype, "update");
    __decorate([
        common_1.Delete(':id'),
        __param(0, common_1.Param('id'))
    ], UsuarioController.prototype, "remove");
    __decorate([
        common_1.Post('upload-image'),
        common_1.UseInterceptors(platform_express_1.FileInterceptor('filename', {
            storage: multer_1.diskStorage({
                destination: function (req, file, cb) {
                    //console.log("file 2", file);
                    var fs = require('fs');
                    var dir = "./public/usuario";
                    if (!fs.existsSync('./public')) {
                        fs.mkdirSync('./public');
                        fs.mkdirSync(dir);
                    }
                    else if (!fs.existsSync(dir)) {
                        fs.mkdirSync(dir);
                    }
                    cb(null, dir);
                },
                filename: function (req, file, cb) {
                    cb(null, "" + file.originalname);
                }
            }),
            limits: { fileSize: 104857600 },
            fileFilter: function fileFilter(req, file, cb) {
                //console.log("file 1", file);
                var filetypes = /.jpg|.png|.jpeg|.PNG|.mp4/;
                var mimetype = filetypes.test(file.mimetype);
                var extname2 = filetypes.test(path_1.extname(file.originalname).toLowerCase());
                if (mimetype && extname2) {
                    return cb(null, true);
                }
                else {
                    cb(null, false);
                }
            }
        })),
        __param(0, common_1.UploadedFile())
    ], UsuarioController.prototype, "uploadImage");
    __decorate([
        common_1.Get('imagen/:name'),
        __param(0, common_1.Res()), __param(1, common_1.Param('name'))
    ], UsuarioController.prototype, "getFileCurso");
    UsuarioController = __decorate([
        common_1.Controller('usuario')
    ], UsuarioController);
    return UsuarioController;
}());
exports.UsuarioController = UsuarioController;
