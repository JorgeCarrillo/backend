import { Body, Controller, Delete, Get, Param, Post, Put, Query, Res } from '@nestjs/common';
import { EmisorReceptorRepository } from '../repositories/emisor-receptor.repository';
import { CrearEmisorReceptorDto } from '../dtos/create/crear-emisor-receptor.dto';

@Controller('emisor_receptor')
export class EmisorReceptorController {

  constructor(
    private readonly emisorReceptorRepository: EmisorReceptorRepository
  ) { }

  @Post()
  create(@Body() crearNuevo: CrearEmisorReceptorDto) {
    return this.emisorReceptorRepository.insert(crearNuevo);
  }

  @Get()
  async findAll(@Res() response) {
    return response.send(await this.emisorReceptorRepository.selectAll());
  }

  @Get('id/:id')
  async findById(@Res() response, @Param('id') id) {
    return response.send(await this.emisorReceptorRepository.selectById(id));
  }

  @Get('emisor/:id_emisor/:id_receptor')
  async findByIdemi(@Res() response, @Param('id_emisor')id_emisor, @Param('id_receptor')id_receptor) {
    return response.send(await this.emisorReceptorRepository.selectByIdemi(id_emisor,id_receptor));
  }

  @Get('receptor/:id_receptor')
  async findByIdrecep(@Res() response, @Param('id_receptor') id_receptor) {
    return response.send(await this.emisorReceptorRepository.selectByIdrecep(id_receptor));
  }
  @Get('ultimomensaje/:id_receptor/:id_emisor')
  async findByIdUltimoMensaje(@Res() response, @Param('id_emisor')id_emisor, @Param('id_receptor')id_receptor) {
    return response.send(await this.emisorReceptorRepository.findByIdUltimoMensaj(id_emisor,id_receptor));
  }

  /* @Get('emisor_receptor/:params')
   async findByFilter(@Res() response, @Query() query) {
     //console.log(query)
     return response.send(await this.emisorReceptorRepository.selectByIds(query));
   }*/

  @Put(':id')
  async update(@Param('id') id, @Body() nuevo) {
    return await this.emisorReceptorRepository.update(id, nuevo);
  }

  @Delete(':id')
  async remove(@Param('id') id) {
    return await this.emisorReceptorRepository.delete(id);
  }

}