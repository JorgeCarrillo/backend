import { Body, Controller, Delete, Get, Param, Post, Put, Res } from '@nestjs/common';
import { ComentariosRepository } from '../repositories/comentarios.repository';
import { CrearComentarioDto } from '../dtos/create/crear-comentario.dto';

@Controller('comentario')
export class ComentariosController {

  constructor(
    private readonly comentarioRepository: ComentariosRepository,
  ) {
  }

  @Post()
  create(@Body() crearNuevo: CrearComentarioDto) {
    return this.comentarioRepository.insert(crearNuevo);
  }

  @Get()
  async findAll(@Res() response) {
    return response.send(await this.comentarioRepository.selectAll());
  }

  @Get('id/:id')
  async findById(@Res() response, @Param('id') id) {
    return response.send(await  this.comentarioRepository.selectById(id));
  }
  @Get('idprofesor/:id')
  async findByIdprofesor(@Res() response, @Param('id') id) {
    return response.send(await  this.comentarioRepository.selectByIdProfesor(id));
  }

  @Put(':id')
  async update(@Param('id') id, @Body() nuevo) {
    return await this.comentarioRepository.update(id, nuevo);
  }

  @Delete(':id')
  async remove(@Param('id') id) {
    return await this.comentarioRepository.delete(id);
  }

}