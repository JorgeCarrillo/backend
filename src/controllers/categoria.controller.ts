import { Body, Controller, Delete, Get, Param, Post, Put, Res } from '@nestjs/common';
import { CrearCategoriaDto } from '../dtos/create/crear-categoria.dto';
import { CategoriaRepository } from '../repositories/categoria.repository';

@Controller('categoria')
export class CategoriaController {

  constructor(
    private readonly categoriaRepository: CategoriaRepository,
  ) {
  }

  @Post()
  create(@Body() crearNuevo: CrearCategoriaDto) {
    return this.categoriaRepository.insert(crearNuevo);
  }

  @Get()
  async findAll(@Res() response) {
    return response.send(await this.categoriaRepository.selectAll());
  }

  @Get('id/:id')
  async findById(@Res() response, @Param('id') id) {
    return response.send(await  this.categoriaRepository.selectById(id));
  }

  @Put(':id')
  async update(@Param('id') id, @Body() nuevo) {
    return await this.categoriaRepository.update(id, nuevo);
  }

  @Delete(':id')
  async remove(@Param('id') id) {
    return await this.categoriaRepository.delete(id);
  }

}