import { Body, Controller, Delete, Get, Param, Post, Put, Res } from '@nestjs/common';
import { DetaDiaHoraRepository } from '../repositories/deta-dia-hora.repository';
import { CrearDetaDiaHoraDto } from '../dtos/create/crear-deta-dia-hora.dto';
import { DetaPerMateRepository } from '../repositories/deta-per-mate';
import { CrearDetaPerMateDto } from '../dtos/create/crear-deta-per-mate';

@Controller('detalle_materia')
export class DetaPerMateController {

  constructor(
    private readonly detallemateriaRepository: DetaPerMateRepository
  ) { }

  @Post()
  create(@Body() crearNuevo: CrearDetaPerMateDto) {
    return this.detallemateriaRepository.insert(crearNuevo);
  }

  @Get()
  async findAll(@Res() response) {
    return response.send(await this.detallemateriaRepository.selectAll());
  }

  @Get('id/:id')
  async findById(@Res() response, @Param('id') id) {
    return response.send(await this.detallemateriaRepository.selectById(id));
  }

  @Get('idtuto/:id')
  async findByIdTutoria(@Res() response, @Param('id') id) {
    return response.send(await this.detallemateriaRepository.selectByIdTutoria(id));
  }

  @Put(':id')
  async update(@Param('id') id, @Body() nuevo) {
    return await this.detallemateriaRepository.update(id, nuevo);
  }

  @Delete(':id')
  async remove(@Param('id') id) {
    return await this.detallemateriaRepository.delete(id);
  }

}