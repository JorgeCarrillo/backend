import { Body, Controller, Delete, Get, Param, Post, Put, Res } from '@nestjs/common';
import { MensajeRepository } from '../repositories/mensaje.repository';
import { CrearMensajeDto } from '../dtos/create/crear-mensaje.dto';

@Controller('mensaje')
export class MensajeController {

  constructor(
    private readonly mensajeRepository: MensajeRepository
  ) {}

  @Post()
  create(@Body() crearNuevo: CrearMensajeDto) {
    return this.mensajeRepository.insert(crearNuevo);
  }

  @Get()
  async findAll(@Res() response) {
    return response.send(await this.mensajeRepository.selectAll());
  }

  @Get('id/:id')
  async findById(@Res() response, @Param('id') id) {
    return response.send(await  this.mensajeRepository.selectById(id));
  }

  @Put(':id')
  async update(@Param('id') id, @Body() nuevo) {
    return await this.mensajeRepository.update(id, nuevo);
  }

  @Delete(':id')
  async remove(@Param('id') id) {
    return await this.mensajeRepository.delete(id);
  }

}