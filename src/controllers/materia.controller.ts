import { Body, Controller, Delete, Get, Param, Post, Put, Res } from '@nestjs/common';
import { MateriaRepository } from '../repositories/materia.repository';
import { CrearMateriaDto } from '../dtos/create/crear-materia.dto';

@Controller('materia')
export class MateriaController {

  constructor(
    private readonly materiaRepository: MateriaRepository,
  ) {
  }

  @Post()
  create(@Body() crearNuevo: CrearMateriaDto) {
    return this.materiaRepository.insert(crearNuevo);
  }

  @Get()
  async findAll(@Res() response) {
    return response.send(await this.materiaRepository.selectAll());
  }

  @Get('id/:id')
  async findById(@Res() response, @Param('id') id) {
    return response.send(await  this.materiaRepository.selectById(id));
  }

  @Put(':id')
  async update(@Param('id') id, @Body() nuevo) {
    return await this.materiaRepository.update(id, nuevo);
  }

  @Delete(':id')
  async remove(@Param('id') id) {
    return await this.materiaRepository.delete(id);
  }

}