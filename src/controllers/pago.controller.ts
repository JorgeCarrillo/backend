import { Body, Controller, Delete, Get, Param, Post, Put, Res } from '@nestjs/common';
import { PagoRepository } from '../repositories/pago.repository';
import { CrearPagoDto } from '../dtos/create/crear-pago.dto';

@Controller('pago')
export class PagoController {

  constructor(
    private readonly pagoRepository: PagoRepository
  ) {}

  @Post()
  create(@Body() crearNuevo: CrearPagoDto) {
    return this.pagoRepository.insert(crearNuevo);
  }

  @Get()
  async findAll(@Res() response) {
    return response.send(await this.pagoRepository.selectAll());
  }

  @Get('id/:id')
  async findById(@Res() response, @Param('id') id) {
    return response.send(await  this.pagoRepository.selectById(id));
  }

  @Put(':id')
  async update(@Param('id') id, @Body() nuevo) {
    return await this.pagoRepository.update(id, nuevo);
  }

  @Delete(':id')
  async remove(@Param('id') id) {
    return await this.pagoRepository.delete(id);
  }

}