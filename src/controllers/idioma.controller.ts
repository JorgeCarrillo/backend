import { Body, Controller, Delete, Get, Param, Post, Put, Res } from '@nestjs/common';
import { IdiomaRepository } from '../repositories/idioma.repository';
import { CrearIdiomaDto } from '../dtos/create/crear-idioma.dto';

@Controller('idioma')
export class IdiomaController {

  constructor(
    private readonly idiomaRepository: IdiomaRepository,
  ) { }

  @Post()
  create(@Body() crearNuevo: CrearIdiomaDto) {
    return this.idiomaRepository.insert(crearNuevo);
  }

  @Get()
  async findAll(@Res() response) {
    return response.send(await this.idiomaRepository.selectAll());
  }
  @Get('filtro')
  async todo(@Res() response) {
    return response.send(await this.idiomaRepository.selectTodo());
  }

  @Get('id/:id')
  async findById(@Res() response, @Param('id') id) {
    return response.send(await this.idiomaRepository.selectById(id));
  }

  @Get('usuario/:id')
  async findByIdUsuario(@Res() response, @Param('id') id) {
    return response.send(await this.idiomaRepository.selectByIdUsuario(id));
  }

  @Put(':id')
  async update(@Param('id') id, @Body() nuevo) {
    return await this.idiomaRepository.update(id, nuevo);
  }

  @Delete(':id')
  async remove(@Param('id') id) {
    return await this.idiomaRepository.delete(id);
  }

}