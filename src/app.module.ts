import { HttpModule, Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsuarioEntity } from './entities/usuario.entity';
import { RolEntity } from './entities/rol.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AutenticacionController } from './controllers/autenticacion.controller';
import { AutenticacionService } from './services/autenticacion.service';
import { JwtService } from './services/jwt.service';
import { PermisoEntity } from './entities/permiso.entity';
import { UsuarioRepository } from './repositories/usuario.repository';
import { RolRepository } from './repositories/rol.repository';
import { RolController } from './controllers/rol.controller';
import { PermisoRepository } from './repositories/permiso.repository';
import { PermisoController } from './controllers/permiso.controller';
import { UsuarioController } from './controllers/usuario.controller';
import { CiudadEntity } from './entities/ciudad.entity';
import { ConfiguracionEntity } from './entities/configuracion.entity';
import { DetallesProfesorEntity } from './entities/detalles-profesor.entity';
import { EstadoEntity } from './entities/estado.entity';
import { LoginUsuarioEntity } from './entities/login-usuario.entity';
import { MateriaEntity } from './entities/materia.entity';
import { PaisEntity } from './entities/pais.entity';
import { TitulosProfesorEntity } from './entities/titulos-profesor.entity';
import { UsuarioRolEntity } from './entities/usuario-rol.entity';
import { CiudadController } from './controllers/ciudad.controller';
import { ConfiguracionController } from './controllers/configuracion.controller';
import { DetallesProfesorController } from './controllers/detalles-profesor.controller';
import { EstadoController } from './controllers/estado.controller';
import { LoginUsuarioController } from './controllers/login-usuario.controller';
import { MateriaController } from './controllers/materia.controller';
import { PaisController } from './controllers/pais.controller';
import { TitulosProfesorController } from './controllers/titulos-profesor.controller';
import { UsuarioRolController } from './controllers/usuario-rol.controller';
import { CiudadRepository } from './repositories/ciudad.repository';
import { ConfiguracionRepository } from './repositories/configuracion.repository';
import { DetallesProfesorRepository } from './repositories/detalles-profesor.repository';
import { EstadoRepository } from './repositories/estado.repository';
import { LoginUsuarioRepository } from './repositories/login-usuario.repository';
import { MateriaRepository } from './repositories/materia.repository';
import { PaisRepository } from './repositories/pais.repository';
import { TitulosProfesorRepository } from './repositories/titulos-profesor.repository';
import { UsuarioRolRepository } from './repositories/usuario-rol.repository';
import { RecoveryPasswordController } from './controllers/recovery-password.controller';
import { RecoveryPasswordService } from './services/recovery-password.service';
import { EmailService } from './services/email.service';
import { RegisterService } from './services/register.service';
import { CategoriaEntity } from './entities/categoria.entity';
import { CategoriaController } from './controllers/categoria.controller';
import { CategoriaRepository } from './repositories/categoria.repository';
import { ZoomApiController } from './controllers/external/zoomApi.controller';
import { ZoomApiService } from './services/zoomApi.service';
import { CuentaZoomEntity } from './entities/cuenta-zoom.entity';
import { SubcuentaZoomEntity } from './entities/subcuenta-zoom.entity';
import { CuentaZoomController } from './controllers/cuenta-zoom.controller';
import { SubcuentaZoomController } from './controllers/subcuenta-zoom.controller';
import { CuentaZoomRepository } from './repositories/cuenta-zoom.repository';
import { SubcuentaZoomRepository } from './repositories/subcuenta-zoom.repository';
import { MoodleService } from './services/moodle.service';
import { MoodleController } from './controllers/external/moodle.controller';
import { AsigPerServEntity } from './entities/asig-per-serv.entity';
import { AsigUserPerEntity } from './entities/asig-user-per.entity';
import { CancelAnulaEntity } from './entities/cancel-anula.entity';
import { ComentariosEntity } from './entities/comentarios.entity';
import { DetaDiaHoraEntity } from './entities/deta-dia-hora.entity';
import { TajetaPagoEntity } from './entities/tarjeta-pago.entity';
import { DetallePedidoEntity } from './entities/detalle-pedido.entity';
import { EmisorReceptorEntity } from './entities/emisor-receptor.entity';
import { FaqEntity } from './entities/faq.entity';
import { IdiomaEntity } from './entities/idioma.entity';
import { MensajeEntity } from './entities/mensaje.entity';
import { PagoEntity } from './entities/pago.entity';
import { PedidoEntity } from './entities/pedido.entity';
import { PersonalizadoEntity } from './entities/personalizado.entity';
import { ReunionEntity } from './entities/reunion.entity';
import { ServiciosEntity } from './entities/servicios.entity';
import { AsigPerServRepository } from './repositories/asig-per-serv.repository';
import { AsigUserPerRepository } from './repositories/asig-user-per.repository';
import { CancelAnulaRepository } from './repositories/cancel-anula.repository';
import { ComentariosRepository } from './repositories/comentarios.repository';
import { DetaDiaHoraRepository } from './repositories/deta-dia-hora.repository';
import { TajetaPagoRepository } from './repositories/tarjeta-pago.repository';
import { DetallePedidoRepository } from './repositories/detalle-pedido.repository';
import { EmisorReceptorRepository } from './repositories/emisor-receptor.repository';
import { FaqRepository } from './repositories/faq.repository';
import { IdiomaRepository } from './repositories/idioma.repository';
import { MensajeRepository } from './repositories/mensaje.repository';
import { PagoRepository } from './repositories/pago.repository';
import { PedidoRepository } from './repositories/pedido.repository';
import { PersonalizadoRepository } from './repositories/personalizado.repository';
import { ReunionRepository } from './repositories/reunion.repository';
import { ServiciosRepository } from './repositories/servicios.repository';
import { AsigPerServController } from './controllers/asig-per-serv.controller';
import { AsigUserPerController } from './controllers/asig-user-per.controller';
import { CancelAnulaController } from './controllers/cancel-anula.controller';
import { ComentariosController } from './controllers/comentarios.controller';
import { DetaDiaHoraController } from './controllers/deta-dia-hora.controller';
import { TajetaPagoController } from './controllers/tarjeta-pago.controller';
import { DetallePedidoController } from './controllers/detalle-pedido.controller';
import { EmisorReceptorController } from './controllers/emisor-receptor.controller';
import { FaqController } from './controllers/faq.controller';
import { IdiomaController } from './controllers/idioma.controller';
import { MensajeController } from './controllers/mensaje.controller';
import { PagoController } from './controllers/pago.controller';
import { PedidoController } from './controllers/pedido.controller';
import { PersonalizadoController } from './controllers/personalizado.controller';
import { ReunionController } from './controllers/reunion.controller';
import { ServiciosController } from './controllers/servicios.controller';
import { DetaPerMateEntity } from './entities/deta-per-mate';
import { DetaPerMateController } from './controllers/deta-per-mate';
import { DetaPerMateRepository } from './repositories/deta-per-mate';

const ENTIDADES = [
  AsigPerServEntity,
  AsigUserPerEntity,
  CancelAnulaEntity,
  CategoriaEntity,
  CiudadEntity,
  ComentariosEntity,
  ConfiguracionEntity,
  CuentaZoomEntity,
  DetaDiaHoraEntity,
  TajetaPagoEntity,
  DetaPerMateEntity,
  DetallePedidoEntity,
  DetallesProfesorEntity,
  EmisorReceptorEntity,
  EstadoEntity,
  FaqEntity,
  IdiomaEntity,
  LoginUsuarioEntity,
  MateriaEntity,
  MensajeEntity,
  PagoEntity,
  PaisEntity,
  PedidoEntity,
  PermisoEntity,
  PersonalizadoEntity,
  ReunionEntity,
  RolEntity,
  ServiciosEntity,
  SubcuentaZoomEntity,
  TitulosProfesorEntity,
  UsuarioEntity,
  UsuarioRolEntity
];


@Module({
  imports: [
    HttpModule,
    TypeOrmModule.forRootAsync({
      useFactory: () => ({
        type: 'postgres',
        host: process.env.DB_HOST || 'localhost',
        port: +process.env.DB_PORT || 5432,
        username: process.env.DB_USER || 'postgres',
        password: process.env.DB_PASS || '12345',
        database: process.env.DB_DATABASE || 'amauta_tutorias_back',
        entities: ENTIDADES,
        synchronize: true,
      }),
    }),
    TypeOrmModule.forFeature(ENTIDADES),
  ],
  controllers: [
    AppController,
    AsigPerServController,
    AsigUserPerController,
    AutenticacionController,
    CancelAnulaController,
    CategoriaController,
    CiudadController,
    ComentariosController,
    ConfiguracionController,
    CuentaZoomController,
    DetaDiaHoraController,
    TajetaPagoController,
    DetaPerMateController,
    DetallePedidoController,
    DetallesProfesorController,
    EmisorReceptorController,
    EstadoController,
    FaqController,
    IdiomaController,
    LoginUsuarioController,
    MateriaController,
    MensajeController,
    PagoController,
    PaisController,
    PedidoController,
    PermisoController,
    PersonalizadoController,
    RecoveryPasswordController,
    ReunionController,
    RolController,
    ServiciosController,
    SubcuentaZoomController,
    TitulosProfesorController,
    UsuarioController,
    UsuarioRolController,

    /*EXTERNALS*/
    MoodleController,
    ZoomApiController,
  ],
  providers: [
    AppService,
    AutenticacionService,
    JwtService,
    MoodleService,
    RecoveryPasswordService,
    EmailService,
    RegisterService,
    ZoomApiService,

    AsigPerServRepository,
    AsigUserPerRepository,
    CancelAnulaRepository,
    CategoriaRepository,
    CiudadRepository,
    ComentariosRepository,
    ConfiguracionRepository,
    CuentaZoomRepository,
    DetaDiaHoraRepository,
    TajetaPagoRepository,
    DetaPerMateRepository,
    DetallePedidoRepository,
    DetallesProfesorRepository,
    EmisorReceptorRepository,
    EstadoRepository,
    FaqRepository,
    IdiomaRepository,
    LoginUsuarioRepository,
    MateriaRepository,
    MensajeRepository,
    PagoRepository,
    PaisRepository,
    PedidoRepository,
    PermisoRepository,
    PersonalizadoRepository,
    ReunionRepository,
    RolRepository,
    ServiciosRepository,
    SubcuentaZoomRepository,
    TitulosProfesorRepository,
    UsuarioRepository,
    UsuarioRolRepository

  ],
})
export class AppModule {
}
