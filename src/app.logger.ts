import { Logger } from '@nestjs/common';
import { AppUtil } from './app.util';
import { AppConstant } from './app.constant';

/**
 * Imprime en consola los logs de la aplicación y a su vez guarda en un archivo de texto.
 * @author Darwin Guzmán
 * @version 1.0
 */
export class AppLogger extends Logger {
  pathFileLog: string;

  constructor() {
    super();
    this.pathFileLog = AppConstant.LOGS_ION_ARCHIVO_PATH;
  }

  log(message: any, context?: string): void {
    super.log(message, context);
    this.addMessageToFile(message);
  }

  warn(message: any, context?: string): void {
    super.warn(message, context);
    this.addMessageToFile(message);
  }

  error(message: any, trace?: string, context?: string): void {
    super.error(message, trace, context);
    this.addMessageToFile(message);
  }

  private addMessageToFile(message: any) {
    AppUtil.appendFile(this.pathFileLog, `${new Date().toLocaleString()}: ${message}\n`);
  }

}