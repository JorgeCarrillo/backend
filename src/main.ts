import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as dotenv from 'dotenv';
import { Logger, ValidationPipe } from '@nestjs/common';
import * as bodyParser from 'body-parser';
import { join } from "path";
import { NestExpressApplication } from '@nestjs/platform-express';

async function bootstrap() {
  dotenv.config({ path: `${process.env.NODE_ENV}.env` });
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  app.useGlobalPipes(new ValidationPipe(
    {
      whitelist: true,
      forbidNonWhitelisted: true,
    },
  ));
  app.enableCors();
  app.useStaticAssets(join(__dirname, '../public'), {
    prefix: '/public/',
  });
  app.useStaticAssets(join(__dirname, '../www'), );
  app.use(bodyParser.json({ limit: '50mb' }));
  app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
  const port = process.env.PORT || 3000;
  await app.listen(port);
  Logger.log(`Server is listening on ${process.env.NODE_ENV} environment with port ${port}`, 'Main');
}

bootstrap();
