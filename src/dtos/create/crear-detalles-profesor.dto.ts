import { IsInt, IsOptional, IsString } from 'class-validator';

export class CrearDetallesProfesorDto {

  @IsString()
  @IsOptional()
  cv_deta_profe: string;

  @IsString()
  @IsOptional()
  penales_deta_profe: string;

  @IsString()
  @IsOptional()
  descri_deta_profe: string;

  @IsString()
  @IsOptional()
  ruta_deta_profe: string;

  @IsString()
  @IsOptional()
  facebook: string;

  @IsString()
  @IsOptional()
  twitter: string;

  @IsString()
  @IsOptional()
  youtube: string;

  @IsString()
  @IsOptional()
  instagram: string;

  @IsString()
  @IsOptional()
  experiencia: string;

  @IsString()
  @IsOptional()
  preferencia_ense: string;

  @IsString()
  @IsOptional()
  preciofijo: string;

  @IsString()
  @IsOptional()
  video_presentacion: string;

  @IsString()
  @IsOptional()
  rating: string;

  @IsInt()
  id_usuario_rol: number;
}