import { IsString, IsOptional } from 'class-validator';

export class CrearCategoriaDto {

  @IsString()
  nombre_categoria: string;

  @IsString()
  @IsOptional()
  descripcion_categoria: string;

  @IsString()
  @IsOptional()
  imagen_categoria: string;

  /*@IsUrl()
  ruta_material: string;*/

  /*@IsOptional()
  @IsInt()
  curso: number;*/

}