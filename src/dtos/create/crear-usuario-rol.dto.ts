import { IsInt } from 'class-validator';

export class CrearUsuarioRolDto {

  @IsInt()
  usuario: number;

  @IsInt()
  idRol: number;

}