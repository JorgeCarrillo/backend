import { IsInt, IsString } from 'class-validator';

export class TarjetaPagoDto {

  @IsString()
  titular_tarjeta: string;

  @IsInt()
  numero_tarjeta: number;

  @IsInt()
  mes: number;

  @IsInt()
  ano: number;

  @IsInt()
  cvv: number;

  @IsInt()
  id_usuario_rol: number;

}