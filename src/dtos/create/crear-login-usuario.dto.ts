import { IsDateString, IsEmail, IsInt, IsOptional, IsString } from 'class-validator';

export class CrearLoginUsuarioDto {

  @IsEmail()
  nombre_login: string;

  @IsString()
  password_login: string;

  @IsDateString()
  @IsOptional()
  fecha_ingre: Date;

  @IsInt()
  @IsOptional()
  id_usuario: number;

}