import { IsBoolean, IsDateString, IsEmail, IsInt, IsOptional, IsString } from 'class-validator';

export class CrearUsuarioDto {

  @IsString()
  @IsOptional()
  identificacion_usuario: string;

  @IsString()
  @IsOptional()
  nombres_usuario: string;

  @IsString()
  @IsOptional()
  apellidos_usuario: string;

  @IsDateString()
  @IsOptional()
  fecha_naci_usuario: Date;

  @IsDateString()
  @IsOptional()
  fecha_regisro_usuario: Date;

  @IsEmail()
  @IsOptional()
  correo_usuario: string;

  @IsString()
  @IsOptional()
  foto_usuario: string;

  @IsString()
  @IsOptional()
  telefono_usuario: string;

  @IsString()
  @IsOptional()
  direccion_usuario: string;

  @IsString()
  @IsOptional()
  sexo_usuario: string;

  @IsString()
  @IsOptional()
  codigo_password: string;

  @IsBoolean()
  @IsOptional()
  estado_codigo: boolean;

  @IsBoolean()
  @IsOptional()
  estado_usuario: boolean;

  @IsBoolean()
  @IsOptional()
  estado_usuario_cuenta: boolean;

  @IsDateString()
  @IsOptional()
  ultima_conexion: Date;

  @IsString()
  @IsOptional()
  ciudad_nombre: string;

  @IsInt()
  @IsOptional()
  ciudad: number;

}
