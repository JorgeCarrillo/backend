export class CrearRolDto {
  public constructor(init?: Partial<CrearRolDto>) {
    Object.assign(this, init);
  }

  readonly nombre_rol: string;
  readonly descripcion: string;
  readonly idsPermisos: number[];
}
