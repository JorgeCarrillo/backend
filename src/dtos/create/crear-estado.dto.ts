import { IsInt, IsOptional, IsString } from 'class-validator';

export class CrearEstadoDto {

  @IsString()
  nombre_estado: string;

  @IsInt()
  @IsOptional()
  pais: number;

}