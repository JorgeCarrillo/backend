import { IsInt, IsString } from 'class-validator';

export class CrearCuentaZoomDto {

  @IsString()
  nombre_cuenta: string;

  @IsString()
  password_cuenta: string;

  @IsString()
  api_secret: string;

  @IsString()
  token: string;

  @IsInt()
  numero_cuentas: number;

}