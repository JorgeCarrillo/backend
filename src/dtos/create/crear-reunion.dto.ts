import { IsBoolean, IsDateString, IsInt, IsString, IsUrl } from 'class-validator';

export class CrearReunionDto {

  @IsString()
  id_reunion: string;

  @IsUrl()
  enlace: string;
  @IsUrl()
  enlaceEstudiante: string;
  @IsString()
  password: string;

  @IsString()
  fecha_inicio: string;

  @IsString()
  fecha_fin: string;

  @IsString()
  hora_inicio: string;

  @IsString()
  hora_fin: string;

  @IsBoolean()
  estado: boolean;

  @IsInt()
  id_subcuenta: number;

  @IsInt()
  id_pedido: number;

}