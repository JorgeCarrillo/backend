import { IsInt, IsString } from 'class-validator';

export class CrearCancelAnulaDto {

  @IsString()
  condicion_anula: string;

  @IsString()
  condicion_cambia: string;

  /*@IsInt()
  id_tutoria: number;*/

  @IsInt()
  id_pedido: number;

}