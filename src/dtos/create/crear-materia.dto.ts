import { IsInt, IsString } from 'class-validator';

export class CrearMateriaDto {

  @IsString()
  nombre_materia: string;

  @IsInt()
  id_categoria?: number;

}