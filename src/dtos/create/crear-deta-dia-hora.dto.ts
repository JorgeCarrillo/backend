import { IsInt, IsString } from 'class-validator';

export class CrearDetaDiaHoraDto {

  @IsString()
  hora_inicio: string;

  @IsString()
  hora_fin: string;

  @IsInt()
  estado_reserva: number;
  
  @IsInt()
  id_usuario_rol: number;

}