import { IsBoolean, IsInt, IsString } from 'class-validator';

export class CrearIdiomaDto {

  @IsString()
  idioma: string;

  @IsString()
  nivel: string;

  @IsBoolean()
  preferencia: boolean;

  @IsInt()
  id_usuario: number;

}