import { IsInt, IsOptional, IsString } from 'class-validator';

export class CrearCiudadDto {

  @IsString()
  nombre_ciudad: string;

  @IsInt()
  @IsOptional()
  id_estado: number;

}