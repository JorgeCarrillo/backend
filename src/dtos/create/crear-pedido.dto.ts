import { IsBoolean, IsDateString, IsInt, IsString } from 'class-validator';

export class CrearPedidoDto {

  @IsString()
  detalle_pedido: string;

  @IsString()
  fecha_inicio: string;

  @IsBoolean()
  estado: boolean;

  @IsInt()
  id_asig_user_per: number;

  @IsInt()
  id_usuario_rol: number;
  @IsInt()
  id_usuario_profesor: number;

  @IsInt()
  id_deta_dia_hora: number;

}