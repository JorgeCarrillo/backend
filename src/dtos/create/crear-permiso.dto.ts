import { IsString } from 'class-validator';

export class CrearPermisoDto {

  @IsString()
  nombre_permiso: string;

  @IsString()
  descripcion_permiso: string;

  @IsString()
  url_permiso: string;

  @IsString()
  metodo: string;

}