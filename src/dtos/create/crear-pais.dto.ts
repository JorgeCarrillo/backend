import { IsString } from 'class-validator';

export class CrearPaisDto {

  @IsString()
  nombre_pais: string;

}