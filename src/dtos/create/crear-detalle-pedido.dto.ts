import { IsInt, IsNumber, IsString } from 'class-validator';

export class CrearDetallePedidoDto {

  @IsString()
  codigo: string;

  @IsString()
  descripcion: string;

  @IsInt()
  cantidad: number;

  @IsNumber()
  valor_unitario: number;

  @IsNumber()
  valor_total: number;

  @IsInt()
  id_pedido: number;

  @IsString()
  fecha_creacion: string;

}