import { IsInt, IsOptional, IsString } from 'class-validator';

export class CrearTitulosProfesorDto {

  @IsString()
  senescyt_titulos_profe: string;

  @IsInt()
  id_usuario_rol: number;

  @IsString()
  nivel_profesor: string;

}