import { IsNotEmpty, IsString } from 'class-validator';
import { CrearLoginUsuarioDto } from './crear-login-usuario.dto';
import { CrearUsuarioDto } from './crear-usuario.dto';

export class CrearCuentaDto {

  @IsNotEmpty()
  login: CrearLoginUsuarioDto;

  @IsNotEmpty()
  user: CrearUsuarioDto;

  @IsString()
  @IsNotEmpty()
  nombre_rol: string;

}