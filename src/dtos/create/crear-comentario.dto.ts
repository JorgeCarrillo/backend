import { IsDate, IsDateString, IsInt, IsString } from 'class-validator';

export class CrearComentarioDto {

  @IsDateString()
  fecha_valoracion: Date;

  @IsString()
  comentario: string;

  @IsInt()
  valoracion: number;

  @IsInt()
  id_usuario_rol: number;

  @IsInt()
  id_usuario_rol_profesor: number;
}