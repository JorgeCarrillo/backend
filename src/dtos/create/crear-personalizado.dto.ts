import { IsBoolean, IsDateString, IsInt, IsString } from 'class-validator';

export class CrearPersonalizadoDto {

  @IsString()
  fecha_creacion: string;

  @IsBoolean()
  demo: boolean;

  @IsString()
  duracion_demo: string;

  /* @IsInt()
   precio_fijo: number;*/

  /*  @IsInt()
    id_materia: number;
  
    @IsInt()
    id_descuento: number;
  
    @IsInt()
    id_disponibilidad: number;*/
}