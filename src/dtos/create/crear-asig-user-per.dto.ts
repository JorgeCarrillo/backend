import { IsInt } from 'class-validator';

export class CrearAsigUserPerDto {

  @IsInt()
  id_usuario_rol: number;

  @IsInt()
  id_asig_per_serv: number;

}