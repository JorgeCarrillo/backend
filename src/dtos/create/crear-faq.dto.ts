import { IsString } from 'class-validator';

export class CrearFaqDto {

  @IsString()
  pregunta: string;

  @IsString()
  respuesta: string;

}