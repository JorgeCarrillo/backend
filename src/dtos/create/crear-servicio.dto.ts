import { IsBoolean, IsString } from 'class-validator';

export class CrearServicioDto {

  @IsString()
  nombre_servicio: string;

  @IsString()
  descripcion: string;

  @IsBoolean()
  estado: boolean;

}