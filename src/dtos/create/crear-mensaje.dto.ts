import { IsDateString, IsString } from 'class-validator';

export class CrearMensajeDto {

  @IsString()
  mensaje: string;

  @IsString()
  fecha_mensaje: string;

  @IsString()
  hora: string;

}