import { IsInt } from 'class-validator';

export class CrearEmisorReceptorDto {

  @IsInt()
  id_emisor: number;

  @IsInt()
  id_receptor: number;

  @IsInt()
  id_mensaje: number;

}