import { IsDecimal, IsInt, IsString } from 'class-validator';

export class CrearPagoDto {

  @IsString()
  metodo_pago: string;

  @IsDecimal()
  sub_total: number;

  @IsDecimal()
  impuesto: number;

  @IsDecimal()
  total: number;

  @IsInt()
  id_detalle_pedido: number;

}