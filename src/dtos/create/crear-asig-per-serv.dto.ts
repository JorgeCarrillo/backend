import { IsInt } from 'class-validator';

export class CrearAsigPerServDto {

  @IsInt()
  id_servicio: number;

  @IsInt()
  id_tutoria: number;

}