import { IsInt, IsOptional, IsString } from 'class-validator';

export class CrearSubcuentaZoomDto {

  @IsString()
  nombre_usuario: string;

  @IsString()
  contraseña: string;

  @IsInt()
  @IsOptional()
  id_cuenta: number;
  
  @IsString()
  @IsOptional()
  id_usuario_zoom: string;

  @IsString()
  @IsOptional()
  disponibilidad_hora: string;



}