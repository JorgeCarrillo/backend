import { IsInt } from 'class-validator';

export class CrearDetaPerMateDto {

  @IsInt()
  id_tutoria: number;

  @IsInt()
  id_materia: number;


}