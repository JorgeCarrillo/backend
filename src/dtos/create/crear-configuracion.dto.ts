import { IsNotEmpty, IsString } from 'class-validator';

export class CrearConfiguracionDto {

  @IsString()
  @IsNotEmpty()
  nombre_config: string;

  @IsString()
  @IsNotEmpty()
  valor_config: string;

}