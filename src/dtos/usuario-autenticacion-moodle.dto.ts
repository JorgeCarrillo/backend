import { IsEmail, IsOptional, IsString } from 'class-validator';

export class UsuarioAutenticacionMoodleDto {

  @IsEmail()
  email: string;

  @IsString()
  @IsOptional()
  wstoken?: string;

}
