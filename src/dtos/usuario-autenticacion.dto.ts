import { IsEmail, IsString } from 'class-validator';

export class UsuarioAutenticacionDto {

  @IsEmail()
  nombre_login: string;

  @IsString()
  password_login: string;

}
