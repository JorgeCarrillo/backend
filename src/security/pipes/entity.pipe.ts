import { ArgumentMetadata, HttpException, HttpStatus, Injectable, PipeTransform } from '@nestjs/common';

const Joi = require('joi');

@Injectable()
export class EntityPipe implements PipeTransform {

  constructor(private readonly _schema) {
  }


  transform(jsonToValidate: any, metadata: ArgumentMetadata) {
    const {
      error,
    } = Joi.validate(jsonToValidate, this._schema);
    if (error) {
      throw new HttpException({
        error: error,
        mensaje: '¡Json no válido!',
      }, HttpStatus.BAD_REQUEST);
    } else {
      return jsonToValidate;
    }
  }
}
